###############################################################
#
# Jahshaka 1.9 RC1 Configuration File
#
###############################################################

###############################################################
# System wide settings here, you can modify these
###############################################################
# This is the default config for all libraries and the app

include( config.pro )

## inclusion control
JAH_MAIN_SETTINGS_INCLUDED=1

###System Options###
JAHTHEMES       = true   #some linuxes have trouble with qt themes
JAHGIFT         = true   #for giFT p2p support very experimental
SPACEBALL       = false  #for spaceball support

###Media Options###
JAHMPEGDECODER  = false   #mpeg decoding using built in encoder
JAHMPEGENCODER  = false   #mpeg encoding support using built in decoder

###Hardware Acceleration###
NVIDIA_GPU      = true   #for GPU support
NVIDIA_GPGPU    = false  #for GPGPU support (not compatable with GPU keep this off!)

###Jahplayer Build dont touch this###
JAHPLAYER       =   false

###############################################################
# now we do a bit of auto configueration

###Debug or release ###
CONFIG  += warn_on qt opengl thread rtti exceptions stl
contains( JAHDEBUG,true ) {
message(DEBUG)
CONFIG += debug
} else {
message(RELEASE)
CONFIG  += release
DEFINES = NDEBUG
}

###currently windows and osx builds are static by default###

win32 {
	JAHSTATIC       = true  #set to true for static linking
}

mac {
	JAHSTATIC       = true  #set to true for static linking
}

###MLT Configure Options###

#false by default
MLTSUPPORT      = false
LINUXAVISUPPORT = false

!win32 {

	# Determine if the mlt-config script is available
	MLTPREFIX = $$system(mlt-config --prefix 2>/dev/null)

	# Determine if avifile-config script is available
	AVIPREFIX = $$system(avifile-config --prefix 2>/dev/null)

	!isEmpty( AVIPREFIX ) {
		# Drop to avifile if no MLT detected
		LINUXAVISUPPORT = true  #avi support on linux using avifile
	}

	!isEmpty( MLTPREFIX ) {
		# Use MLT when available
		MLTSUPPORT      = true   #mlt support on linux (possibly os/x and irix)
		LINUXAVISUPPORT = false
	}
}

### OpenObjectLib configure options ###

# false by default
OPENOBJECTLIBSUPPORT = false

!win32 {
	# determine if openobjectlib exists
	OPENOBJECTLIBEXISTS = $$system(pkg-config --exists openlibraries 2>/dev/null ; echo $?)

	contains( OPENOBJECTLIBEXISTS, 0 ) {
		OPENOBJECTLIBSUPPORT = true
	}
}

###############################################################
# Configuration from here on down
###############################################################

# we must specify the path where libsurface3d.so will be stored
unix {
	JAH_SHARED_OBJECT_PATH = $$system( pwd )
}

###############################################################
# Sets up the Qt depend path, override if it doesnt work
# should be /usr/lib/qt-3.1/include
#think this is just for irix

unix {
	QTPATH = $$system(echo $QTDIR)
}

win32 {
	QTPATH = C:/Qt/3.3.3
}

JAHDEPENDPATH = $$QTPATH/include

unix {
	TMAKE_CXXFLAGS_RELEASE  += -rdynamic
}

###############################################################
# OS specific settings here
###############################################################

#for windows only we test here since uname wont work
win32	{
	OSNAME = Windows
}
!win32	{
	OSNAME = $$system(uname -s)
}

message( Operating System Type  ($$OSNAME)  )

OSDEF = LOST


###############################################################
# calculated paths here
###############################################################

unix {
	JAHSHAREDIR = $$JAHPREFIX/share/jahshaka
}

###############################################################
# QMake related fixes and globals here
###############################################################

false {
isEmpty( QMAKE_INSTALL_FILE ):	QMAKE_INSTALL_FILE = $$QMAKE_COPY_FILE
isEmpty( QMAKE_INSTALL_DIR ):	QMAKE_INSTALL_DIR = $$QMAKE_COPY_DIR

isEmpty( INSTALL_FILE ):	INSTALL_FILE = $$QMAKE_INSTALL_FILE
isEmpty( INSTALL_DIR ):		INSTALL_DIR = $$QMAKE_INSTALL_DIR
isEmpty( DEL_FILE ):		DEL_FILE = $$QMAKE_DEL_FILE
isEmpty( DEL_DIR ):		DEL_DIR = $$QMAKE_DEL_DIR
}

###############################################################
# Configure declrartions
###############################################################

DEFINES += JAHPREFIX=\"$$JAHPREFIX\" _THREAD_SAFE=true

#for qt theme support
contains( JAHTHEMES,true ) 		{
DEFINES += JAHTHEMES
}

#for giFT p2p support
contains( JAHGIFT,true ) 		{
DEFINES += JAHGIFT
}

#for Nvidia GPU
contains( NVIDIA_GPU,true ) 	{
DEFINES += NVIDIA_GPU
}

#for Nvidia GPGPU
contains( NVIDIA_GPGPU,true ) 	{
DEFINES += NVIDIA_GPGPU
}

#for the audio hack
contains( JAHAUDIO,true ) 		{
DEFINES += AUDIOSUPPORT
}

#for jahplayer support
contains( JAHPLAYER,true ) 		{
DEFINES += JAHPLAYER
}

#for native mpeg decoder support
contains( JAHMPEGDECODER,true ) {
DEFINES += JAHMPEGDECODER
}

#for native mpeg encoder support
contains( JAHMPEGENCODER,true ) {
DEFINES += JAHMPEGENCODER
}

#for spaceball
contains( SPACEBALL,true ) 		{
DEFINES += SPACEBALL
}

#for avi
contains( LINUXAVISUPPORT,true ) {
DEFINES += LINUXAVISUPPORT
}

#for mlt
contains( MLTSUPPORT,true ) 	{
    DEFINES += MLTSUPPORT
	LIBS += -L$$MLTPREFIX/lib -lmlt++ $$system(mlt-config --libs 2>/dev/null) -lz
	FREEDIR += $$MLTPREFIX/include/mlt
	FREEDIR += $$MLTPREFIX/include
}

#for OpenObjectLib
contains( OPENOBJECTLIBSUPPORT,true ) {
	DEFINES += NEWOPENOBJECTLIB

	contains( OSNAME,[wW]indows ) {
		DEFINES += HAVE_FLEX_STRING
		DEFINES += HAVE_OPENIMAGELIB

		#location of openobjectlib and openimagelib libraries
		OPENOBJECTLIB_INCLUDE	= C:/openobjectlib/include/openobjectlib \
								  C:/openobjectlib/include
		OPENIMAGELIB_INCLUDE	= C:/openimagelib/include/openimagelib \
								  C:/openimagelib/include
		BOOST_INCLUDE			= C:/Boost/include/boost-1_33

		OPENOBJECTLIB_LIBS		= C:/openobjectlib/lib
		OPENIMAGELIB_LIBS		= C:/openimagelib/lib
		BOOST_LIBS				= C:/Boost/lib
	} else {
		OPENLIBRARIES_INCLUDE = $$system(pkg-config --cflags openlibraries 2>/dev/null | sed 's/-I//g')
		OPENLIBRARIES_LIBS = $$system(pkg-config --libs openlibraries 2>/dev/null)
		OPENIMAGELIB_PLUGINS_PATH = $$system(pkg-config --variable=OPENIMAGELIB_PLUGINS openlibraries 2>/dev/null)
		OPENMEDIALIB_PLUGINS_PATH = $$system(pkg-config --variable=OPENMEDIALIB_PLUGINS openlibraries 2>/dev/null)
		OPENEFFECTSLIB_PLUGINS_PATH = $$system(pkg-config --variable=OPENEFFECTSLIB_PLUGINS openlibraries 2>/dev/null)
		OPENOBJECTLIB_PLUGINS_PATH = $$system(pkg-config --variable=OPENOBJECTLIB_PLUGINS openlibraries 2>/dev/null)
		
		DEFINES += OPENIMAGELIB_PLUGINS=\"$$OPENIMAGELIB_PLUGINS_PATH\"
		DEFINES += OPENMEDIALIB_PLUGINS=\"$$OPENMEDIALIB_PLUGINS_PATH\"
		DEFINES += OPENEFFECTSLIB_PLUGINS=\"$$OPENEFFECTSLIB_PLUGINS_PATH\"
		DEFINES += OPENOBJECTLIB_PLUGINS=\"$$OPENOBJECTLIB_PLUGINS_PATH\"
	}
}

#define this for script support. needed because of the player build.
contains( JAHPLAYER,false ) {
DEFINES += JAHSCRIPT
}

###############################################################
# System wide OS specific settings here, you can modify these
###############################################################

###############################################################
# Linux specific settings here

contains( OSNAME,[lL]inux  ) {
    message( "Using linux presets..." )
    OSDEF = FOUND

	CONFIG += x11

    #os specific variable
    JAHOS=LINUX
    DEFINES += JAHLINUX

    #location of freetype libraries on linux
    FREEDIR += /usr/include/freetype2

	#for  <GL/glut.h>
    FREEDIR += /usr/local/include
    FREEDIR += /usr/include

}

###############################################################
# OsX specific settings here

contains( OSNAME,[dD]arwin ) {
    message( "Using Mac OsX presets..." )
    OSDEF = FOUND

    #os specific variable
    JAHOS=DARWIN
    DEFINES += JAHDARWIN

	DEFINES += FTGL_LIBRARY_STATIC
	#DEFINES += GLEW_STATIC
	DEFINES += PIXELSHADERS

    #default location of freetype libraries on mac
    FREEDIR += /usr/local/include/freetype2

    contains( OPENALFRAMEWORK, true) {
	OPENAL_INCLUDE=/System/Library/Frameworks/OpenAL.framework/Headers
	OPENAL_LIBS=-framework OpenAL
	DEFINES += OPENALFRAMEWORK
    } else {
	OPENAL_LIBS=-lopenal
    }
}

###############################################################
# Sgi Irix specific settings here

contains( OSNAME,IRIX64    ) {
    message( "Using Irix presets..." )
    OSDEF = FOUND

    #os specific variable
    JAHOS=IRIX
    DEFINES += JAHIRIX

    #location of freetype libraries on irix
    FREEDIR += /usr/nekoware/include/freetype2

    SGIDIR  += /usr/nekoware/include
}

###############################################################
# Windows specific settings here

contains( OSNAME,[wW]indows    ) {
    message( "Using Windows presets..." )
    OSDEF = FOUND

    #os specific variables
    JAHOS=WINDOWS
    DEFINES += JAHWINDOWS
    DEFINES += WIN32
    DEFINES += WINAVISUPPORT
    #DEFINES += _DEBUG
    DEFINES += _WINDOWS
	DEFINES += PARTICLEDLL_EXPORTS
	DEFINES += FTGL_LIBRARY_STATIC
	#DEFINES += GLEW_STATIC
	DEFINES += PIXELSHADERS
	#DEFINES += JAH_DLL_EXPORT
	#DEFINES += QT_NO_STYLE_WINDOWS
	DEFINES -= UNICODE

    #location of freetype libraries on windows
    FREEDIR += C:/freetype/include

	contains( OPENOBJECTLIBSUPPORT,false ) {
		DEFINES += GLEW_STATIC
	}
}



###############################################################

# Sgi Irix old Qt specific settings here
#this is a hack for sgi systems using the older qt 3.0 libs
#that are distributed with the aug03 freeware
#jahshaka needs at least 3.1 for optimal functionality

unix {

#exists( /usr/lib/Xm/bindings/sgi ) {

#message( "Using Unstable 3.0 Irix presets..." )

    #os specific variable
#    JAHOS=IRIX
#    OSDEF = FOUND
#    DEFINES += JAHIRIX

    #location of freetype libraries on irix
#    FREEDIR += /usr/freeware/include/freetype2

    #location of qt libraries on irix
#    JAHDEPENDPATH += /usr/freeware/Qt/include
#    SGIDIR  += /usr/freeware/include
#    SGIDIR += /usr/freeware/include/GL

} else {

##############################################################
# Cant figure out the os use some defaults and hope

contains( OSDEF, LOST  ) {
    message( "Cant figure out OS Using linux defaults..." )

    #os specific variable
    JAHOS=LINUX
    DEFINES += JAHLINUX

	#we have to assume this is where it is
    QTPATH = /usr/lib/qt-3.1
    JAHDEPENDPATH += /usr/lib/qt-3.1/include

    #location of freetype libraries on linux
    FREEDIR += /usr/include/freetype2

    #for the audio hack
    contains( JAHAUDIO,true ) {

    #we hack the directories where libaudio is on to freedir
    FREEDIR += /usr/local/include
    FREEDIR += /usr/include
    DEFINES += AUDIOSUPPORT
    }
}

}

} ###end of file



