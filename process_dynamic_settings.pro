# $Id: process_dynamic_settings.pro,v 1.1 2005/12/07 14:37:09 ezequiel Exp $

## mandatory symbols:
##
## JAH_BUILD_ROOT={path_to_root_dir_in_source_tree}
##

# for now, this file can only be included once
isEmpty( JAH_PROCESS_DYNAMIC_SETTINGS_INCLUDED ) {
JAH_PROCESS_DYNAMIC_SETTINGS_INCLUDED=1

## pre-validation
isEmpty( JAH_BUILD_ROOT ) {
	error( "JAH_BUILD_ROOT was not specified." )
}

## make sure the necessary files are included
isEmpty( JAH_MAIN_SETTINGS_INCLUDED ):	include( $$JAH_BUILD_ROOT/Settings.pro )

## processing

# specify JAH_PROCESS_USEOPENLIBRARIES_SYS=true to include openlibraries support
contains( JAH_PROCESS_USEOPENLIBRARIES_SYS,true ) {

	# specify JAH_PROCESS_USEOPENLIBRARIES_SYS_CFLAGS=false to avoid using the cflags for the openlibraries
	!contains( JAH_PROCESS_USEOPENLIBRARIES_SYS_CFLAGS,false ){
		INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
		# LATER: add settings to the flags variable(s) (CFLAGS/CXXFLAGS?)
	}

	# specify JAH_PROCESS_USEOPENLIBRARIES_SYS_LIBS=false to avoid linking against the libraries
	!contains( JAH_PROCESS_USEOPENLIBRARIES_SYS_LIBS,false ) {
		LIBS += $$OPENLIBRARIES_LIBS
	}
}

} # JAH_PROCESS_DYNAMIC_SETTINGS_INCLUDED

