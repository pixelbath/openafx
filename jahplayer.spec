%define tarball_name jahshaka
%define name jahshaka
%define version 2.0
%define release 1
%define _prefix /usr

Summary: jahplayer
Name: jahplayer
Version: %{version}
Release: %{release}
Source: %{tarball_name}.tar.gz
Vendor: The jahshaka Project
URL: http://www.jahshaka.org
License: GPL
Group: Multimedia/Applications
Prefix: %{_prefix}
BuildRoot: %{_tmppath}/%name-%{version}-root
Requires: olib-mlt++ openlibraries
BuildRequires: olib-mlt++-devel openlibraries-devel

%description
Jahplayer

%prep
%setup -n jahshaka -q

%build
./configure --prefix=%{_prefix} --disable-debug --disable-plugins jahplayer
LDFLAGS=-Wl,--rpath=%{_prefix}/lib make
./configure --prefix=$RPM_BUILD_ROOT/%{_prefix} --disable-plugins jahplayer
strip jahplayer
mv jahplayer jahplayer.bin
find . -name "*.so" -exec strip {} \;

echo "#!/usr/bin/env sh" > jahplayer
echo "export PATH=/usr/olib/0.2/bin:\$PATH" >> jahplayer
echo "exec jahplayer.bin \"\$*\"" >> jahplayer
chmod +x jahplayer

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
cp jahplayer.bin $RPM_BUILD_ROOT/%{_prefix}/bin/jahplayer.bin

%clean
#rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_prefix}/bin/
%{_prefix}/lib/
%{_prefix}/share/

