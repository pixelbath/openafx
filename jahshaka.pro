###############################################################
#
# Jahshaka 2.0 QMake build files
#
###############################################################

TEMPLATE = subdirs

message( "Jahshaka 2.0 build system" )
message( "-----------------------------" )
message( "" )
message( "Checking operating system and paths" )
message( "" )

include( Settings.pro )

message( "" )
message( $$DEFINES )
message( $$FREEDIR )
message( "" )

###############################################################
# notify user of build status

contains( JAHPLAYER,true ) {
message( "Building Jahplayer 2.0" )
message( "----------------------" )
message( "" )
} else {
message( "Building Jahshaka 2.0" )
message( "---------------------" )
message( "" )
}

###############################################################
# Configure subsystems

message( "Configuring Subsystems" )
message( "----------------------" )
message( "" )

message( ">> Configuring Auxillary Libraries" )
SUBDIRS +=	source/AuxiliaryLibraries
message( "" )
message( ">> Configuring OpenLibraries" )
SUBDIRS +=	source/OpenLibraries

message( "Configuring Jahshaka" )
message( "--------------------" )
message( "" )
message( "Configuring JahCore" )
SUBDIRS += source/Jahshaka/JahCore
message( "Configuring JahDesktop" )
SUBDIRS += source/Jahshaka/JahDesktop
message( "Configuring JahLibraries" )
SUBDIRS += source/Jahshaka/JahLibraries
message( "Configuring JahModules" )
SUBDIRS += source/Jahshaka/JahModules
message( "Configuring JahSource" )
SUBDIRS += source/Jahshaka/JahSource
message( "Configuring JahWidgets" )
SUBDIRS += source/Jahshaka/JahWidgets

message( "Configuring Jahshaka Linker" )
SUBDIRS += source

message( "" )
message( "Finished Configuring Subsystems" )
message( "-------------------------------" )
message( "" )

############################################

#flag for clearspeed support on linux
contains( CLEARSPEED,true ) {
message( "Configuring ClearSpeed Support" )
SUBDIRS +=  plugins/csplugins
}

############################################
#options for plugin support
#plugins can always be built manually by
#doing ./configure in the plugins directory

contains(JAHPLUGINBUILD,true) {
message( "Configuring Plugin Support" )
SUBDIRS +=  plugins
message( " " )
}

###############################################################
message( "Make files have been configured" )
message( "type make to build" )
message( "" )
message( "please file all bugs at http://bugs.jahshaka.org" )
message( "" )

contains(JAHPLUGINBUILD,false) {
message( "" )
message( "PLUGINS" )
message( "-------" )
message( "Dont forget to build the plugins!" )
message( "Just type:" )
message( "cd plugins; ./configure; make" )
}

