# OpenAFX

This is a fork of the Jahshaka 2.0 project. The intention is to create a timeline-based effects editor.

Below is the original readme which will be phased out as this project is modernized. Versioning will start at v0.1.

# Original README

**Welcome the the Jahshaka 2.0RC1 release!**

Since this is currently beta software, be very careful (as mentioned above)!

Jahshaka currently runs on Linux, Windows, OsX and Irix.  We have
tested on Fedora Core 3 and 4, Slackware, Windows XP, Irix 6.5.18 and 
OsX 10.3 and 10.4 although most other linuxes and windows should work.

## COMPILING

To run jahshaka, you need to compile first if you are working with the source.
If you have downloaded the binary, skip over to GETTING STARTED

The only dependencies you need are Qt 3.x, freetype 2.x and glut 3.7+. You can 
find these online easily by doing a search in google. Please read the INSTALL
document for information on compiling jahshaka

## GETTING STARTED

To execute jahshaka, go to the jahshaka main directory (where this file is) 
and type:

`./jahshaka (for linux)`

or 

double click on the jahshaka file (for windows or OsX)

## EXPERIMENTING

To start using jahshaka, simply load a few clips onto the desktop by clicking the 
Load Clip button. We currently only support stills and image sequences but media
file support is on the way.

Once you have a few clips, check out the modules.
Create a layer, and then to get a clip to fill it, hit the 'Get Desktop' button.
Select a clip from the desktop, and then hit the red 'return' button to use it.

In order to save-out scene files they must have a .jsf extension. Saving and
loading is done through the World layers object menu.

## FEEDBACK

So far things are working nicely, and the force-play option in the controller
is a great example of what hardware accelerated open-gl can really do
when its put to good use. The interface is a mess, and we are cleaning it up for
the next alpha release, plus making things a lot more slick and intuitive.


## FORUMS

Please report all bugs to the Jahshaka bugzilla at:

http://bugs.jahshaka.org

Thanks and Enjoy!


The Jahshaka Project


