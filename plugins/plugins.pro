###############################################################
#
# Jahshaka Plugins QMake build files
#
###############################################################

message( "Jahshaka       plugins" )
message( "----------------------" )
message( "" )

###############################################################
# Configure subdirectories

message( "Adding Plugins to path" )
SUBDIRS +=  rfxplugins \
            jfxplugins \
            jitplugins \
            rtplugins

TEMPLATE = subdirs

