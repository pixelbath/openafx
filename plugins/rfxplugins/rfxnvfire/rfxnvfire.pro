###############################################################
# $Id: rfxnvfire.pro,v 1.11 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvfire

DEFINES     += 		RFXNVFIRE_EXPORTS

HEADERS     =           rfxnvfire.h \
			config.h

SOURCES     =           rfxnvfire.cpp \
                        turbulence.cpp

include ( ../rfxplugins_standard.pro )

