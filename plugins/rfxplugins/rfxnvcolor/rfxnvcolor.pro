###############################################################
# $Id: rfxnvcolor.pro,v 1.14 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvcolor

DEFINES     += 		RFXNVCOLOR_EXPORTS

HEADERS     =           rfxnvcolor.h \
			config.h

SOURCES     =           rfxnvcolor.cpp \
                        rfxarbcolor.cpp

include ( ../rfxplugins_standard.pro )

