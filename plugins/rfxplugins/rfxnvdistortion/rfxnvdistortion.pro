###############################################################
# $Id: rfxnvdistortion.pro,v 1.14 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvdistortion

DEFINES     += 		RFXNVDISTORTION_EXPORTS

HEADERS     =           rfxnvdistortion.h \
			config.h

SOURCES     =           rfxnvdistortion.cpp \
                        rfxarbdistortion.cpp

include ( ../rfxplugins_standard.pro )

