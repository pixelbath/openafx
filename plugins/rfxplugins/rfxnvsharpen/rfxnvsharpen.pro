###############################################################
# $Id: rfxnvsharpen.pro,v 1.12 2005/12/07 14:37:12 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvsharpen

DEFINES     += 		RFXNVSHARPEN_EXPORTS

HEADERS     =           rfxnvsharpen.h \
			config.h

SOURCES     =           rfxnvsharpen.cpp \
                        rfxarbsharpen.cpp

include ( ../rfxplugins_standard.pro )

