###############################################################
# $Id: rfxnvcharcoal.pro,v 1.10 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvcharcoal

DEFINES     += 		RFXNVCHARCOAL_EXPORTS

HEADERS     =           rfxnvcharcoal.h \
			config.h

SOURCES     =           rfxnvcharcoal.cpp \
                        mesh.cpp

include ( ../rfxplugins_standard.pro )

