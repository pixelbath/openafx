###############################################################
# $Id: rfxnvreflection.pro,v 1.9 2005/12/07 14:37:12 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvreflection

DEFINES     += 		RFXNVREFLECTION_EXPORTS

HEADERS     =           rfxnvreflection.h \
			config.h

SOURCES     =           rfxnvreflection.cpp \
                        turbulence.cpp

include ( ../rfxplugins_standard.pro )

