###############################################################
# $Id: rfxnvpan.pro,v 1.12 2005/12/07 14:37:12 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvpan

DEFINES     += 		RFXNVPAN_EXPORTS

HEADERS     =           rfxnvpan.h \
			config.h

SOURCES     =           rfxnvpan.cpp \
                        rfxarbpan.cpp

include ( ../rfxplugins_standard.pro )

