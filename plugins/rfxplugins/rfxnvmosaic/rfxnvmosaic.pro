###############################################################
# $Id: rfxnvmosaic.pro,v 1.11 2005/12/07 14:37:12 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvmosaic

DEFINES     += 		RFXNVMOSAIC_EXPORTS

HEADERS     =           rfxnvdistortion.h \
			config.h

SOURCES     =           rfxnvmosaic.cpp \
                        rfxarbmosaic.cpp

include ( ../rfxplugins_standard.pro )

