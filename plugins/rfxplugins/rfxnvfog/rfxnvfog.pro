###############################################################
# $Id: rfxnvfog.pro,v 1.11 2005/12/07 14:37:12 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvfog

DEFINES     += 		RFXNVFOG_EXPORTS

HEADERS     =           rfxnvfog.h \
			config.h

SOURCES     =           rfxnvfog.cpp \
                        turbulence.cpp

include ( ../rfxplugins_standard.pro )

