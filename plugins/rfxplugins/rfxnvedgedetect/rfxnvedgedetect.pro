###############################################################
# $Id: rfxnvedgedetect.pro,v 1.14 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvedgedetect

DEFINES     += 		RFXNVEDGEDETECT_EXPORTS

HEADERS     =           rfxnvedgedetect.h \
			config.h

SOURCES     =           rfxnvedgedetect.cpp \
                        rfxarbedgedetect.cpp

include ( ../rfxplugins_standard.pro )

