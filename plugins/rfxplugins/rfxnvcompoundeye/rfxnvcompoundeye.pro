###############################################################
# $Id: rfxnvcompoundeye.pro,v 1.14 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvcompoundeye

DEFINES     += 		RFXNVCOMPOUNDEYE_EXPORTS

HEADERS     =           rfxnvcompoundeye.h \
			config.h

SOURCES     =           rfxnvcompoundeye.cpp \
                        rfxarbcompoundeye.cpp

include ( ../rfxplugins_standard.pro )

