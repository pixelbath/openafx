###############################################################
# $Id: rfxnvchannelblur.pro,v 1.9 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvchannelblur

DEFINES     += 		RFXNVCHANNELBLUR_EXPORTS

HEADERS     =           rfxnvchannelblur.h \
			config.h

SOURCES     =           rfxnvchannelblur.cpp \
                        rfxarbchannelblur.cpp

include ( ../rfxplugins_standard.pro )

