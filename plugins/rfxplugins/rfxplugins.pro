###############################################################
#
# Jahshaka RasFx Plugins QMake build files
#
###############################################################

message( "Jahshaka RasFx plugins" )
message( "----------------------" )
message( "" )

###############################################################
# Configure subdirectories

message( "Adding Plugins to path" )
SUBDIRS +=  rfxnvswirl \
            rfxnvfisheye \
            rfxnvcompoundeye \
            rfxnvsphere \
            rfxnvcolor \
            rfxnvblur \
            rfxnvchannelblur \
            rfxnvedgedetect \
            rfxnvsharpen \
            rfxnvpan \
            rfxnvdistortion \
            rfxnvturbulence \
            rfxnvnorthernlights \
            rfxnvflame \
            rfxnvfire \
            rfxnvfog \
            rfxnvdiffusion \
            rfxnvcharcoal \
            rfxnvlighting \
            rfxnvmosaic \
            rfxnvreflection

TEMPLATE = subdirs

