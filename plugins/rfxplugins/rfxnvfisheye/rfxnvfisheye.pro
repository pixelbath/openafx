###############################################################
# $Id: rfxnvfisheye.pro,v 1.13 2005/12/07 14:37:12 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvfisheye

DEFINES     += 		RFXNVFISHEYE_EXPORTS

HEADERS     =           rfxnvfisheye.h \
			config.h

SOURCES     =           rfxnvfisheye.cpp \
                        rfxarbfisheye.cpp

include ( ../rfxplugins_standard.pro )

