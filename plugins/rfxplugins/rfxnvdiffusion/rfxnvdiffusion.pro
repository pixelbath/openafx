###############################################################
# $Id: rfxnvdiffusion.pro,v 1.11 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvdiffusion

DEFINES     += 		RFXNVDIFFUSION_EXPORTS

HEADERS     =           rfxnvdiffusion.h \
			config.h

SOURCES     =           rfxnvdiffusion.cpp \
                        turbulence.cpp

include ( ../rfxplugins_standard.pro )

