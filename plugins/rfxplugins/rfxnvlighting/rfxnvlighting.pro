###############################################################
# $Id: rfxnvlighting.pro,v 1.10 2005/12/07 14:37:12 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvlighting

DEFINES     += 		RFXNVLIGHTING_EXPORTS

HEADERS     =           rfxnvlighting.h \
			config.h

SOURCES     =           rfxnvlighting.cpp \
                        mesh.cpp

include ( ../rfxplugins_standard.pro )

