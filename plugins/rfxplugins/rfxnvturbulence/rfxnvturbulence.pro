###############################################################
# $Id: rfxnvturbulence.pro,v 1.9 2005/12/07 14:37:13 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvturbulence

DEFINES     += 		RFXNVTURBULENCE_EXPORTS

HEADERS     =           rfxnvturbulence.h \
			config.h

SOURCES     =           rfxnvturbulence.cpp

include ( ../rfxplugins_standard.pro )

