
#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvswirl

DEFINES     += 		RFXNVSWIRL_EXPORTS

HEADERS     =           rfxnvswirl.h \
			config.h

SOURCES     =           rfxnvswirl.cpp \
                        rfxarbswirl.cpp \

include ( ../rfxplugins_standard.pro )

