###############################################################
# $Id: rfxnvsphere.pro,v 1.12 2005/12/07 14:37:13 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvsphere

DEFINES     += 		RFXNVSPHERE_EXPORTS

HEADERS     =           rfxnvsphere.h \
			config.h

SOURCES     =           rfxnvsphere.cpp \
                        rfxarbsphere.cpp

include ( ../rfxplugins_standard.pro )

