###############################################################
# $Id: rfxnvblur.pro,v 1.14 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rfxnvblur

DEFINES     += 		RFXNVBLUR_EXPORTS

HEADERS     =           rfxnvblur.h \
			config.h

SOURCES     =           rfxnvblur.cpp \
                        rfxarbblur.cpp

include ( ../rfxplugins_standard.pro )

