###############################################################
# $Id: rtripple.pro,v 1.9 2005/12/07 14:37:13 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../rtripple

DEFINES     += 		RTRIPPLE_EXPORTS

HEADERS     =           rtripple.h \
			config.h

SOURCES     =           rtripple.cpp

include ( ../rtplugins_standard.pro )

