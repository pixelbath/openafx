###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin
HEADERS     =           csmandelbrot.h

SOURCES     =           csmandelbrot.cpp

TARGET      =           ../csmandelbrot

DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes

INCLUDEPATH =           . .. \
                        ../../../jah/libraries/jahplugins \
                        ../../../jah/libraries/glew

DEFINES += __LITTLE_ENDIAN
