###############################################################
#
# Jahshaka JahFx Plugins QMake build files
#
###############################################################

message( "Jahshaka Clearspeed plugins" )
message( "---------------------------" )
message( "" )

###############################################################
# Configure subdirectories

message( "Adding Plugins to path" )
SUBDIRS +=  csfisheye \
            csmandelbrot \
            csmandelbrotcolor

#need to get this code back into the plugin modules for them to run
#by adding a cscore directory
#SUBDIRS += jah/Modules/effect/clearspeed/host
#LIBS += Modules/effect/clearspeed/host/libclearspeed.a
#LIBS += -L/opt/cs_sdk/lib -lllapiusap -ldrvcommon -lcsthread -lloader_spoff -lspof -lbuild_version -lsdksupport -lsystem_definition -lpfelf -lcommand_options -lproperties -lgeneric -lgallimaufry -lhs_regex

TEMPLATE = subdirs
