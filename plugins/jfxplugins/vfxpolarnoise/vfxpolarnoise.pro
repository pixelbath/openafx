###############################################################
# $Id: vfxpolarnoise.pro,v 1.8 2005/12/07 14:37:09 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../vfxpolarnoise

DEFINES     += 		VFXPOLARNOISE_EXPORTS

HEADERS     =           vfxpolarnoise.h \
			config.h

SOURCES     =           vfxpolarnoise.cpp

include ( ../jfxplugins_standard.pro )

