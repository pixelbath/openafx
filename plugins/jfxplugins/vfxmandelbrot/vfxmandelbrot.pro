###############################################################
# $Id: vfxmandelbrot.pro,v 1.8 2005/12/07 14:37:09 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../vfxmandelbrot

DEFINES     += 		VFXMANDELBROT_EXPORTS

HEADERS     =           vfxmandelbrot.h \
			config.h

SOURCES     =           vfxmandelbrot.cpp

include ( ../jfxplugins_standard.pro )

