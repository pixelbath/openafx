###############################################################
# $Id: vfxfisheye.pro,v 1.8 2005/12/07 14:37:09 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../vfxfisheye

DEFINES     += 		VFXBATHROOM_EXPORTS

HEADERS     =           vfxfisheye.h \
			config.h

SOURCES     =           vfxfisheye.cpp

include ( ../jfxplugins_standard.pro )

