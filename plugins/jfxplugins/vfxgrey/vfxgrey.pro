###############################################################
# $Id: vfxgrey.pro,v 1.8 2005/12/07 14:37:09 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../vfxgrey

DEFINES     += 		VFXGREY_EXPORTS

HEADERS     =           vfxgrey.h \
			config.h

SOURCES     =           vfxgrey.cpp

include ( ../jfxplugins_standard.pro )

