###############################################################
#
# Jahshaka JahFx Plugins QMake build files
#
###############################################################

message( "Jahshaka JahFx plugins" )
message( "----------------------" )
message( "" )

###############################################################
# Configure subdirectories

message( "Adding Plugins to path" )
SUBDIRS +=  vfxgrey \
            vfxswirl \
            vfxfisheye \
            vfxoil \
            vfxrelief \
            vfxbathroom \
            vfxpolarnoise \
            vfxpolarwarp \
            vfxmandelbrot \
            vfxmosaic

TEMPLATE = subdirs

