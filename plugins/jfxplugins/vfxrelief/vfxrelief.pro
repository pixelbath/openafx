###############################################################
# $Id: vfxrelief.pro,v 1.8 2005/12/07 14:37:10 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../vfxrelief

DEFINES     += 		VFXRELIEF_EXPORTS

HEADERS     =           vfxrelief.h \
			config.h

SOURCES     =           vfxrelief.cpp

include ( ../jfxplugins_standard.pro )

