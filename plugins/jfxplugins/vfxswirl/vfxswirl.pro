###############################################################
# $Id: vfxswirl.pro,v 1.8 2005/12/07 14:37:10 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../vfxswirl

DEFINES     += 		VFXSWIRL_EXPORTS

HEADERS     =           vfxswirl.h \
			config.h

SOURCES     =           vfxswirl.cpp

include ( ../jfxplugins_standard.pro )

