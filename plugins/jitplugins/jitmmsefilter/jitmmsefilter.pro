###############################################################
# $Id: jitmmsefilter.pro,v 1.8 2005/12/07 14:37:10 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../jitmmsefilter

DEFINES += JITMMSEFILTER_EXPORTS

HEADERS     =           jitmmsefilter.h

SOURCES     =           jitmmsefilter.cpp

include ( ../jitplugins_standard.pro )

