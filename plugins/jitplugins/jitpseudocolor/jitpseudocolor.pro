###############################################################
# $Id: jitpseudocolor.pro,v 1.8 2005/12/07 14:37:10 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../jitpseudocolor

DEFINES     += 		JITPSEUDOCOLOR_EXPORTS

HEADERS     =           jitpseudocolor.h \
			config.h

SOURCES     =           jitpseudocolor.cpp

include ( ../jitplugins_standard.pro )

