###############################################################
# $Id: jitgaussnoise.pro,v 1.8 2005/12/07 14:37:10 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../jitgaussnoise

DEFINES += JITGAUSSNOISE_EXPORTS

HEADERS     =           jitgaussnoise.h\
			config.h

SOURCES     =           jitgaussnoise.cpp

include ( ../jitplugins_standard.pro )

