###############################################################
# $Id: jitrgbvary.pro,v 1.8 2005/12/07 14:37:11 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../jitrgbvary

DEFINES     += 		JITRGBVARY_EXPORTS

HEADERS     =           jitrgbvary.h \
			config.h

SOURCES     =           jitrgbvary.cpp

include ( ../jitplugins_standard.pro )

