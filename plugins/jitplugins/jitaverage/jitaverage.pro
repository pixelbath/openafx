###############################################################
# $Id: jitaverage.pro,v 1.8 2005/12/07 14:37:10 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../jitaverage

DEFINES += JITAVERAGE_EXPORTS

HEADERS     =           jitaverage.h \
						config.h

SOURCES     =           jitaverage.cpp

include ( ../jitplugins_standard.pro )

