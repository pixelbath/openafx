###############################################################
# $Id: jitfftfilter.pro,v 1.11 2005/12/07 14:37:10 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../jitfftfilter

DEFINES += JITFFTFILTER_EXPORTS

HEADERS     =           jitfftfilter.h \
                        blurlib.h \
                        fftlib.h \
                        config.h

SOURCES     =           jitfftfilter.cpp \
                        blurlib.cpp \
                        fftlib.cpp

include ( ../jitplugins_standard.pro )

