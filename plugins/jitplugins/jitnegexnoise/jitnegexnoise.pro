###############################################################
# $Id: jitnegexnoise.pro,v 1.8 2005/12/07 14:37:10 ezequiel Exp $
###############################################################

#include presets file in home directory

include( ../../../Settings.pro )

###############################################################

#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          plugin

TARGET      =           ../jitnegexnoise

DEFINES += JITNEGEXNOISE_EXPORTS

HEADERS     =           jitnegexnoise.h \
			config.h

SOURCES     =           jitnegexnoise.cpp

include ( ../jitplugins_standard.pro )

