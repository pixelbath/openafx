###############################################################
#
# Jahshaka JitFx Plugins build files
#
###############################################################

message( "Jahshaka JitFx plugins" )
message( "----------------------" )
message( "" )

###############################################################
# Configure subdirectories

message( "Adding Plugins to path" )
SUBDIRS +=  jitgaussnoise \
            jitnegexnoise \
            jitminmaxfilter \
            jitrangefilter \
            jitmmsefilter \
            jitmedianfilter \
            jitpseudocolor \
            jitaverage \
            jitrgbvary \
            jitgeommeanfilter \
            jitfftfilter

TEMPLATE = subdirs

