# $Id: plugin_standard.pro,v 1.3 2005/12/07 14:37:09 ezequiel Exp $

# mandatory symbols:
#
# JAHPLUGIN_BASEDIR: dir name (not path) for the current plugin's library file.
#  example: JAHPLUGIN_BASEDIR = jfxplugins
#
# optional symbols:
#
#

# by default, this file is ultimately included from:
#  {root}/plugins/{family}/{plugin}, so we have to use ../../.. to reach 'root'
#  (example: plugins/jfxplugins/vfxbathroom)
isEmpty( JAH_BUILD_ROOT ):	JAH_BUILD_ROOT=../../..

# installation:
# . plugin libraries are installed by default
# . use JAHPLUGIN_INSTALL_LIB=false before including this file
#    to disable instalation for the current plugin library
!contains( JAHPLUGIN_INSTALL_LIB,false ) {
	unix:target.path = $$JAHSHAREDIR/plugins/$$JAHPLUGIN_BASEDIR
	unix:INSTALLS += target
}

# DEPENDPATH values:
# . use JAHPLUGIN_DEPEND_ADDSTANDARD=false to disable adding the standard
#   (inherited from parent's qmake project files) paths to the
#   DEPENDPATH variable.
!contains( JAHPLUGIN_DEPEND_ADDSTANDARD,false ) {
	DEPENDPATH += $$JAHDEPENDPATH
}

# MAYBE: define a symbol to point to the "source" directory to avoid
#  the hardcoded assumption that that's ../../../source .
# MAYBE: use another symbol, say JAHSOURCES_ROOTDIR=../../.. and then
#  use $$JAHSOURCES_ROOTDIR/source instead.

# specify JAHPLUGIN_INCLUDE_STDDIRS=false to avoid addnig the standard paths
#  to the list of directories to be searched for include files.
# default: include the standard paths
!contains( JAHPLUGIN_INCLUDE_STDDIRS,false ) {
	INCLUDEPATH += \
	 . .. \
	 $$JAH_BUILD_ROOT/source/Jahshaka/JahLibraries/jahplugins \
	 # end
}

# specify JAHPLUGIN_INCLUDE_OPENGPULIB=true to add the path to opengpulib
#  to the list of directories to be searched for include files.
# default: do not include opengpulib
contains( JAHPLUGIN_INCLUDE_OPENGPULIB,true ) {
	INCLUDEPATH += \
	 $$JAH_BUILD_ROOT/source/OpenLibraries/opengpulib \
	 # end
}

# specify JAHPLUGIN_LIBS_LINK_OPENGPULIB=true to link agaings opengpulib
# default: don't link against opengpulib
contains( JAHPLUGIN_LIBS_LINK_OPENGPULIB,true ) {
	win32 {
		LIBS += $$JAH_BUILD_ROOT/source/OpenLibraries/lib/opengpulib.lib
		LIBS += glew32.lib
	}

	!win32 {
		LIBS += -L$$JAH_BUILD_ROOT/source/OpenLibraries/lib -lopengpulib
		LIBS += -lGLEW
	}
}

## default options for the "dynamic processing" of options
!contains( JAH_PROCESS_USEOPENLIBRARIES_SYS,false ) {
	JAH_PROCESS_USEOPENLIBRARIES_SYS=true
	#-? !contains( JAH_PROCESS_USEOPENLIBRARIES_SYS_LIBS,true ):	JAH_PROCESS_USEOPENLIBRARIES_SYS_LIBS=false
}

include( $$JAH_BUILD_ROOT/process_dynamic_settings.pro )

