###############################################################
#
# Jahshaka 1.9a9 QMake build files
#
###############################################################

message( "Jahplayer 2.0 build system" )
message( "-----------------------------" )
message( "" )
message( "Checking operating system and paths" )
message( "" )

include( Settings.pro )

message( "" )
message( $$DEFINES )
message( $$FREEDIR )
message( "" )

###############################################################
# notify user of build status

contains( JAHPLAYER,true ) {	
message( "Building Jahplayer 2.0" )
message( "----------------------" )
message( "" )
} else {
message( "Building Jahshaka 2.0" )
message( "---------------------" )
message( "" )
}

###############################################################
# Configure subdirectories

message( "Configuring Subsystems" )
message( "----------------------" )
message( "" )

message( ">> Configuring Auxillary Libraries" )
SUBDIRS +=	source/AuxiliaryLibraries
message( "" )
message( ">> Configuring OpenLibraries" )
SUBDIRS +=	source/OpenLibraries

message( "Configuring Jahplayer" )
message( "--------------------" )
message( "" )
message( "Configuring JahCore" )
SUBDIRS += source/Jahshaka/JahCore
message( "Configuring JahDesktop" )
SUBDIRS += source/Jahshaka/JahDesktop
message( "Configuring JahLibraries" )
SUBDIRS += source/Jahshaka/JahLibraries
message( "Configuring JahModules" )
SUBDIRS += source/Jahshaka/JahModules
message( "Configuring JahSource" )
SUBDIRS += source/Jahshaka/JahSource
message( "Configuring JahWidgets" )
SUBDIRS += source/Jahshaka/JahWidgets

message( "Configuring Jahshaka Linker" )
SUBDIRS += source

message( "" )
message( "Finished Configuring Subsystems" )
message( "-------------------------------" )
message( "" )

message( "Configuring Core Libraries" )
SUBDIRS +=  jah/Source \
            jah/Core/Objects \
            jah/Core/Render \
            jah/Core/World

message( "Configuring Standard Libraries" )
SUBDIRS +=  jah/libraries/jahtracer \
            jah/libraries/openmedialib/assetobject \
            jah/libraries/openmedialib/imagelibs \
            jah/libraries/jahkeyframes \
            jah/libraries/openmedialib/mediaobject \
            
            jah/libraries/glew \
            
            jah/libraries/jahthemes \
            jah/libraries/jahtimer \
            jah/libraries/jahpreferences \
            jah/libraries/jahtranslate \
            jah/libraries/GLCore \
            jah/libraries/jahplayer \
            jah/libraries/jahdatabase

message( "Configuring UI Widgets" )
SUBDIRS +=  jah/uiwidgets/interfaceobjs \
            jah/uiwidgets/jahcalc \
            jah/uiwidgets/jahfileloader


SUBDIRS +=  jah/desktop \
            jah/player

#files for audio support
contains( JAHAUDIO,true ) {
message( "Configuring Audio Support" )
SUBDIRS +=  jah/libraries/jahaudio  jah/libraries/jahaudio/G72x jah/libraries/jahaudio/GSM610
}



#files for sqlite database support
message( "Configuring Sqlite databse Support" )
SUBDIRS +=  jah/sqlite

#files for layer object support
message( "Configuring layer objects Support" )
SUBDIRS +=  jah/objectlibs/FTGL \
            jah/objectlibs/particle \
            jah/objectlibs/blur

#files for the main modules
message( "Configuring Jahshaka Modules" )
SUBDIRS +=  jah/Modules/glplayer

message( "Configuring Media Support" )
message( "-------------------------" )


#files for mpeg decoder on linux
contains( JAHMPEGDECODER,true ) {
message( "Configuring MPEG decoding Support" )
SUBDIRS += jah/encoders/mpegdec
}

#files for Avi support on linux
contains( LINUXAVISUPPORT,true ) {
message( "Configuring linux Avi Support" )
}

#files for Avi support on linux
contains( JAHTHEMES,true ) {
message( "Using Qt Theme Support" )
}

SUBDIRS += jah


TEMPLATE = subdirs
#CONFIG  += release  warn_on  qt  opengl  thread
message( " " )
message( "Make files have been configured" )
message( "type make to build jahshaka" )
message( "" )
message( "Once jahplayer has compiled" )
message( "type ./jahplayer to run" )
message( "" )
message( "please report bugs on www.jahplayer.com" )
message( "forums->bugs" )
message( "" )
message( "known bugs" )
message( "----------" )
message( "" )
message( "Q: If jahshaka crashes on startup its probably the database" )
message( "A: heres a quick fix" )
message( "cd to the jahshaka home directory then" )
message( "cd database" )
message( "then type ./wipedbase to clear it out" )
message( "your assets will still be on disk but you will have to" )
message( "re-import them onto your desktop" )


###########################################
#installation options, currently incomplete
#we need to make sure all the necessary folders are copied over
###########################################

#!mac:unix:TARGET = DSIC
#!mac:unix:target.path = /usr/lib
#!mac:unix:INSTALLS += target

###########################################
#the binary goes in /usr/bin
#we really need to strip the binary
#but dont know how to do it...

#for some reason we need to add this for unix
QMAKE_INSTALL_FILE = cp
QMAKE_INSTALL_DIR =  cp -r

unix:jahtarget.path = /usr/bin
unix:jahtarget.files = jahplayer

unix:INSTALLS = jahtarget

##########################################
#everything else goes in /usr/share/jahshaka

unix:pixmapsdir.path    = /usr/share/jahshaka/Pixmaps
unix:pixmapsdir.files   = Pixmaps/*
unix:INSTALLS          += pixmapsdir

unix:fontsdir.path      = /usr/share/jahshaka/fonts
unix:fontsdir.files     = fonts/*
unix:INSTALLS          += fontsdir

unix:scenesdir.path     = /usr/share/jahshaka/scenes
unix:scenesdir.files    = scenes/*
unix:INSTALLS          += scenesdir

unix:mediadir.path      = /usr/share/jahshaka/media
unix:mediadir.files     = media/*
unix:INSTALLS          += mediadir

unix:databasedir.path   = /usr/share/jahshaka/database
unix:databasedir.files  = database/JahplayerDesktopDB
unix:INSTALLS          += databasedir

###################################################
#need to move this into a more user friendly install location
unix:shaderdir.path     = /usr/share/jahshaka/jah/libraries/shader
unix:shaderdir.files    = jah/libraries/shader/*
unix:INSTALLS          += shaderdir

unix:pixshaderdir.path  = /usr/share/jahshaka/jah/objectlibs/pixelshaders
unix:pixshaderdir.files = jah/objectlibs/pixelshaders/*
unix:INSTALLS          += pixshaderdir

