@echo off

rem #####################################################
rem # Jahshaka configuratoin script
rem # sets up for either jahshaka or jahplayer build
rem #####################################################

rem These should be obtained from the command line options
echo JAHDEBUG=true > config.pro
echo JAHSTATIC=false >> config.pro
echo JAHPREFIX= >> config.pro
echo JAHPLUGINBUILD=true >> config.pro

if (%1)==() goto configureJahshaka
if (%1)==(jahshaka) goto configureJahshaka
if (%1)==(jahplayer) goto configureJahplayer
if (%1)==(/?) goto showHelp

:configureJahshaka
    echo Configuring build type for jahshaka
    echo.
    copy /Y jahshakaSettings.pro Settings.pro
    qmake jahshaka.pro
	echo BUILDTYPE=jahshaka >> config.pro
    goto out

:configureJahplayer
    echo Configuring build type for jahplayer
    echo.
    copy /Y jahplayerSettings.pro Settings.pro
    qmake jahplayer.pro
	echo BUILDTYPE=jahplayer >> config.pro
    goto out	

:showHelp
    echo ---------------------------------------
    echo Jahshaka Configure Script User Commands
    echo ---------------------------------------
    echo.  
    echo SYNOPSIS
    echo      configure [OPTION]
    echo 
    echo DESCRIPTION
    echo      Configures the jahshaka build process to build either
    echo      jahshaka or jahplayer
    echo.      
    echo      By default running configure with no options will build jahshaka
    echo.
    echo      You need to follow the configure command with the make command
    echo      in order to actually build the software!
    echo.      
    echo OPTIONS
    echo      configure
    echo           sets up the build process for jahshaka
    echo
    echo      configure jahshaka
    echo           sets up the build process for jahshaka
    echo.
    echo      configure jahplayer
    echo           sets up the build process for jahplayer
    echo.
    echo JAHSHAKA PLUGINS
    echo.
    echo      If the plugin build process is not set to active
    echo      then the plugins will not be built by default
    echo. 
    echo      In order to build the plugins you will need to run the configure script
    echo      in the plugin directory after building jahshaka
    echo.
    echo      ie:
    echo      >cd plugins
    echo      >configure
    echo      >nmake
    echo.
    echo ---------------------------------------
    
:out
