;--------------------------------
;NSIS Jahshaka Installation Script
;--------------------------------

!ifndef VERSION
  !define VERSION 2.0
!endif

;--------------------------------
;Configuration

!ifdef OUTFILE
  OutFile "${OUTFILE}"
!else
  OutFile jahshaka-${VERSION}-installer.exe
!endif

SetCompressor /SOLID lzma

InstType "Full"

InstallDir $PROGRAMFILES\Jahshaka
InstallDirRegKey HKLM Software\Jahshaka ""

;--------------------------------
;Header Files

!define ALL_USERS

!include "MUI.nsh"
!include "Sections.nsh"
!include "add_to_path.nsh"
!include "write_env_str.nsh"

;Names
Name "Jahshaka"
Caption "Jahshaka (v.${VERSION}) Setup"

;Interface Settings
!define MUI_ABORTWARNING

!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "jahshaka.bmp" ; optional
!define MUI_WELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\orange.bmp"

!define MUI_COMPONENTSPAGE_SMALLDESC

;Pages
!define MUI_WELCOMEPAGE_TITLE "Welcome to Jahshaka (v.${VERSION}) Setup Wizard"
!define MUI_WELCOMEPAGE_TEXT "This wizard will guide you through the installation of Jahshaka.\r\n\r\n$_CLICK"

!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "..\COPYING"
!insertmacro MUI_PAGE_COMPONENTS
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_LINK "Visit the web site for the latest news, FAQs and support"
!define MUI_FINISHPAGE_LINK_LOCATION "http://www.jahshaka.org/"

!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
;Languages

!insertmacro MUI_LANGUAGE "English"

Function .onInit
    ReadRegStr $R0 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\{D9A0D2AC-24F9-4D99-9B68-BD0A4F95A4C4}" "InstallLocation"
    StrCmp $R0 "" check_for_old_nsis_installer
    MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION \
    "An older installshield installed version of Jahshaka is already installed. $\n$\nClick `OK` to remove the \
    previous version or `Cancel` to cancel this upgrade." \
    IDOK uninst_installshield_installation
    Abort
    
  uninst_installshield_installation:
    ClearErrors
    SetDetailsPrint textonly
    DetailPrint "Uninstalling old InstallShield installed jahshaka installation..."
    SetDetailsPrint listonly
    ExecWait 'MsiExec.exe /x{D9A0D2AC-24F9-4D99-9B68-BD0A4F95A4C4}'
    IfErrors no_remove_uninstaller
        goto check_for_old_nsis_installer
  
  check_for_old_nsis_installer:
    ReadRegStr $R0 HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Jahshaka" "UninstallString"
    StrCmp $R0 "" done
    MessageBox MB_OKCANCEL|MB_ICONEXCLAMATION \
    "An older nsis installed version of Jahshaka is already installed. $\n$\nClick `OK` to remove the \
    previous version or `Cancel` to cancel this upgrade." \
    IDOK uninst_nsis_installation
    Abort
 
  uninst_nsis_installation:
    ClearErrors
    SetDetailsPrint textonly
    DetailPrint "Uninstalling old NSIS installed jahshaka installation..."
    SetDetailsPrint listonly
    ExecWait '$R0 _?=$INSTDIR'
      IfErrors no_remove_uninstaller
        goto done
        
  no_remove_uninstaller:
    MessageBox MB_ICONEXCLAMATION \
    "Unable to remove previous version of Jahshaka$\nTry uninstalling via the control panel."
    Abort
  
  done:
FunctionEnd

Section "Jahshaka" SecCore

  SectionIn 1 2 3 RO
  SetOutPath $INSTDIR

  SetOverwrite on
  SetShellVarContext all
  
;----------------------------------------------------------------------------------------
  SetDetailsPrint textonly
  DetailPrint "Installing GTK2..."
  SetDetailsPrint listonly

  ; gtk2
  SetOutPath $INSTDIR\..\gtk2
  File /r "C:\Program Files\gtk2\*"
  
  SetDetailsPrint both

  Push "$INSTDIR\..\gtk2\bin"
  Call AddToPath


;----------------------------------------------------------------------------------------
  SetDetailsPrint textonly
  DetailPrint "Installing MLT..."
  SetDetailsPrint listonly

  ; MLT
  SetOutPath $INSTDIR\..\mlt
  File /r "C:\Program Files\mlt\*"
  
  SetDetailsPrint both

  ; Path environment variable
  Push "$INSTDIR\..\mlt\bin"
  Call AddToPath

  ; Include environment variable
  Push "MLT_REPOSITORY"
  Push "$INSTDIR\..\mlt\share\mlt\modules"
  Call WriteEnvStr

;----------------------------------------------------------------------------------------
  SetDetailsPrint textonly
  DetailPrint "Installing Jahshaka..."
  SetDetailsPrint listonly

  SetOutPath $INSTDIR
  File C:\windows\system32\glew32.dll
  File C:\windows\system32\glut32.dll
  File C:\windows\system32\msvcr71.dll
  File C:\windows\system32\msvcp71.dll
  File ..\jahshaka.exe
  File ..\JahshakaCommunity.url
  File ..\opengpulib.dll
  File C:\Qt\3.3.6\bin\qt-mt336.dll
  File openlibraries-0.3.0-runtime.exe
  File OpenALwEAX.exe
  File jah-0.1.0-installer.exe
  
  SetOutPath $INSTDIR\database
  File ..\database\instructions.txt
  File ..\database\sqlite3.exe
  File ..\database\wipedbase
  
  SetOutPath $INSTDIR\fonts
  File ..\fonts\arial.ttf
  File ..\fonts\comic.ttf
  File ..\fonts\Gemelli.ttf
  File ..\fonts\GesseleRegular.ttf
  File ..\fonts\Herbert.ttf
  File ..\fonts\impact.ttf
  File ..\fonts\times.ttf

  SetOutPath $INSTDIR\media

  SetOutPath $INSTDIR\media\encoded
  
  SetOutPath $INSTDIR\media\images
  File ..\media\images\beck.bmp
  File ..\media\images\bird.png
  File ..\media\images\code.bmp
  File ..\media\images\coppertex.bmp
  File ..\media\images\earthtex.jpg
  File ..\media\images\espn.png
  File ..\media\images\espninterface.png
  File ..\media\images\fantasy.jpg
  File ..\media\images\font.png
  File ..\media\images\glasstex.bmp
  File ..\media\images\jahlogo.png
  File ..\media\images\jupiter.jpg
  File ..\media\images\sky.bmp
  File ..\media\images\starfield.jpg
  
  SetOutPath $INSTDIR\media\models
  File ..\media\models\f-16.obj

  SetOutPath $INSTDIR\media\models\disco_lighting
  File ..\media\models\disco_lighting\_0.frag
  File ..\media\models\disco_lighting\_0.vert
  File ..\media\models\disco_lighting\_1.frag
  File ..\media\models\disco_lighting\_1.vert
  File ..\media\models\disco_lighting\_2.frag
  File ..\media\models\disco_lighting\_2.vert
  File ..\media\models\disco_lighting\Fieldstone.tga
  File ..\media\models\disco_lighting\FieldstoneBumpDOT3.tga
  File ..\media\models\disco_lighting\LightCube.dds
  File ..\media\models\disco_lighting\test.x3d
  File ..\media\models\disco_lighting\Tiles.dds
  File ..\media\models\disco_lighting\TilesDOT3.tga
  
  SetOutPath $INSTDIR\media\renders
  
  SetOutPath $INSTDIR\media\renders\images
  
  SetOutPath $INSTDIR\media\share
  
  SetOutPath $INSTDIR\Pixmaps
  File ..\Pixmaps\f-16.mtl
  File ..\Pixmaps\face.3DS
  File ..\Pixmaps\face.bmp
  File ..\Pixmaps\jah-emptymediatable.png
  File ..\Pixmaps\jahaudio.png
  File ..\Pixmaps\jahback.jpg
  File ..\Pixmaps\jahicon.ico
  File ..\Pixmaps\jahicons.psd
  File ..\Pixmaps\jahlayer.bmp
  File ..\Pixmaps\jahlayer.png
  File ..\Pixmaps\jahlogo.png
  File ..\Pixmaps\jahshaka.icns
  File ..\Pixmaps\jahstartupimage.png
  File ..\Pixmaps\jahtext.png
  File ..\Pixmaps\missingmedia.bmp
  File ..\Pixmaps\missingmedia.png
  File ..\Pixmaps\no_tr.gif
  File ..\Pixmaps\trans.gif
  
  SetOutPath $INSTDIR\Pixmaps\desktop
  File ..\Pixmaps\desktop\advance-one-frame-mini.png
  File ..\Pixmaps\desktop\back-one-frame-mini.png
  File ..\Pixmaps\desktop\binview.png
  File ..\Pixmaps\desktop\desktopback.png
  File ..\Pixmaps\desktop\desktopfforward.png
  File ..\Pixmaps\desktop\desktopforward.png
  File ..\Pixmaps\desktop\desktopplay-old.png
  File ..\Pixmaps\desktop\desktopplay.png
  File ..\Pixmaps\desktop\desktoprreverse.png
  File ..\Pixmaps\desktop\desktopstop.png
  File ..\Pixmaps\desktop\forward-to-end-mini.png
  File ..\Pixmaps\desktop\grey-advance-one-frame-mini.png
  File ..\Pixmaps\desktop\grey-back-one-frame-mini.png
  File ..\Pixmaps\desktop\grey-forward-to-end-mini.png
  File ..\Pixmaps\desktop\grey-play-mini.png
  File ..\Pixmaps\desktop\grey-rewind-to-beginning-mini.png
  File ..\Pixmaps\desktop\jah-bighead-canvasbg.png
  File ..\Pixmaps\desktop\jahnodes.png
  File ..\Pixmaps\desktop\node-styled.png
  File ..\Pixmaps\desktop\phototool.png
  File ..\Pixmaps\desktop\play-mini.png
  File ..\Pixmaps\desktop\rendertool.png
  File ..\Pixmaps\desktop\rewind-to-beginning-mini.png
  File ..\Pixmaps\desktop\rotatetool.png
  File ..\Pixmaps\desktop\scaletool.png
  File ..\Pixmaps\desktop\simplezoomin.png
  File ..\Pixmaps\desktop\simplezoomout.png
  File ..\Pixmaps\desktop\stop-mini.png
  File ..\Pixmaps\desktop\thumbview.png
  File ..\Pixmaps\desktop\transtool.png
  File ..\Pixmaps\desktop\widenode-styled-green.png
  File ..\Pixmaps\desktop\widenode-styled.png
  File ..\Pixmaps\desktop\zoomin.png
  File ..\Pixmaps\desktop\zoomout.png
  File ..\Pixmaps\desktop\eye-closed.png
  File ..\Pixmaps\desktop\eye-open.png

  SetOutPath $INSTDIR\Pixmaps\desktopController
  File ..\Pixmaps\desktopController\delete_off.png
  File ..\Pixmaps\desktopController\delete_on.png
  File ..\Pixmaps\desktopController\load_off.png
  File ..\Pixmaps\desktopController\load_on.png
  File ..\Pixmaps\desktopController\name_off.png
  File ..\Pixmaps\desktopController\name_on.png
  File ..\Pixmaps\desktopController\play_off.png
  File ..\Pixmaps\desktopController\play_on.png
  
  SetOutPath $INSTDIR\Pixmaps\interface
  File ..\Pixmaps\interface\charcoal.xpm
  File ..\Pixmaps\interface\marble.xpm
  File ..\Pixmaps\interface\metal.xpm
  File ..\Pixmaps\interface\metaldark.xpm
  File ..\Pixmaps\interface\oldmetal.xpm
  File ..\Pixmaps\interface\stone1.xpm
  File ..\Pixmaps\interface\stonebright.xpm
  File ..\Pixmaps\interface\stonedark.xpm
  
  SetOutPath $INSTDIR\Pixmaps\Jahplayer
  File ..\Pixmaps\Jahplayer\playerdelete.png
  File ..\Pixmaps\Jahplayer\playerdeleteon.png
  File ..\Pixmaps\Jahplayer\playerload.png
  File ..\Pixmaps\Jahplayer\playerloadon.png
  File ..\Pixmaps\Jahplayer\playername.png
  File ..\Pixmaps\Jahplayer\playernameon.png
  File ..\Pixmaps\Jahplayer\playerplay.png
  File ..\Pixmaps\Jahplayer\playerplayon.png
  
  SetOutPath $INSTDIR\Pixmaps\modules
  File ..\Pixmaps\modules\light.png
  File ..\Pixmaps\modules\reflect.png
  File ..\Pixmaps\modules\stop.png
  
  SetOutPath $INSTDIR\Pixmaps\network
  File ..\Pixmaps\network\background.png
  File ..\Pixmaps\network\error.html
  File ..\Pixmaps\network\offline.html
  File ..\Pixmaps\network\offline.png
  
  SetOutPath $INSTDIR\Pixmaps\paint
  File ..\Pixmaps\paint\buttonBezier.png
  File ..\Pixmaps\paint\buttonBucket.png
  File ..\Pixmaps\paint\buttonCircle.png
  File ..\Pixmaps\paint\buttonCircleFilled.png
  File ..\Pixmaps\paint\buttonDropper.png
  File ..\Pixmaps\paint\buttonErase.png
  File ..\Pixmaps\paint\buttonEraser.png
  File ..\Pixmaps\paint\buttonFillAllBucket.png
  File ..\Pixmaps\paint\buttonForegroundDropper.png
  File ..\Pixmaps\paint\buttonHistoryNext.png
  File ..\Pixmaps\paint\buttonHistoryPrevious.png
  File ..\Pixmaps\paint\buttonLine.png
  File ..\Pixmaps\paint\buttonPen.png
  File ..\Pixmaps\paint\buttonRectangle.png
  File ..\Pixmaps\paint\buttonRectangleFilled.png
  File ..\Pixmaps\paint\buttonTriangle.png
  File ..\Pixmaps\paint\buttonTriangleFilled.png
  
  SetOutPath $INSTDIR\Pixmaps\player
  File ..\Pixmaps\player\ffoward_off.png
  File ..\Pixmaps\player\ffoward_on.png
  File ..\Pixmaps\player\foward_off.png
  File ..\Pixmaps\player\foward_on.png
  File ..\Pixmaps\player\frewind_on.png
  File ..\Pixmaps\player\frewind_off.png
  File ..\Pixmaps\player\keyback_off.png
  File ..\Pixmaps\player\keyback_on.png
  File ..\Pixmaps\player\keyforward_off.png
  File ..\Pixmaps\player\keyforward_on.png
  File ..\Pixmaps\player\keyminus_off.png
  File ..\Pixmaps\player\keyminus_on.png
  File ..\Pixmaps\player\keyplus_off.png
  File ..\Pixmaps\player\keyplus_on.png
  File ..\Pixmaps\player\play_off.png
  File ..\Pixmaps\player\play_on.png
  File ..\Pixmaps\player\reflect.png
  File ..\Pixmaps\player\rewind_off.png
  File ..\Pixmaps\player\rewind_on.png
  File ..\Pixmaps\player\stop_off.png
  File ..\Pixmaps\player\stop_on.png
  
  SetOutPath $INSTDIR\Pixmaps\widgets
  File ..\Pixmaps\widgets\collapse-close.png
  File ..\Pixmaps\widgets\collapse-open.png
  File ..\Pixmaps\widgets\lock.png
  File ..\Pixmaps\widgets\plotbg.png
  File ..\Pixmaps\widgets\reset-magnification.png
  File ..\Pixmaps\widgets\silence.png
  File ..\Pixmaps\widgets\no-silence.png
  File ..\Pixmaps\widgets\tick.png
  File ..\Pixmaps\widgets\unlock.png
  File ..\Pixmaps\widgets\zoom-in.png
  
  SetOutPath $INSTDIR\Pixmaps\wireup
  File ..\Pixmaps\wireup\fxnode.png
  File ..\Pixmaps\wireup\fxnode1.png
  File ..\Pixmaps\wireup\fxnode2.png
  File ..\Pixmaps\wireup\green-glow.png
  File ..\Pixmaps\wireup\grid-for-canvas.png
  File ..\Pixmaps\wireup\newnode.png
  File ..\Pixmaps\wireup\supernewnode.png
  File ..\Pixmaps\wireup\widenode-styled-green.png

  SetOutPath $INSTDIR\plugins

  SetOutPath $INSTDIR\plugins\editingfx
  File ..\plugins\editingfx\bad_picture.jfx
  File ..\plugins\editingfx\bad_picture.jpg
  File ..\plugins\editingfx\charcoal.jfx
  File ..\plugins\editingfx\charcoal.jpg
  File ..\plugins\editingfx\chroma.jfx
  File ..\plugins\editingfx\chroma.jpg
  File ..\plugins\editingfx\chroma_hold.jfx
  File ..\plugins\editingfx\chroma_hold.jpg
  File ..\plugins\editingfx\colour_blue.jfx
  File ..\plugins\editingfx\colour_green.jfx
  File ..\plugins\editingfx\colour_red.jfx
  File ..\plugins\editingfx\drop_in.jfx
  File ..\plugins\editingfx\fly_in.jfx
  File ..\plugins\editingfx\fly_out.jfx
  File ..\plugins\editingfx\greyscale.jfx
  File ..\plugins\editingfx\greyscale.jpg
  File ..\plugins\editingfx\invert.jfx
  File ..\plugins\editingfx\invert.jpg
  File ..\plugins\editingfx\luma_in.jfx
  File ..\plugins\editingfx\luma_in.jpg
  File ..\plugins\editingfx\luma_out.jfx
  File ..\plugins\editingfx\luma_out.jpg
  File ..\plugins\editingfx\mirror.jfx
  File ..\plugins\editingfx\mirror.jpg
  File ..\plugins\editingfx\motion_blur.jfx
  File ..\plugins\editingfx\motion_blur.jpg
  File ..\plugins\editingfx\noise.jfx
  File ..\plugins\editingfx\oval.jfx
  File ..\plugins\editingfx\oval.jpg
  File ..\plugins\editingfx\oval.png
  File ..\plugins\editingfx\sepia.jfx
  File ..\plugins\editingfx\sepia.jpg
  File ..\plugins\editingfx\text.jfx
  File ..\plugins\editingfx\mono.jpg
  File ..\plugins\editingfx\mono.jfx
  
  SetOutPath $INSTDIR\plugins\encoders
  File ..\plugins\encoders\divx.jpl
  File ..\plugins\encoders\dv.jpl
  File ..\plugins\encoders\jpg.jpl
  File ..\plugins\encoders\libdv.jpl
  File ..\plugins\encoders\mp2.jpl
  File ..\plugins\encoders\mpg.jpl
  File ..\plugins\encoders\png.jpl
  File ..\plugins\encoders\psp.jpl
  File ..\plugins\encoders\swf.jpl
  
  SetOutPath $INSTDIR\plugins\jfxplugins
  File ..\plugins\jfxplugins\vfxbathroom.dll
  File ..\plugins\jfxplugins\vfxfisheye.dll
  File ..\plugins\jfxplugins\vfxgrey.dll
  File ..\plugins\jfxplugins\vfxmandelbrot.dll
  File ..\plugins\jfxplugins\vfxmosaic.dll
  File ..\plugins\jfxplugins\vfxoil.dll
  File ..\plugins\jfxplugins\vfxpolarnoise.dll
  File ..\plugins\jfxplugins\vfxpolarwarp.dll
  File ..\plugins\jfxplugins\vfxrelief.dll
  File ..\plugins\jfxplugins\vfxswirl.dll
  
  SetOutPath $INSTDIR\plugins\jitplugins
  File ..\plugins\jitplugins\jitaverage.dll
  File ..\plugins\jitplugins\jitfftfilter.dll
  File ..\plugins\jitplugins\jitgaussnoise.dll
  File ..\plugins\jitplugins\jitgeommeanfilter.dll
  File ..\plugins\jitplugins\jitmedianfilter.dll
  File ..\plugins\jitplugins\jitminmaxfilter.dll
  File ..\plugins\jitplugins\jitmmsefilter.dll
  File ..\plugins\jitplugins\jitnegexnoise.dll
  File ..\plugins\jitplugins\jitpseudocolor.dll
  File ..\plugins\jitplugins\jitrangefilter.dll
  File ..\plugins\jitplugins\jitrgbvary.dll
  
  SetOutPath $INSTDIR\plugins\rfxplugins
  File ..\plugins\rfxplugins\rfxnvblur.dll
  File ..\plugins\rfxplugins\rfxnvchannelblur.dll
  File ..\plugins\rfxplugins\rfxnvcharcoal.dll
  File ..\plugins\rfxplugins\rfxnvcolor.dll
  File ..\plugins\rfxplugins\rfxnvcompoundeye.dll
  File ..\plugins\rfxplugins\rfxnvdiffusion.dll
  File ..\plugins\rfxplugins\rfxnvdistortion.dll
  File ..\plugins\rfxplugins\rfxnvedgedetect.dll
  File ..\plugins\rfxplugins\rfxnvfire.dll
  File ..\plugins\rfxplugins\rfxnvfisheye.dll
  File ..\plugins\rfxplugins\rfxnvflame.dll
  File ..\plugins\rfxplugins\rfxnvfog.dll
  File ..\plugins\rfxplugins\rfxnvlighting.dll
  File ..\plugins\rfxplugins\rfxnvmosaic.dll
  File ..\plugins\rfxplugins\rfxnvnorthernlights.dll
  File ..\plugins\rfxplugins\rfxnvpan.dll
  File ..\plugins\rfxplugins\rfxnvreflection.dll
  File ..\plugins\rfxplugins\rfxnvsharpen.dll
  File ..\plugins\rfxplugins\rfxnvsphere.dll
  File ..\plugins\rfxplugins\rfxnvswirl.dll
  File ..\plugins\rfxplugins\rfxnvturbulence.dll

  SetOutPath $INSTDIR\plugins\rfxplugins\rfxcore
  File ..\plugins\rfxplugins\rfxcore\create_turbulence_texture_combiner_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxcore\create_turbulence_texture_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxcore\create_turbulence_texture_vert_gpu.vp
  File ..\plugins\rfxplugins\rfxcore\jahshaka_basic_vert.vp
  File ..\plugins\rfxplugins\rfxcore\jahshaka_basic_vert_arb.vp
  File ..\plugins\rfxplugins\rfxcore\turbulence_combiner_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvblur
  File ..\plugins\rfxplugins\rfxnvblur\blur10_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur10_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur15_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur15_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur1_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur2_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur3_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur3_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur7_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur7_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvblur\blur_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvchannelblur
  File ..\plugins\rfxplugins\rfxnvchannelblur\channelblur_frag.fp
  File ..\plugins\rfxplugins\rfxnvchannelblur\channelblur_frag_arb.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvcharcoal
  File ..\plugins\rfxplugins\rfxnvcharcoal\charcoal_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvcharcoal\charcoal_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvcolor
  File ..\plugins\rfxplugins\rfxnvcolor\color_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvcolor\color_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvcompoundeye
  File ..\plugins\rfxplugins\rfxnvcompoundeye\fisheye_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvcompoundeye\fisheye_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvdiffusion
  File ..\plugins\rfxplugins\rfxnvdiffusion\diffusion_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvdiffusion\diffusion_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvdistortion
  File ..\plugins\rfxplugins\rfxnvdistortion\distortion_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvdistortion\distortion_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvdistortion\distortion_vert_arb_gpu.vp
  File ..\plugins\rfxplugins\rfxnvdistortion\distortion_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvedgedetect
  File ..\plugins\rfxplugins\rfxnvedgedetect\edgedetect_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvedgedetect\edgedetect_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvfire
  File ..\plugins\rfxplugins\rfxnvfire\fire_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvfire\fire_vert_gpu.vp

  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvfire\images
  File ..\plugins\rfxplugins\rfxnvfire\images\Fire.bmp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvfisheye
  File ..\plugins\rfxplugins\rfxnvfisheye\fisheye_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvfisheye\fisheye_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvflame
  File ..\plugins\rfxplugins\rfxnvflame\flame_blur_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvflame\flame_combiner_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvflame\flame_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvflame\flame_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvfog
  File ..\plugins\rfxplugins\rfxnvfog\fog_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvfog\fog_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvfog\images
  File ..\plugins\rfxplugins\rfxnvfog\images\Clouds.bmp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvlighting
  File ..\plugins\rfxplugins\rfxnvlighting\lighting_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvlighting\lighting_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvmosaic
  File ..\plugins\rfxplugins\rfxnvmosaic\mosaic_frag.fp
  File ..\plugins\rfxplugins\rfxnvmosaic\mosaic_frag_arb.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvnorthernlights
  File ..\plugins\rfxplugins\rfxnvnorthernlights\northernlights_combiner_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvnorthernlights\northernlights_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvnorthernlights\northernlights_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvpan
  File ..\plugins\rfxplugins\rfxnvpan\pan_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvpan\pan_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvreflection
  File ..\plugins\rfxplugins\rfxnvreflection\diffusion_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvreflection\diffusion_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvsharpen
  File ..\plugins\rfxplugins\rfxnvsharpen\edgedetect_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvsharpen\edgedetect_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvsphere
  File ..\plugins\rfxplugins\rfxnvsphere\sphere_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvsphere\sphere_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvswirl
  File ..\plugins\rfxplugins\rfxnvswirl\swirl_frag_arb_gpu.fp
  File ..\plugins\rfxplugins\rfxnvswirl\swirl_frag_gpu.fp
  
  SetOutPath $INSTDIR\plugins\rfxplugins\rfxnvturbulence
  File ..\plugins\rfxplugins\rfxnvturbulence\turbulence_combiner_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvturbulence\turbulence_frag_gpu.fp
  File ..\plugins\rfxplugins\rfxnvturbulence\turbulence_vert_gpu.vp
  
  SetOutPath $INSTDIR\plugins\rtplugins
  File ..\plugins\rtplugins\rtripple.dll
  
  SetOutPath $INSTDIR\scenes
  File ..\scenes\animtest.jsf
  File ..\scenes\espn.jsf
  
  SetOutPath $INSTDIR\source
  
  SetOutPath $INSTDIR\source\Jahshaka
  
  SetOutPath $INSTDIR\source\Jahshaka\JahLibraries
  
  SetOutPath $INSTDIR\source\Jahshaka\JahLibraries\shaders
  File ..\source\Jahshaka\JahLibraries\shaders\blur.fp
  File ..\source\Jahshaka\JahLibraries\shaders\charcoal_frag_gpu.fp
  File ..\source\Jahshaka\JahLibraries\shaders\charcoal_vert_gpu.vp
  File ..\source\Jahshaka\JahLibraries\shaders\color_frag_arb_gpu.fp
  File ..\source\Jahshaka\JahLibraries\shaders\color_frag_gpu.fp
  File ..\source\Jahshaka\JahLibraries\shaders\earthquake_frag_gpu.fp
  File ..\source\Jahshaka\JahLibraries\shaders\earthquake_vert_gpu.vp
  File ..\source\Jahshaka\JahLibraries\shaders\fire3D_frag_gpu.fp
  File ..\source\Jahshaka\JahLibraries\shaders\fire3D_vert_gpu.vp
  File ..\source\Jahshaka\JahLibraries\shaders\lighting_frag_gpu.fp
  File ..\source\Jahshaka\JahLibraries\shaders\lighting_vert_gpu.vp
  File ..\source\Jahshaka\JahLibraries\shaders\post.fp
  File ..\source\Jahshaka\JahLibraries\shaders\program1.vp
  File ..\source\Jahshaka\JahLibraries\shaders\program2.vp
  File ..\source\Jahshaka\JahLibraries\shaders\swirl_frag_arb_gpu.fp
  File ..\source\Jahshaka\JahLibraries\shaders\swirl_frag_gpu.fp
  
  SetOutPath $INSTDIR\source\Jahshaka\JahLibraries\shaders\images
  File ..\source\Jahshaka\JahLibraries\shaders\images\Clouds.bmp
  File ..\source\Jahshaka\JahLibraries\shaders\images\Clouds.jpg
  File ..\source\Jahshaka\JahLibraries\shaders\images\Fire.bmp

  SetDetailsPrint both
  
  SetOutPath $INSTDIR
  CreateDirectory "$SMPROGRAMS\Jahshaka"
  CreateShortCut "$SMPROGRAMS\Jahshaka\Jahshaka Community.lnk" "$PROGRAMFILES\Internet Explorer\iexplore.exe" "http://www.jahshaka.org"
  ;CreateShortCut "$SMPROGRAMS\Jahshaka\Jahplayer.lnk" "$INSTDIR\jahplayer.exe"
  CreateShortCut "$SMPROGRAMS\Jahshaka\Jahshaka Debug.lnk" "$INSTDIR\jahshaka.exe" "debug file"
  CreateShortCut "$SMPROGRAMS\Jahshaka\Jahshaka.lnk" "$INSTDIR\jahshaka.exe"
  CreateShortCut "$DESKTOP\Jahshaka.lnk" "$INSTDIR\jahshaka.exe"

  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Jahshaka" "DisplayName" "Jahshaka"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Jahshaka" "UninstallString" "$INSTDIR\uninst-jahshaka.exe"

;----------------------------------------------------------------------------------------
  
  ;SetOutPath $INSTDIR
  ;SetShellVarContext current
  
  SetDetailsPrint textonly
  DetailPrint "Installing OpenAL..."
  SetDetailsPrint listonly

  ExecWait '"$INSTDIR\openalweax.exe"'

;----------------------------------------------------------------------------------------
  
  SetDetailsPrint textonly
  DetailPrint "Installing OpenLibraries..."
  SetDetailsPrint listonly

  ExecWait '"$INSTDIR\openlibraries-0.3.0-runtime.exe"'

;----------------------------------------------------------------------------------------
  
  SetDetailsPrint textonly
  DetailPrint "Installing Jah..."
  SetDetailsPrint listonly

  ExecWait '"$INSTDIR\jah-0.1.0-installer.exe"'


SectionEnd

Section -post

	WriteUninstaller $INSTDIR\uninst-jahshaka.exe

SectionEnd

;--------------------------------
;Descriptions

!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
  !insertmacro MUI_DESCRIPTION_TEXT ${SecCore} "Jahshaka runtime files"
!insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section Uninstall

  SetDetailsPrint textonly
  DetailPrint "Deleting Files..."
  SetDetailsPrint listonly
  
  SetShellVarContext all

  ;-- Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\Jahshaka"
  
  ;-- Remove desktop and start->Programs->Jahshaka shortcuts
  Delete $DESKTOP\Jahshaka.lnk
  RMDir /r $SMPROGRAMS\Jahshaka

  ;-- Remove install directory
  RMDir /r $INSTDIR

  ;-- Remove MLT
  Push "MLT_REPOSITORY"
  Call un.DeleteEnvStr
  Push "$INSTDIR\..\mlt\bin"
  Call un.RemoveFromPath
  RMDir /r $INSTDIR\..\mlt
  
  ;-- Remove GTK2
  Push "$INSTDIR\..\gtk2\bin"
  Call un.RemoveFromPath
  RMDir /r $INSTDIR\..\gtk2
  
  ;-- Remove Jah
  ExecWait '"$INSTDIR\..\Jah\uninst-jah.exe"'
  
  ;-- Remove OpenLibraries
  ReadEnvStr $0 PYTHONPATH
  ExecWait '"$0\..\uninst-openlibraries.exe"'
  
  SetDetailsPrint both
 
SectionEnd


