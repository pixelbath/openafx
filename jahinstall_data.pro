# $Id: jahinstall_data.pro,v 1.1.2.2 2005/11/21 18:09:13 ezequiel Exp $

isEmpty( JAHINSTALL_LOCALROOT ):error( "file not correctly included. define JAHINSTALL_LOCALROOT before including." )

##

#-? JAHINSTALL_INCLUDEFILE = $${JAHINSTALL_LOCALROOT}/jahinstall_proc.pro
JAHINSTALL_INCLUDEFILE = ../jahinstall_proc.pro

###
# jedit:
# . use super-abbreviation: ji
# . then search and replace:
#   WORKS:
#   . search: ^(\s*(?:INSTALLS\s*\+?=\s*)?[[:alnum:]_-]+)/
#   . replace: $1_
#   . options: restrict to selection. regular expressions, not hypersearch
#   . execute: replace all (do several times without closing the dialog)
#
#   leaves the "INSTALLS" section unchanged:
#   . search: ^(\s*[[:alnum:]_]+)/(.+=.+$)
#   . replace: $1_$2
#   . options: restrict to selection. regular expressions, not hypersearch
#     (do several times without closing the dialog)
#

# find subdirectories:
#  find -mindepth 1 -type d -a -not -name CVS | sort

###[ intallation of data files ]###
unix {

####
## data files
####

JAHINSTALL_FILEWILDCARDS = *.*

JAHINSTALL_DIRPATH = Pixmaps
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps.path += $${JAHINSTALL_TARGETPATH}
Pixmaps.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps

JAHINSTALL_DIRPATH = Pixmaps/desktop
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_desktop.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_desktop.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_desktop

JAHINSTALL_DIRPATH = Pixmaps/desktopController
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_desktopController.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_desktopController.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_desktopController

JAHINSTALL_DIRPATH = Pixmaps/interface
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_interface.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_interface.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_interface

JAHINSTALL_DIRPATH = Pixmaps/jahplayer
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_jahplayer.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_jahplayer.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_jahplayer

JAHINSTALL_DIRPATH = Pixmaps/modules
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_modules.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_modules.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_modules

JAHINSTALL_DIRPATH = Pixmaps/network
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_network.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_network.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_network

JAHINSTALL_DIRPATH = Pixmaps/paint
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_paint.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_paint.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_paint

JAHINSTALL_DIRPATH = Pixmaps/player
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_player.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_player.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_player

JAHINSTALL_DIRPATH = Pixmaps/widgets
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_widgets.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_widgets.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_widgets

JAHINSTALL_DIRPATH = Pixmaps/wireup
include( $${JAHINSTALL_INCLUDEFILE} )
Pixmaps_wireup.path += $${JAHINSTALL_TARGETPATH}
Pixmaps_wireup.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += Pixmaps_wireup

## fonts

JAHINSTALL_FILEWILDCARDS = *.ttf

JAHINSTALL_DIRPATH = fonts
include( $${JAHINSTALL_INCLUDEFILE} )
fonts.path += $${JAHINSTALL_TARGETPATH}
fonts.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += fonts

## scenes

JAHINSTALL_FILEWILDCARDS = *.jsf

JAHINSTALL_DIRPATH = scenes
include( $${JAHINSTALL_INCLUDEFILE} )
scenes.path += $${JAHINSTALL_TARGETPATH}
scenes.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += scenes

## media

# FIXME: installation of data files works this way, but if we use:
#  JAHINSTALL_FILEWILDCARDS = *
#  then it will try and install the subdierctories, too.
#  CVS directories are not supposed to be copied over.
JAHINSTALL_FILEWILDCARDS = *.* *dir

JAHINSTALL_DIRPATH = media
include( $${JAHINSTALL_INCLUDEFILE} )
media.path += $${JAHINSTALL_TARGETPATH}
media.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += media

JAHINSTALL_DIRPATH = media/encoded
include( $${JAHINSTALL_INCLUDEFILE} )
media_encoded.path += $${JAHINSTALL_TARGETPATH}
media_encoded.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += media_encoded

JAHINSTALL_DIRPATH = media/images
include( $${JAHINSTALL_INCLUDEFILE} )
media_images.path += $${JAHINSTALL_TARGETPATH}
media_images.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += media_images

JAHINSTALL_DIRPATH = media/models
include( $${JAHINSTALL_INCLUDEFILE} )
media_models.path += $${JAHINSTALL_TARGETPATH}
media_models.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += media_models

JAHINSTALL_DIRPATH = media/models/disco_lighting
include( $${JAHINSTALL_INCLUDEFILE} )
media_models_disco_lighting.path += $${JAHINSTALL_TARGETPATH}
media_models_disco_lighting.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += media_models_disco_lighting

JAHINSTALL_DIRPATH = media/renders
include( $${JAHINSTALL_INCLUDEFILE} )
media_renders.path += $${JAHINSTALL_TARGETPATH}
media_renders.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += media_renders

JAHINSTALL_DIRPATH = media/renders/images
include( $${JAHINSTALL_INCLUDEFILE} )
media_renders_images.path += $${JAHINSTALL_TARGETPATH}
media_renders_images.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += media_renders_images

JAHINSTALL_DIRPATH = media/share
include( $${JAHINSTALL_INCLUDEFILE} )
media_share.path += $${JAHINSTALL_TARGETPATH}
media_share.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += media_share

## databases

JAHINSTALL_FILEWILDCARDS = Jah*DB *.txt

JAHINSTALL_DIRPATH = database
include( $${JAHINSTALL_INCLUDEFILE} )
database.path += $${JAHINSTALL_TARGETPATH}
database.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += database

## shaders

JAHINSTALL_FILEWILDCARDS = *.fp *.vp

JAHINSTALL_DIRPATH = source/Jahshaka/JahLibraries/shaders
include( $${JAHINSTALL_INCLUDEFILE} )
source_Jahshaka_JahLibraries_shaders.path += $${JAHINSTALL_TARGETPATH}
source_Jahshaka_JahLibraries_shaders.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += source_Jahshaka_JahLibraries_shaders

JAHINSTALL_FILEWILDCARDS = *.*

JAHINSTALL_DIRPATH = source/Jahshaka/JahLibraries/shaders/images
include( $${JAHINSTALL_INCLUDEFILE} )
source_Jahshaka_JahLibraries_shaders_images.path += $${JAHINSTALL_TARGETPATH}
source_Jahshaka_JahLibraries_shaders_images.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += source_Jahshaka_JahLibraries_shaders_images

## plugins data

JAHINSTALL_FILEWILDCARDS = *.jfx *.jpg *.png

JAHINSTALL_DIRPATH = plugins/editingfx
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_editingfx.path += $${JAHINSTALL_TARGETPATH}
plugins_editingfx.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_editingfx

JAHINSTALL_FILEWILDCARDS = *.jpl

JAHINSTALL_DIRPATH = plugins/encoders
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_encoders.path += $${JAHINSTALL_TARGETPATH}
plugins_encoders.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_encoders

## plugins - rfxplugins

JAHINSTALL_FILEWILDCARDS = *.fp *.vp

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxcore
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxcore.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxcore.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxcore

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvblur
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvblur.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvblur.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvblur

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvchannelblur
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvchannelblur.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvchannelblur.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvchannelblur

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvcharcoal
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvcharcoal.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvcharcoal.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvcharcoal

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvcolor
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvcolor.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvcolor.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvcolor

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvcompoundeye
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvcompoundeye.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvcompoundeye.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvcompoundeye

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvdiffusion
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvdiffusion.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvdiffusion.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvdiffusion

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvdistortion
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvdistortion.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvdistortion.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvdistortion

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvedgedetect
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvedgedetect.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvedgedetect.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvedgedetect

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvfire
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvfire.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvfire.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvfire

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvfisheye
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvfisheye.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvfisheye.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvfisheye

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvflame
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvflame.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvflame.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvflame

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvfog
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvfog.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvfog.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvfog

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvlighting
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvlighting.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvlighting.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvlighting

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvmosaic
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvmosaic.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvmosaic.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvmosaic

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvnorthernlights
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvnorthernlights.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvnorthernlights.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvnorthernlights

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvpan
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvpan.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvpan.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvpan

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvreflection
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvreflection.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvreflection.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvreflection

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvsharpen
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvsharpen.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvsharpen.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvsharpen

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvsphere
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvsphere.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvsphere.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvsphere

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvswirl
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvswirl.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvswirl.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvswirl

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvturbulence
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvturbulence.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvturbulence.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvturbulence

### plugins images

JAHINSTALL_FILEWILDCARDS = *.bmp *.png *.jpg

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvfire/images
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvfire_images.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvfire_images.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvfire_images

JAHINSTALL_DIRPATH = plugins/rfxplugins/rfxnvfog/images
include( $${JAHINSTALL_INCLUDEFILE} )
plugins_rfxplugins_rfxnvfog_images.path += $${JAHINSTALL_TARGETPATH}
plugins_rfxplugins_rfxnvfog_images.files += $${JAHINSTALL_TARGETFILES}
INSTALLS += plugins_rfxplugins_rfxnvfog_images

###[ intallation of data files ]###
}

# ======================================================================

######
### installation of other files (manual)
######

###########################################
#installation options, currently incomplete
#we need to make sure all the necessary folders are copied over
###########################################

#!mac:unix:TARGET = DSIC
#!mac:unix:target.path = $$JAHPREFIX/lib
#!mac:unix:INSTALLS += target

###########################################
#the binary goes in $$JAHPREFIX/bin
#we really need to strip the binary
#but dont know how to do it...

unix {

jahtarget_bin.path = $$JAHPREFIX/bin
jahtarget_bin.files += $${JAHINSTALL_LOCALROOT}/jahuserinstall

INSTALLS += jahtarget_bin

}

# ======================================================================

false {

###################################################
#now we need to install the openlibraries

unix:openlibrarydir.path   = $$JAHPREFIX/share/jahshaka/OpenLibraries/lib
unix:openlibrarydir.files  = source/OpenLibraries/lib/*.so*
unix:INSTALLS             += openlibrarydir

unix:openobjectdir.path    = $$JAHPREFIX/share/jahshaka/source/OpenLibraries/openobjectlib/plugins/
unix:openobjectdir.files   = source/OpenLibraries/openobjectlib/plugins/*.so*
unix:INSTALLS             += openobjectdir

}

# ======================================================================

# eof

