###############################################################
#
# VisualMedia OpenLibraries QMake build files
#
###############################################################

include( ../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Configuring OpenLibraries" )
message( "-------------------------" )

SUBDIRS += opencore
SUBDIRS += openassetlib/v2_openassetlib/sqlite3
SUBDIRS += openassetlib/v2_openassetlib
SUBDIRS += openassetlib
SUBDIRS += opengpulib
SUBDIRS += openimagelib
SUBDIRS += openmedialib
SUBDIRS += opennetworklib

#need to consolidate this
SUBDIRS += openobjectlib
contains( OPENOBJECTLIBSUPPORT,false ) {
	SUBDIRS += openobjectlib/plugins
}

contains( JAHPLAYER,true ) {	
SUBDIRS -= opennetworklib
}

TEMPLATE = subdirs

