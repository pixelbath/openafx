###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE        =       lib

include ( ../lib_standard.pro )

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT,true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

HEADERS			=   	\
						openobjectlib.h \
						openobjectplugins.h \
						openobject.h

SOURCES         =   	\
						openobjectlib.cpp \
						openobjectplugins.cpp \
						openobject.cpp

TARGET          =       ../lib/openobjectlib
DEPENDPATH      =       $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH +=	. .. \
		../../Jahshaka/JahLibraries/jahpreferences \
        ../openimagelib \
        ../opencore


contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}
