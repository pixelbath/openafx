###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =	lib
CONFIG      +=	plugin 
HEADERS     =	\
				../openobjectplugintemplate.h \
				../openobject_plugincore.h \
				glm.h \
				dirent32.h \
				openobject_obj.h 

SOURCES     =   \
				glm.c \
				openobject_obj.cpp


TARGET      =	../oo_obj

DEPENDPATH  =	$$JAHDEPENDPATH


