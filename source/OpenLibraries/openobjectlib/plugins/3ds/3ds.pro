###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =	lib
CONFIG      +=	plugin 
HEADERS     =   \        
			../openobjectplugintemplate.h \
			../openobject_plugincore.h \
			l3ds.h \
			openobject_3ds.h 

SOURCES     =   \
	        l3ds.cpp \
			openobject_3ds.cpp


TARGET      =	../oo_3ds

#INCLUDEPATH =	../../../../Source
