###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../../jahshakaSettings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =   lib
CONFIG      +=	plugin 

HEADERS     =	\
				../openobjectplugintemplate.h \
				../openobject_plugincore.h \
				dxf.h \
				openobject_dxf.h 

SOURCES     =   \
				dxf.cpp \
				openobject_dxf.cpp


#win32:DEFINES	-=	JAH_DLL_EXPORT
#win32:LIBS		+=	surface3d.lib

#unix:LIBS		+=  $$JAH_SHARED_OBJECT_PATH/libsurface3d.so
#unix:TMAKE_CXXFLAGS_RELEASE  -= -rdynamic


TARGET      =   ../oo_dxf

DEPENDPATH  =   $$JAHDEPENDPATH

#!win32 {

LIBS +=         -L../../../lib		-lsurface3d

#}

INCLUDEPATH =	../../surface3d
LIBPATH		+=	../../../../../

