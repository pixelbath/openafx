# standard include file for open libraries

# installation:
# . libraries are installed by default
# . use OLIB_INSTALL_LIB=false before including this file
#    to disable instalation for the current library
!contains( OLIB_INSTALL_LIB,false ) {
	unix:target.path = $$JAHPREFIX/lib
	unix:INSTALLS += target
}

