###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib

include ( ../lib_standard.pro )

CONFIG += dll exception

DEFINES += OPENGPULIB_EXPORTS

HEADERS     =           gpumathlib.h \
                        glsl_objects.h \
                        config.h

SOURCES     =           gpumathlib.cpp \
                        gpumathlib_textures.cpp \
                        glsl_objects.cpp

TARGET      =           ../lib/opengpulib
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes

#INCLUDEPATH =           . \
#                        ../../AuxiliaryLibraries/glew
#

contains( JAHOS,IRIX ) {
INCLUDEPATH +=      $$SGIDIR
}

contains( OSNAME,[dD]arwin ) {
	QMAKE_LFLAGS_SONAME = -Wl,-install_name,$$JAHPREFIX/lib/
}

win32 {
LIBS += glew32.lib
}

contains( OPENOBJECTLIBSUPPORT,true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
	LIBS += $$OPENLIBRARIES_LIBS
} else {
	LIBS += ../../AuxiliaryLibraries/glew/libglew.a
}

