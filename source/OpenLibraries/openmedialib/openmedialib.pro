###############################################################
#
# VisualMedia OpenMediaLib QMake build files
#
###############################################################

include( ../../../Settings.pro )

###############################################################
# Configure subdirectories


message( "Configuring Media Support" )
message( "-------------------------" )

win32 {
SUBDIRS += codecs/aviutils
}

#files for mpeg encoder on linux
contains( JAHMPEGENCODER,true ) {
message( "Configuring MPEG encoding Support" )
SUBDIRS += codecs/mpegenc
}

SUBDIRS += formats

SUBDIRS += mediaobject

TEMPLATE = subdirs

