###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE 	= lib

include ( ../../lib_standard.pro )

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

HEADERS		=   mediaobject.h \
				mediaobject_.h
SOURCES     =   mediaobject.cpp

contains( JAHMPEGDECODER,true ) {
SOURCES     += mediaobject_mpeg.cpp
HEADERS		+= mediaobject_mpeg.h
}

contains( LINUXAVISUPPORT,true ) {
SOURCES      += mediaobject_linavi.cpp
HEADERS		 += mediaobject_linavi.h
}

contains( MLTSUPPORT,true ) {
SOURCES      += mediaobject_mlt.cpp
HEADERS      += mediaobject_mlt.h
}

win32:SOURCES   += mediaobject_winavi.cpp
win32:HEADERS	+= mediaobject_winavi.h

TARGET = ../../lib/mediaobject
DEPENDPATH = $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . .. \
                        ../../opencore \
                        ../../openimagelib


#files for mpeg decoder on linux
contains( JAHMPEGDECODER,true ) {
INCLUDEPATH +=          ../codecs/mpegdec
}

#files for mpeg encoder on linux
contains( JAHMPEGENCODER,true ) {
INCLUDEPATH +=          ../codecs/mpegenc
}

contains( MLTSUPPORT,true ) {
mltpath= $$system(mlt-config --prefix)
INCLUDEPATH +=          $$mltpath/include $$mltpath/include/mlt
}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}


