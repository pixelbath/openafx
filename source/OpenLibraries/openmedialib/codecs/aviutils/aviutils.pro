###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib

OLIB_INSTALL_LIB=false
include ( ../../../lib_standard.pro )

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

HEADERS     =           \
			avi_utils.h

SOURCES     =           \
			avi_utils.cpp

TARGET      =           ../../../lib/aviutils
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           .

contains( JAHOS,IRIX ) {
INCLUDEPATH +=      $$SGIDIR
}


