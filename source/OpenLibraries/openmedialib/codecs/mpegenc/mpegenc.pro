###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =       lib

include ( ../../../lib_standard.pro )

CONFIG += warn_off

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

HEADERS     =       mpeg2enc.h \
                    vlc.h
SOURCES		=	\
			conform.cpp \
                    	fdctref.cpp \
                   	idct.cpp \
                    	motion.cpp \
                    mpeg2enc.cpp \
                    predict.cpp \
                    putbits.cpp \
                    puthdr.cpp \
                    putmpg.cpp \
                    putpic.cpp \
                    putseq.cpp \
                    putvlc.cpp \
                    quantize.cpp \
                    ratectl.cpp \
                    readpic.cpp\
                    stats.cpp \
                    transfrm.cpp \
                    writepic.cpp

TARGET 		= 	../../../lib/mpegenc

DEPENDPATH  =       $$JAHDEPENDPATH
INCLUDEPATH =       	. \
			../../encoders/mpeg2enc \
                    ../../libraries \
                    ../../Modules/encoder \
                    ../../player  \
                    $$FREEDIR

