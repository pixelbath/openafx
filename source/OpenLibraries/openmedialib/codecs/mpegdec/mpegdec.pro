###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE 	= lib

include ( ../../../lib_standard.pro )

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

# We do not care about warnings here
CONFIG += warn_off

HEADERS		=	\
			config.h \
                      	global.h \
			getvlc.h \
			mpeg2dec.h
#                      	qimagefilter.h

SOURCES		= 	\
                	mpegdec_getbits.c \
			mpegdec_getblk.c \
			mpegdec_gethdr.c \
			mpegdec_getpic.c \
			mpegdec_getvlc.c \
			mpegdec_idct.c \
			mpegdec_idctref.c \
			mpegdec_motiondec.c \
			mpegdec_mpeg2dec.c \
			mpegdec_recon.c \
			mpegdec_spatscal.c \
			mpegdec_store.c \
			mpegdec_subspic.c \
			mpegdec_systems.c

TARGET	= 	../../../lib/mpegdec

DEPENDPATH = $$JAHDEPENDPATH

INCLUDEPATH =	. \
		../../encoders/mpegenc \
		../../encoders/mpegdec \
		../../libraries \
		../../Modules/encoder \
		../../player  \
		$$FREEDIR

