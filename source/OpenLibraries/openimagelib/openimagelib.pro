###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE 	= 		lib

include ( ../lib_standard.pro )

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

HEADERS		=   	tga.h			\
                	rgb.h			\
                	exr.h

SOURCES		=   	tga.cpp			\
                	rgb.cpp			\
                	exr.cpp

TARGET 		= 		../lib/openimagelib
DEPENDPATH 	= 		$$JAHDEPENDPATH

###############################################################

INCLUDEPATH += $$OPENLIBRARIES_INCLUDE

contains( JAHOS,IRIX ) {
INCLUDEPATH += 		$$SGIDIR
}


