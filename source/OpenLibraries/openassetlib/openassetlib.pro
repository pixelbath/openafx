###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE 	=       lib

include ( ../lib_standard.pro )

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

HEADERS		=       assetdata.h \
                    assetexchange.h

SOURCES		=		assetdata.cpp \
                    assetexchange.cpp \
                    assetexchange_io.cpp

TARGET 		= 		../lib/openassetlib
DEPENDPATH 	=       $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =       . \
					../opencore \ 
					../openimagelib \
					../openmedialib/mediaobject

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../AuxiliaryLibraries/sndfile/sndfile
}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}


