#include( ./src/openassetlib.pro )
VERSION		=	0.0.1
TEMPLATE	=	lib
TARGET		=	v2_openassetlib
DESTDIR		=	./distributables
CONFIG		+=	staticlib warn_on thread exceptions rtti stl

win32:!exists( ./common/lib/sqlite3.lib ) {
	error( "No sqlite3.lib file found" )
}

LIBS			+=	./common/lib/sqlite3.lib
win32:QMAKE_LIB	+=	./common/lib/sqlite3.lib
DEPENDPATH		+=	./common/include
INCLUDEPATH		+=	./common/include

debug	{
	OBJECTS_DIR	=	Debug
} else	{
	OBJECTS_DIR	=	Release
}

DEFINES		+=	

HEADERS		+=	./src/Asset.h						\
				./src/DesktopAsset.h				\
				./src/DataSource.h				\
				./src/Database.h					\
				./src/AssetManager.h				\

SOURCES		+=	./src/Asset.cpp					\
				./src/DesktopAsset.cpp			\
				./src/DataSource.cpp				\
				./src/Database.cpp				\
				./src/AssetManager.cpp			\
				
unix {
	QMAKE_CXXFLAGS_DEBUG += -fexceptions
	QMAKE_CXXFLAGS_RELEASE += -fexceptions
}
