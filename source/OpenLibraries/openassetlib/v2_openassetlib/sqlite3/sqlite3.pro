#include( ./src/sqlite3.pro )
VERSION		= 3.2.7
TEMPLATE	= lib
TARGET		= sqlite3
DESTDIR		=	./distributables
CONFIG		+= staticlib warn_off
INCLUDEPATH += .

debug	{
	OBJECTS_DIR	=	Debug
} else	{
	OBJECTS_DIR	=	Release
}

# Input
HEADERS += ./src/btree.h \
           ./src/hash.h \
           ./src/keywordhash.h \
           ./src/opcodes.h \
           ./src/os.h \
           ./src/os_common.h \
           ./src/os_unix.h \
           ./src/os_win.h \
           ./src/pager.h \
           ./src/parse.h \
           ./src/sqlite3.h \
           ./src/sqliteInt.h \
           ./src/vdbe.h \
           ./src/vdbeInt.h
SOURCES += ./src/alter.c \
           ./src/analyze.c \
           ./src/attach.c \
           ./src/auth.c \
           ./src/btree.c \
           ./src/build.c \
           ./src/callback.c \
           ./src/complete.c \
           ./src/date.c \
           ./src/delete.c \
           ./src/expr.c \
           ./src/func.c \
           ./src/hash.c \
           ./src/insert.c \
           ./src/legacy.c \
           ./src/main.c \
           ./src/opcodes.c \
           ./src/os_unix.c \
           ./src/os_win.c \
           ./src/pager.c \
           ./src/parse.c \
           ./src/pragma.c \
           ./src/prepare.c \
           ./src/printf.c \
           ./src/random.c \
           ./src/select.c \
           ./src/shell.c \
           ./src/table.c \
#           ./src/tclsqlite.c \
           ./src/tokenize.c \
           ./src/trigger.c \
           ./src/update.c \
           ./src/utf.c \
           ./src/util.c \
           ./src/vacuum.c \
           ./src/vdbe.c \
           ./src/vdbeapi.c \
           ./src/vdbeaux.c \
           ./src/vdbefifo.c \
           ./src/vdbemem.c \
           ./src/where.c
