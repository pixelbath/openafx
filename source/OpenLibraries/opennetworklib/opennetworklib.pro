###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE        =       lib

include ( ../lib_standard.pro )

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

HEADERS			= 		httpd.h  \
                        jahtextbrowser.h \
                        jahhttpd.h \
                        jhclient.h

SOURCES         =   	httpd.cpp \
                        jahtextbrowser.cpp \
                        jahhttpd.cpp \
                        jhclient.cpp

TARGET          =       ../lib/opennetworklib
DEPENDPATH      =       $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH 	=		. .. \
						../opencore \
                        ../openassetlib \
                        ../openmedialib/mediaobject \
                        ../openimagelib \
                        ../../Jahshaka/JahLibraries/jahdatabase \
                        ../../Jahshaka/JahLibraries/jahtracer \
                        ../../Jahshaka/JahLibraries/jahpreferences \
                        ../../Jahshaka/JahDesktop/desktop \
                        ../../Jahshaka/JahCore/jahworld \
                        ../../Jahshaka/JahCore/jahobjects \
						../../OpenLibraries/opengpulib \
						../../OpenLibraries/openassetlib/v2_openassetlib/sqlite3/src \
                        ../openmedialib


#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../AuxiliaryLibraries/sndfile/sndfile
}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT,true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

