###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE 	=       lib

include ( ../lib_standard.pro )

#for dynamic build support
contains( JAHSTATIC,false ) {
CONFIG += dll
}

#for dynamic build support
contains( JAHSTATIC,true ) {
CONFIG += staticlib
}

HEADERS		=       opentracer.h \
					openpreferences.h
SOURCES		=       opentracer.cpp \
					openpreferences.cpp

TARGET 		= 		../lib/opencorelib
DEPENDPATH 	=       $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . ..


INCLUDEPATH =	. .. \ 
                ../openimagelib
				
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}


