###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE = lib
CONFIG		+= staticlib warn_off
HEADERS		=   xdrvlib.h 
SOURCES		=   xdrvlib.c 

TARGET = spaceball
DEPENDPATH = $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =		. 

contains( JAHOS,IRIX ) {
INCLUDEPATH += 		$$SGIDIR
}
			

