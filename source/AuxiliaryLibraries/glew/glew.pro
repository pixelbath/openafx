###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
DEFINES     +=          GLEW_STATIC
HEADERS     =           glew.h 
SOURCES     =           glew.c
TARGET      =           glew
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . .. \
                        ../../Core/Objects \
                        ../../Core/Render \
                        ../../Core/World  \
                        ../../desktop
            
#files for mpeg player on linux
contains( JAHMPEGPLAYER,true ) {
INCLUDEPATH +=          ../../encoders/mpeg_play
}

#files for mpeg encoder on linux
contains( JAHMPEGENCODER,true ) {
INCLUDEPATH +=          ../../encoders/mpegenc
}

#files for mpeg decoder on linux
contains( SPACEBALL,true ) {
INCLUDEPATH +=          ../../libraries/spaceball
}

INCLUDEPATH +=          ../../libraries \
                        ../../libraries/assetobj \
                        ../../libraries/GLCore \
                        ../../libraries/glew \
                        ../../libraries/imagelibs \
                        ../../libraries/jahkeyframes \
                        ../../libraries/jahpreferences \
                        ../../libraries/jahtimer \
                        ../../libraries/jahtracer \
                        ../../libraries/mediaobject
                        
contains( SPACEBALL,true ) {
INCLUDEPATH     +=      ../../libraries/spaceball
}

INCLUDEPATH +=          ../../uiwidgets \
                        ../../uiwidgets/interfaceobjs \
                        ../../uiwidgets/jahcalc \
                        ../../uiwidgets/jahfileloader \
                        ../../uiwidgets/keyframes \
                        ../../uiwidgets/nodes \
                        ../../uiwidgets/timeline \
                        ../../Modules/animate \
                        ../../Modules/color \
                        ../../Modules/edit \
                        ../../Modules/effect \
                        ../../Modules/painter \
                        ../../Modules/text \
                        ../../objectlibs/FTGL \
                        ../../objectlibs/glmlib \
                        ../../objectlibs/l3ds \
                        ../../objectlibs/particle \
                        ../../objectlibs/blur \
                        ../../network \
                        ../../player \
                        ../../sqlite \
                        $$FREEDIR

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



