###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE =              lib
CONFIG		+=          staticlib 
HEADERS		=       	\
						giftconnection.h \
                        giftparse.h \
                        giftsocket.h \
                        config.h

!win32: HEADERS += giftconnection_lin.h 
win32: HEADERS += giftconnection_win.h 

SOURCES     =           \
						giftconnection.cpp \
                        giftsocket.cpp
TARGET =                gift
DEPENDPATH =            $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}


#for glib
#INCLUDEPATH +=          /usr/include/glib-2.0 \
#                        /usr/lib/glib-2.0/include

