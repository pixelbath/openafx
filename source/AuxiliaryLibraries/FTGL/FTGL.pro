###############################################################
#
# Jahshaka 1.9a4 QMake module files 
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE = lib
CONFIG		+= staticlib warn_off
HEADERS		= 		FTBBox.h \
                FTBitmapGlyph.h \
                FTCharmap.h \
                FTCharToGlyphIndexMap.h \
                FTContour.h \
                FTExtrdGlyph.h \
                FTFace.h \
                FTFont.h \
                FTGL.h \
                FTGLBitmapFont.h \
                FTGLExtrdFont.h \
                FTGLOutlineFont.h \
                FTGLPixmapFont.h \
                FTGLPolygonFont.h \
                FTGLTextureFont.h \
                FTGlyph.h \
                FTGlyphContainer.h \
                FTLibrary.h \
                FTList.h \
                FTOutlineGlyph.h \
                FTPixmapGlyph.h \
                FTPoint.h \
                FTPolyGlyph.h \
                FTSize.h \
                FTTextureGlyph.h \
                FTVector.h \
                FTVectoriser.h 
SOURCES     =   FTBitmapGlyph.cpp \
                FTCharmap.cpp \
                FTContour.cpp \
                FTExtrdGlyph.cpp \
                FTFace.cpp \
                FTFont.cpp \
                FTGLBitmapFont.cpp \
                FTGLExtrdFont.cpp \
                FTGLOutlineFont.cpp \
                FTGLPixmapFont.cpp \
                FTGLPolygonFont.cpp \
                FTGLTextureFont.cpp \
                FTGlyph.cpp \
                FTGlyphContainer.cpp \
                FTLibrary.cpp \
                FTOutlineGlyph.cpp \
                FTPixmapGlyph.cpp \
                FTPoint.cpp \
                FTPolyGlyph.cpp \
                FTSize.cpp \
                FTTextureGlyph.cpp \
                FTVectoriser.cpp 
TARGET = ftgl
DEPENDPATH = $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =		. 

INCLUDEPATH += $$FREEDIR

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
win32{
    INCLUDEPATH += C:\freetype\include
}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}




