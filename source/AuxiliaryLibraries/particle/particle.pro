###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings


TEMPLATE	= lib
CONFIG		+= staticlib 

HEADERS		= general.h \
		  p_vector.h \
		  papi.h
SOURCES		= action_api.cpp \
		  actions.cpp \
		  opengl.cpp \
		  system.cpp

TARGET = particle

################################################################

DEPENDPATH = $$JAHDEPENDPATH
INCLUDEPATH =		. 

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}


#os specific tweaks here

##############################
### unix config
##############################
unix:CLEAN_FILES+= *.bck

##############################
### linux config
##############################
linux-g++:TMAKE_CXXFLAGS_RELEASE = -O3 -ffast-math

##############################
### WIN32 config
##############################
#win32-msvc:TEMPLATE     =	vclib

##############################
### OsX Darwin config
##############################

