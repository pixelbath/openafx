###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE = lib
CONFIG		+= staticlib 
HEADERS		=   blurlib.h 
SOURCES		=   blurlib.cpp 
TARGET = blur
DEPENDPATH = $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =		. 

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}




