###############################################################
#
# VisualMedia AuxilliaryLibraries QMake build files
#
###############################################################

include( ../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Configuring AuxilliaryLibraries" )
message( "-------------------------------" )

TEMPLATE = subdirs

SUBDIRS += blur
SUBDIRS += FTGL
SUBDIRS += particle
#SUBDIRS += sqlite
SUBDIRS += apollon
SUBDIRS += gift


contains( JAHPLAYER,true ) {	
SUBDIRS -= apollon
SUBDIRS -= gift
SUBDIRS += glew
}

unix {
	SUBDIRS += spaceball 
}

mac {
	SUBDIRS -= spaceball 
}

contains( OPENOBJECTLIBSUPPORT,false ) {
	SUBDIRS += glew
}

#files for audiosupport
contains( JAHAUDIO,true ) {
	SUBDIRS += sndfile \
}

#TEMPLATE = subdirs

