###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 

HEADERS     =           \
			apollonlistview.h \
                        apollonsearchviewitem.h \
                        apollonsearchlistview.h \
                        apollontransferviewitem.h \
                        apollontransfertab.h \
                        apollontab.h \
                        apollonutils.h \
                        apollonqueryview.h

SOURCES     =           \
			apollonlistview.cpp \
                        apollonsearchlistview.cpp \
                        apollonsearchviewitem.cpp \
                        apollonqueryview.cpp \
                        apollonutils.cpp \
                        apollontransferviewitem.cpp \
                        apollontab.cpp \
                        apollontransfertab.cpp

TARGET      =           apollon

DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           . \
			../gift


contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



