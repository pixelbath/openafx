###############################################################
#
# Jahshaka 1.9a9 QMake module files
#
###############################################################

#include presets file in home directory
include( ../Settings.pro )

###############################################################
# the rest of the makefile settings

TEMPLATE	=	app

contains( JAHPLAYER,false ) {
TARGET	=	../jahshaka
}

contains( JAHPLAYER,true ) {
TARGET	=	../jahplayer
}

DEPENDPATH = $$JAHDEPENDPATH
RC_FILE = ../jahshaka.rc

###############################################################
#set up include path for linking

include(source_includes.pro)

###############################################################
# Now lets link the Application

LIBS +=         Jahshaka/JahSource/jahmain/jahmain.lib \
                Jahshaka/JahSource/jahsplash/jahsplash.lib \
                Jahshaka/JahSource/jahcreate/jahcreate.lib \

LIBS +=         Jahshaka/JahCore/jahworld/jahworld.lib \
                Jahshaka/JahCore/jahrender/jahrender.lib \
                Jahshaka/JahCore/jahobjects/jahobjects.lib

LIBS +=			Jahshaka/JahModules/animation/animate.lib \
				Jahshaka/JahModules/colorize/color.lib \
				Jahshaka/JahModules/editing/edit.lib \
				Jahshaka/JahModules/effect/effect.lib \
				Jahshaka/JahModules/keyer/keyer.lib \
				Jahshaka/JahModules/painter/painter.lib \
				Jahshaka/JahModules/text/text.lib \
				Jahshaka/JahModules/tracker/tracker.lib 


LIBS +=			Jahshaka/JahDesktop/desktop/desktop.lib \
				Jahshaka/JahDesktop/encoder/renderdialog.lib \
				Jahshaka/JahDesktop/library/library.lib \
				Jahshaka/JahDesktop/network/network.lib \
				Jahshaka/JahDesktop/player/player.lib 

LIBS +=			Jahshaka/JahWidgets/interfaceobjs/interfaceobjs.lib \
				Jahshaka/JahWidgets/jahcalc/jahcalc.lib \
				Jahshaka/JahWidgets/jahfileloader/jahfileloader.lib \
				Jahshaka/JahWidgets/keyframes/keyframes.lib \
				Jahshaka/JahWidgets/nodes/jahnodes.lib \
				Jahshaka/JahWidgets/timeline/jahtimeline.lib \ 
				Jahshaka/JahWidgets/colortri/keyercolorwidget.lib \
				Jahshaka/JahWidgets/waveform/jahwaveform.lib \
				Jahshaka/JahWidgets/wireup/jahwireup.lib \
				Jahshaka/JahWidgets/mediatable/jahmediatable.lib
				
LIBS +=			Jahshaka/JahLibraries/jahdatabase/jahdatabase.lib \
				Jahshaka/JahLibraries/jahdataio/jahdataio.lib \
				Jahshaka/JahLibraries/jahformatter/jahformatter.lib \
				Jahshaka/JahLibraries/jahglcore/glcore.lib \ 
				Jahshaka/JahLibraries/jahkeyframes/jahkeyframes.lib \
				Jahshaka/JahLibraries/jahplayer/diskplayer/diskplayer.lib \
				Jahshaka/JahLibraries/jahplugins/jahplugins.lib \
				Jahshaka/JahLibraries/jahpreferences/jahpreferences.lib \
				Jahshaka/JahLibraries/jahthemes/jahthemes.lib \
				Jahshaka/JahLibraries/jahtimer/jahtimer.lib \
				Jahshaka/JahLibraries/jahtracer/jahtracer.lib \
				Jahshaka/JahLibraries/jahtranslate/jahtranslate.lib \
				Jahshaka/JahLibraries/jahxml/jahxml.lib 


#includes for the audio hack
contains( JAHAUDIO,true ) {
LIBS +=         Jahshaka/JahLibraries/jahplayer/audioplayer/audioplayer.lib
}

         
###############################################################
# first lets link the Openlibraries

win32 {
LIBS +=         -L./OpenLibraries/lib		-lopencorelib 
LIBS +=         -L./OpenLibraries/lib		-lopenassetlib 
LIBS +=         -L./OpenLibraries/lib		-lopenobjectlib 
LIBS +=         -L./OpenLibraries/lib		-lmediaobject 
LIBS +=         -L./OpenLibraries/lib 		-lformats 
LIBS +=         -L./OpenLibraries/lib		-lopengpulib 
LIBS +=         -L./OpenLibraries/lib		-lopenimagelib 
LIBS +=         -L./OpenLibraries/lib		-lopennetworklib 
}

#for the new medialibs
LIBS +=         -L./OpenLibraries/lib		-laviutils 

#files for mpeg encoder on linux
contains( JAHMPEGENCODER,true ) {
LIBS +=         -L./OpenLibraries/lib		-lmpegenc 
}

#files for mpeg decoder on linux
contains( JAHMPEGDECODER,true ) {
LIBS +=         -L./OpenLibraries/lib		-lmpegdec 
}

#LIBS +=			-Wl,-rpath ./source/OpenLibraries/lib
  
###############################################################
# first lets link the Auxiliaryibraries

#includes for the audio hack
contains( JAHGIFT,true ) 	
{ 
LIBS +=  		AuxiliaryLibraries/apollon/apollon.lib  \
				AuxiliaryLibraries/gift/gift.lib
}


LIBS +=         AuxiliaryLibraries/blur/blur.lib \
				AuxiliaryLibraries/FTGL/ftgl.lib \
                AuxiliaryLibraries/glew/glew.lib \
                AuxiliaryLibraries/particle/particle.lib \
#                AuxiliaryLibraries/sqlite/sqlite.lib \
                AuxiliaryLibraries/OpenEXR/OpenEXR.lib \
                AuxiliaryLibraries/zlib/zlib.lib


#includes for the audio hack
contains( JAHAUDIO,true ) {
LIBS +=         AuxiliaryLibraries/sndfile/sndfile/sndfile.lib \
                AuxiliaryLibraries/sndfile/sndfile/G72x/jahaudiog72.lib \
                AuxiliaryLibraries/sndfile/sndfile/GSM610/jahaudiogGSM.lib
}


#files for spaceball on linux
contains( SPACEBALL,true ) {
LIBS +=			AuxiliaryLibraries/spaceball/spaceball.lib 
}   

##############################################################

LIBS+= glut32.lib vfw32.lib OLDNAMES.lib

contains(JAHDEBUG, false) {

    LIBS+= MSVCRT.lib 
    LIBS+= C:/freetype/objs/freetype219.lib
    QMAKE_LFLAGS += /subsystem:windows /entry:"mainCRTStartup"
    QMAKE_LFLAGS += /nodefaultlib:libc.lib /nodefaultlib:libcmt.lib /nodefaultlib:libcd.lib /nodefaultlib:libcmtd.lib /nodefaultlib:msvcrtd.lib

} else {

    LIBS+= MSVCRTD.lib 
    LIBS+= C:/freetype/objs/freetype219_D.lib
    QMAKE_LFLAGS += /subsystem:console /incremental:yes /pdbtype:sept

}

QMAKE_LFLAGS += /VERBOSE:LIB



