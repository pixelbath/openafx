###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib

HEADERS     =           coreheaders.h \
                        coreobject.h \
                        coreeffects.h \
                        corestructures.h

SOURCES     =           coreobject.cpp \
                        coreeffects.cpp \
                        jahasset.cpp \    
                        jahutility.cpp \    
                        jahtexture.cpp \
						corestructures.cpp

TARGET      =           jahobjects

DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           .  

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile
}

INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
                        ../../../AuxiliaryLibraries/FTGL \
                        ../../../AuxiliaryLibraries/particle 
                        
INCLUDEPATH += 		../../../Jahshaka/JahWidgets/keyframes \
			../../../Jahshaka/JahWidgets/nodes

INCLUDEPATH +=		../../../OpenLibraries/opencore \
				../../../OpenLibraries/openassetlib \
				../../../OpenLibraries/opengpulib \
				../../../OpenLibraries/openobjectlib \
				../../../OpenLibraries/openmedialib \
				../../../OpenLibraries/openimagelib \
				../../../OpenLibraries/openmedialib/mediaobject 

INCLUDEPATH +=          ../../JahCore/jahobjects \
				../../JahCore/jahrender \
				../../JahCore/jahworld

INCLUDEPATH +=          ../../JahWidgets/interfaceobjs 
#				../../JahWidgets/nodes \ 
#				../../JahWidgets/keyframes 

INCLUDEPATH +=		../../JahLibraries \
				../../JahLibraries/jahtracer \
				../../JahLibraries/jahtimer \
				../../JahLibraries/jahkeyframes \
				../../JahLibraries/jahpreferences \
				../../JahLibraries/jahformatter \
				../../JahLibraries/jahtranslate \
				../../JahLibraries/jahdataio \
				../../JahLibraries/jahplugins \
				../../JahLibraries/jahglcore \
				$$FREEDIR

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT, true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



