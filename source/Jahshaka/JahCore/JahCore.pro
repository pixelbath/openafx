###############################################################
#
# VisualMedia OpenLibraries QMake build files
#
###############################################################

include( ../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Configuring JahCore" )
message( "-------------------" )

SUBDIRS += jahobjects
SUBDIRS += jahrender
SUBDIRS += jahworld

TEMPLATE = subdirs

