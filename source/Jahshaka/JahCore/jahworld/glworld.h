/*******************************************************************************
**
** The header file for the Jahshaka glworld module 
** The Jahshaka Project
** Copyright (C) 2000-2006 VM Inc.
** Released under the GNU General Public License
**
*******************************************************************************/

#ifndef GLWORLD_H
#define GLWORLD_H

//////////////////////////////////////////////
//qt library includes here

#include <qwidget.h>
#include <qapplication.h>
#include <jahheadable.h>
class QFrame;
class QCheckBox;
class QComboBox;
class QButton;
class QPushButton;
class QTable;
class QSlider;
class QHBox;
class QVBox;
class QWidgetStack;
class QTimer;
class QListView;
class QCheckListItem;
class QSlider;
class QLabel;
class QLCDNumber;
class QMultiLineEdit;
class QLineEdit;
class QPopupMenu;
class QHBoxLayout;
class QToolButton;
class QMenuBar;
class QSplitter;
class QTextView;
class QHBoxLayout;
class QSpacerItem;

//custom qt widgets
class InputLCD;
class JahDesktopSideTable;
class SlickSlider;
class TimelineSlider;
class SliderButton;
class FancyPopup;
class JahTimer;
class JahToolButton;
class JahThemes;
class SupergRangeControl;

//////////////////////////////////////////////
//for OpenMediaLib asset support
#include "assetdata.h"

//forward references
namespace bxmlnode
{
    struct basicxmlnode ;
}
 
//////////////////////////////////////////////
//important core includes here

class JahLayer;

//for the Render class
#include "corestructures.h"
#include "coreeffects.h"
class JahRender;

//the different world lists
class ActiveLayerList;
class LayerListEntry;

//////////////////////////////////////////////
//forward definitions for trace and translation
class JahTrace;
class JahTranslate;

//////////////////////////////////////////////
//display widgets for jahshaka
//we nullify them when building jahplayer
//... ugh - horrible

#ifndef JAHPLAYER
class JahKeyframer;
class JahNodes;
#else
typedef struct { int null; } JahKeyframer; 
typedef struct { int null; } JahNodes; 
#endif

class GLCore;
class JahPluginLib;
class JahControl;

static const unsigned int JAH_XML_TAB_DEPTH = 4;
static const unsigned int MODULE_AUTO_SAVE_FREQUENCY = 60;  // Seconds
static const unsigned int NUMBER_OF_AXIS_SLIDERS = 13;
static const unsigned int NUMBER_OF_LIGHTING_SLIDERS = 12;

static const int MAX_TRACKING_POINTS = 4;
static const int MAX_TRACKING_HISTORY = 4;
static const float MAX_TRACKER_CIRCLE_RADIUS = 31;
static const unsigned int TRACKER_POINT_TEXTURE_WIDTH = static_cast<int>( (MAX_TRACKER_CIRCLE_RADIUS + 1.0f) * 2 );
static const float MAX_TRACKER_TRACKING_RADIUS = 200;

///////////////////////////////////////////////
//old array to sort layer rendering on the fly

struct worldSort
{
    int       laynum;
    GLfloat    depth;
};

/////////////////////////////////////////////
// main class

template < typename T > QCheckListItem* factory( T*, QListViewItem*, const QString& );

class GLWorld : public QWidget, public JahHeadable {
    Q_OBJECT

public:

    GLWorld( GLCore* jahcore, const char* name, QHBox* controller=0, int* globalclipnumber=0 );
    virtual ~GLWorld();

	// This method should be called before any attempt to raise a module
	void activate( )
	{
		if ( !m_started )
		{
			lazy( );
			start( );
			m_started = true;
		}
	}

    enum FRAMES_PER_SECOND
    {
        FRAMES_PER_SECOND_15,
        FRAMES_PER_SECOND_24,
        FRAMES_PER_SECOND_25,
        FRAMES_PER_SECOND_30,
        FRAMES_PER_SECOND_60,
        FRAMES_PER_SECOND_90
    };

protected:
    static std::vector<std::string> m_frames_per_second_name_vector;

public:
	virtual void headRender( int width = 0, int height = 0 );
	int getCurrentWidth( ) const { return curr_width_; }
	int getCurrentHeight( ) const { return curr_height_; }

    std::vector<std::string>* getFramesPerSecondNameVector() { return &m_frames_per_second_name_vector; }
    std::string getFramesPerSecondName(FRAMES_PER_SECOND index) { return getFramesPerSecondNameVector()->at(index); }

	// Shoulf be called in the start by any module that needs plugins
	void initializePlugins( );

    //////////////////////////////////////////
    //gl commands to be sorted out later
    virtual void makeCurrent(void);
    virtual void updateGL   (void);

    //build the modules interface
    virtual void buildController( QHBox* controller);

    void initializeWorld();
    void buildEffectsOptions();

    virtual void initializeObjects();

    void raiseCore();

    //initialize a mesh layer
    void        initializeMeshLayer(int layer);
    void        initializeMeshLayer(JahLayer* layer);

    ////////////////////////////////////////
    //pointer to tracer singleton
    JahTrace            * jtrace;

    ////////////////////////////////////////
    //pointer to translater singleton
    JahTranslate        * jt;

    template < typename T > QCheckListItem* addCheckListItem(T* parent, bool status = TRUE, bool headtail = FALSE, 
							     QCheckListItem* (*fn)(T*, QListViewItem*, const QString& ) = &factory );

    //for gpu power status
    bool        gpusupport,nv_gpusupport;

    //main create layer function
    bool buildLayer (JahLayer* layer, QString layerName, LayerCategory::TYPE layertype, ObjectCategory::TYPE objtype );
    
    //added for easy layer configuration in the modules
    JahLayer* configureLayer(QString cname=0, 
                                bool cstatus=0, bool cblend=0, bool cdepth=0,
                                bool cassetstatus=0, QString cfilename=0, QString cextension=0 );


    //virtual functions are meant to be overridden in calling modules
    virtual void updateVideoTexture(JahLayer*, int);
    QImage  screenShot(int Frame,int xloffset,int xroffset,int ytoffset,int yboffset,
                        int xres, int yres, int number_of_base_layers);

    virtual void    mouseMoveCamera( int x,int y );
    virtual void    mouseMoveLayer ( int thelayer, int x, int y );
    virtual void    mouseMoveLayer ( JahLayer* jah_layer, int x, int y );
    virtual void    processSelection(GLuint *pSelectBuff);
    
    virtual void contextMenuEvent(QContextMenuEvent * e);

    virtual void keyPressEvent( QKeyEvent *k );
    virtual void keyReleaseEvent( QKeyEvent * ) { }

    //////////////////////////////////////////////////////////////
    //ui code for menu items
    virtual void createMenuItem(QPopupMenu * ) { }

    //////////////////////////////////////////////////////////////
    //for on screen stats feedback
    void drawOverlayStats(double fps);
    bool stats_showStats;

    //////////////////////////////////////////////////////////////
    //for load and save of scenes

    //used for file loading layer creation
    bool loadLayer ( JahLayer* jah_layer, std::string layer_name, LayerCategory::TYPE layertype, ObjectCategory::TYPE objtype, 
                     bool isasset, assetData asset, bool haskey, assetData keyasset);

    bool loadLayer ( int nl, QString layerName, LayerCategory::TYPE layertype, ObjectCategory::TYPE objtype, 
                     bool isasset, assetData asset, bool haskey, assetData keyasset);

    //PackageSave() is a slot
    void  SaveAllAssets(QString destination); //used by PackageSave

    //test asset and locate
    void locateLoadAsset(assetData& asset_data,std::string const& file_name_string);

    //for xml scene save 
    void SaveAll( std::string const & a_save_filename, bool const a_status );
    void SaveAllData( std::fstream & ai_fstream );
    void saveSceneFile( std::string const & a_save_filename);
    void saveAllLayers( std::fstream & ai_strstream );
    void saveHeaders( std::fstream& fstream, std::string const& save_filename, std::string module_name);
    void saveEffectLayers( std::fstream & ai_fstream , JahLayer* jah_layer );
    void saveLightLayers( std::fstream & ai_fstream , JahLayer* jah_layer );
    void SaveNodes( std::fstream & ai_fstream , JahLayer* jah_layer );
    void SaveProperties( std::fstream & ai_fstream , JahLayer* jah_layer );
    void SaveCameraNodes( std::fstream & ai_fstream );
    void SaveColor( std::fstream & ai_fstream , unsigned int const & a_i ); 
    void SaveText( std::fstream & ai_fstream , unsigned int const & a_i ); 
    void SaveAsset( std::fstream & ai_fstream , JahLayer* jah_layer );
    void SaveKeyAsset( std::fstream & ai_fstream , unsigned int const & a_i );
    void SaveEffect( std::fstream & ai_fstream , unsigned int const & a_i );
    void SaveObjectData( std::fstream & ai_fstream , unsigned int const & a_i );
    void SaveParticleData( std::fstream & ai_fstream , unsigned int const & a_i );
    void SaveTrackerData( std::fstream & ai_fstream );

    const char*     sceneGetIndent();
    void            sceneIncreaseIndent() { m_scene_save_indentation_depth++; }
    void            sceneDecreaseIndent() { m_scene_save_indentation_depth--; }
    int             getSceneSaveIndentationDepth() { return m_scene_save_indentation_depth; }
    void            setSceneSaveIndentationDepth(int depth) { m_scene_save_indentation_depth = depth; }

    //for xml scene load
    bool LoadAll( std::string const& scene_file_string, bool clear_first, bool apply_effects, 
                  std::string const& file_name_string);
    void LoadAllData( bxmlnode::basicxmlnode const* const & l_xml_root );
    void loadSceneFile(std::string file_name );
    int  sceneFindNextTag(std::string& scene_string, const char* keyword);
    assetData LoadAsset( bxmlnode::basicxmlnode const* const & l_xml_root , int const & i ); 
    void LoadCameraNodes( bxmlnode::basicxmlnode const* const & a_layerstyle_node );
    void LoadLayerNodes( bxmlnode::basicxmlnode const* const & a_layerstyle_node , int const & i );
    void LoadEffect ( bxmlnode::basicxmlnode const* const& a_layerstyle_node , int const & i );
    assetData LoadKeyAsset ( bxmlnode::basicxmlnode const* const & a_keyasset_node , int const & i );
    void LoadParticleData ( bxmlnode::basicxmlnode const* const & a_layerstyle_node , int const & i );
    void LoadObjectData ( bxmlnode::basicxmlnode const* const & a_objects_node , int const & i ) ;
    void LoadColor ( bxmlnode::basicxmlnode const* const & a_text_node , int const & i ) ;
    void LoadText ( bxmlnode::basicxmlnode const* const & a_text_node , int const & i );
    bool getRenderOnlyToSelectedEffect() { return m_render_only_to_selected_effect; }
    void setRenderOnlyToSelectedEffect(bool flag) { m_render_only_to_selected_effect = flag; }
    std::string& getJahGlBlendModeStringVector(int index) { return m_jah_gl_blend_mode_string_vector[index]; }
    GLuint getJahGlBlendModeValueVector(int index) { return m_jah_gl_blend_mode_value_vector[index]; }
    bool getSceneSaveExport() { return m_scene_save_export; }
    void setSceneSaveExport(bool flag) { m_scene_save_export = flag; }

    void setXRotationSlider(JahLayer* jah_layer, int value);
    void setYRotationSlider(JahLayer* jah_layer, int value);
    void setZRotationSlider(JahLayer* jah_layer, int value);
    void setXTranslationSlider(JahLayer* jah_layer, int value);
    void setYTranslationSlider(JahLayer* jah_layer, int value);
    void setZTranslationSlider(JahLayer* jah_layer, int value);
    void setXScaleSlider(JahLayer* jah_layer, int value);
    void setYScaleSlider(JahLayer* jah_layer, int value);
    void setZScaleSlider(JahLayer* jah_layer, int value);
    void setTransparencySlider(JahLayer* jah_layer, int value);

    enum JAH_GL_BLEND_MODE
    {
        JAH_GL_ZERO,
        JAH_GL_ONE,
        JAH_GL_SRC_COLOR,
        JAH_GL_ONE_MINUS_SRC_COLOR,
        JAH_GL_SRC_ALPHA,
        JAH_GL_ONE_MINUS_SRC_ALPHA,
        JAH_GL_DST_ALPHA,
        JAH_GL_ONE_MINUS_DST_ALPHA,
        JAH_GL_DST_COLOR,
        JAH_GL_ONE_MINUS_DST_COLOR,
        JAH_GL_NO_BLEND_MODE
    };

    enum RENDER_QUALITY
    {
        RENDER_QUALITY_JPEG,
        RENDER_QUALITY_PNG,
        RENDER_QUALITY_BMP,
        RENDER_QUALITY_MLT_SELECT,
        RENDER_QUALITY_MLT_FLASH_2000_KBS_640X480,
        RENDER_QUALITY_MLT_FLASH_2000_KBS_320X240,
        RENDER_QUALITY_MLT_FLASH_VCD_1000_KBS,
        RENDER_QUALITY_MLT_FLASH_VCD_500_KBS,
        RENDER_QUALITY_MLT_FLASH_VCD_200_KBS,
        RENDER_QUALITY_MLT_FLASH_VCD_100_KBS,
        RENDER_QUALITY_MLT_FLASH_100_KBS_160X120,
        RENDER_QUALITY_MLT_FLASH_50_KBS_160X120
    };

    enum LAYER_TYPE
    {
        LAYER_3D_OBJECT,
        LAYER_PARTICLE,
        LAYER_LIGHT,
        LAYER_3D_TEXT,
        LAYER_IMAGE_LARGE,
        LAYER_IMAGE
    };


    std::vector<std::string> m_jah_gl_blend_mode_string_vector;
    std::vector<GLuint> m_jah_gl_blend_mode_value_vector;

protected:
	// Module defined ui start method
	virtual void start( ) { }

    int             m_scene_save_indentation_depth;
    bool            m_dont_track;
    bool            m_render_only_to_selected_effect;
    bool            m_scene_save_export;
    bool            m_stop_requested;
    JahLayer*       m_base_layer;
    JahLayer*		m_point_layer[MAX_TRACKING_POINTS];
    bool            m_show_lights;
    bool            m_animation_updated;
    bool            m_dont_paint;

public:
    bool            getAnimationUpdated() { return m_animation_updated; }
    void            setAnimationUpdated(bool flag) { m_animation_updated = flag; }
    virtual void    createTrackerPointLayers();
    JahLayer*       getBaseLayer() { return m_base_layer; }
    bool            getStopRequested() { return m_stop_requested; }
    void            setStopRequested(bool flag) { m_stop_requested = flag; }
    bool            getShowLights();
    void            setShowLights(bool flag);
    bool            getUseAutoKey();
    void            setUseAutoKey(bool flag);

    void            startSingleShotTimer();
    bool            getDontPaint() { return m_dont_paint; }
    void            setDontPaint(bool flag) { m_dont_paint = flag; }

    virtual JahLayer*  createALayer(LAYER_TYPE type, JahLayer* parent);


    //////////////////////////////////////////////////////////////
    //custom ui objects
    void createAxisPanel ( QFrame* theuiframe);

    bool            getDontTrack() { return m_dont_track; }
    void            setDontTrack(bool flag) { m_dont_track = flag; }

    virtual QCheckBox* getKeySelect();

    void  loopThroughLayers(std::string message);

    ///////////////////////////////////////////////////////////////////
    //used for lights   
    void updateLights( void); 

    ///////////////////////////////////////////////////////////////////
    //used to set up custom widgets
    void                initializeNodes(QHBox* parent2);

    ///////////////////////////////////////////////////////////////////
    // systemwide variables
    ///////////////////////////////////////////////////////////////////
    
    /////////////////////////////////////////
    //the opengl core
    GLCore          *core;
    
    int             X_RESOLUTION;
    int             Y_RESOLUTION;
    double          PIXEL_RATIO;

    //////////////////////////////////////////
    //for the linked list for dynamic object layers
    QPtrList<LayerListEntry>*       m_layerlist;
    QPtrList<JahLayer>*             m_layer_list;

    QPtrList<LayerListEntry>*   getLayerList() { return m_layerlist; }
    GLCore*                     getCore() { return core; }

    int             getNumberOfLayers();
    JahLayer*       getJahLayer(int layer_number);

    int             getJahLayerNumber(JahLayer* jah_layer);
    LayerListEntry* findLayerListEntry(QListViewItem* item);
    LayerListEntry* findLayerListEntry(JahLayer* jah_layer);
    
    //new linked list for layer sorting
    QPtrList<worldSort> *newworldorder;

    //new shortcut routine for modules
    /// \warning bad overload as an int can be silently converted to a pointer.
    void initializeListview(int layer_number, bool layer_status);
    virtual void initializeListview(JahLayer* jah_layer, bool layer_status);

    //////////////////////////////////////////

    //default module variables
    QString    ModuleName;
	QHBox* m_controller;
    JahLayer  *thegrid;
    JahLayer  *titlesafe;
    JahLayer  *textsafe;
    JahLayer  *camera;

    QCheckBox*              m_show_lights_checkbox;
    QCheckBox*              getShowLightsCheckbox() { return m_show_lights_checkbox; }
    QCheckBox*              m_use_auto_key_checkbox;
    QCheckBox*              getUseAutoKeyCheckbox() { return m_use_auto_key_checkbox; }

    QCheckBox*              m_use_opengl_key_layer_checkbox;
    QCheckBox*              m_use_fast_shader_key_layer_checkbox;
    bool                    useOpenGLKey();
    bool                    useFastShaderKey();
    bool                    getLockKeyAndClip();



    ActiveLayerList  *activeLayers;

    bool m_sliders_have_changed;
    //bool forceupdatefxbuffer;

    int numberOfLights;

    QTimer*     getAutoSaveTimer() { return m_auto_save_timer; }
    bool        getAutoSaveInProgress() { return m_auto_save_in_progress; }
    void        setAutoSaveInProgress(bool flag) { m_auto_save_in_progress = flag; }
    static const char*      getTrackerFragmentShader() { return m_tracker_fragment_shader; }

    bool        getModuleUsesLighting() { return m_module_uses_lighting; }
    void        setModuleUsesLighting(bool flag) { m_module_uses_lighting = flag; }

    QSlider*    getLightingSliderPtr(int slider_number) { return m_lighting_slider[slider_number]; }
    InputLCD*   getLightingLcdPtr(int slider_number) { return m_lighting_lcd[slider_number]; }
    QLabel*     getLightingSliderLabelPtr(int slider_number) { return m_lighting_slider_label[slider_number]; }
    bool        getUseLighting();

    void            hideAllLightingSliders();
    void            showAllLightingSliders();
    bool            getSlidersHaveChanged() { return m_sliders_have_changed; }
    void            setSlidersHaveChanged(bool flag) { m_sliders_have_changed = flag; }
    JahLayer*       getImageLayer() { return m_image_layer; }
    void            setImageLayer(JahLayer* layer) { m_image_layer = layer; }
    virtual void    configureModuleEngine(void);
    int             getFramesPerSecond() { return m_frames_per_second; }
    void            setFramesPerSecond(int value) { m_frames_per_second = value; }
    QCheckBox*      getUseLightingCheckbox() { return m_use_lighting_checkbox; }
    QCheckBox*      getTranslateFirstCheckbox() { return m_translate_first_checkbox; }

    JahLayer*       getFirstJahLayer();

    bool            imageLayerNeedsEffectsRedraw(JahLayer* image_layer);

protected:
    QTimer*                 m_auto_save_timer;
    bool                    m_auto_save_in_progress;
    static const char*      m_tracker_fragment_shader;
    QCheckBox*              m_use_lighting_checkbox;
    QCheckBox*              m_translate_first_checkbox;
    JahLayer*               m_image_layer;
    int                     m_frames_per_second;


public:

    //controls the rendering of objects in a scene
    JahRender* renderSpace;

    //the timer controls screen redraw
    QTimer* timer;
    double jahfps;
    
    //The core timer object for fps
    JahTimer    *coretimer;
   
    //set up defalt variables here
    QString renderpath;
    QString scenepath;

    //base path and media path
    QString JahBasePath;
    QString JahMediaPath;

    int gridval;
    bool polySmooth, select;

    int             *clipnumber;            //used to track global clipnumber

    JahLayer*       m_active_layer;
    int             m_currentFrame; 
    int             m_lastLayer;

    int                 getCurrentFrame() { return m_currentFrame; }
    void                setCurrentFrame(int frame_number) { m_currentFrame = frame_number; }
    void                incrementCurrentFrame(int step) { m_currentFrame += step; }
    JahLayer*           getActiveJahLayer() { return m_active_layer; }
    void                setActiveJahLayer(JahLayer* layer) { m_active_layer = layer; }
    LayerListEntry*     getActiveLayer();
    int                 getActiveLayerNumber();
    EffectLayer*        getActiveEffectLayer();
    bool                activeLayerIsAnEffect();
    void                drawAllLayers(int number_of_base_layers, bool draw_only_to_selected_effect = false);

    void                buildEffects(JahLayer* image_layer, bool draw_only_to_selected_effect = false);

    bool                canAcceptEffects(ObjectCategory::TYPE object_category);
    bool                isQimageBlack(QImage& image, char* message);
    bool                multipleLayersSelectedInListView();
    void                addMultiSelectKeyNodes();
    void                removeMultiSelectKeyNodes();
    void                saveEffects(std::string& file_name, JahLayer* jah_layer);
    bool                getAnimating() { return m_animation; }
    void                setAnimating(bool flag) { m_animation = flag; }
    std::string&        getRenderOutputFileNameString() { return m_render_output_file_name_string; }
    void                setRenderOutputFileNameString(std::string name) { m_render_output_file_name_string = name; }
    bool                getStopScript() { return m_stop_script; }
	void                setStopScript(bool flag) { m_stop_script = flag; }
    void                set (bool flag) { m_stop_script = flag; }
    int                 getRedrawDelay() { return m_redraw_delay; }
    void                setRedrawDelay(int delay) { m_redraw_delay = delay; }


    std::string         m_render_output_file_name_string;
    bool                m_stop_script;

    int             minFrame, maxFrame;
    bool            m_animation;
    bool            m_animsliderselected;
    int             m_redraw_delay;
    int             lastx,lasty;
    
    QString         renderext; 
    QString         renderformat; 
    int             renderquality;
    int             JahRenderer;
    
    int             JahResolutionValue;
    QLabel          *jahreslabel;
    QComboBox       *JahresCombo;
    bool            hasResMenu;

    MouseMode       mmode;                  //mouse selection mode

    bool            HWFOG;                  //for hardware fog support
    bool            HWALIASING;             //for hardware antialiasing support
    
    int             numUiOptions;           //for ui options
    bool            forceplay;              //for realtime videlayer playback
    bool            hwaliasing;             //for hardware anti aliasing

    bool            updateEffect;           //for effects
    bool            oldJahLoadfile;         //for old file loading support
    bool            mousePressed;           //for mouse tracking

    
    bool            haskeyframer;           //interface widgets
    JahKeyframer    *thekeyframer;          //the keyframer
    bool            hasnodeframer;
    bool            haswireup;
#ifdef NEWWIREUPWIDGET
    WireWidget      *thewireup;         //the v3 test nodeframer
#else
    JahNodes        *thenodeframer;         //the nodeframer
#endif

    JahDesktopSideTable *mediatable;
    
    //used to load the plugins in
    static JahPluginLib*   jplugin;

    //for global fog
    GLfloat hfog, vfog;

    ///////////////////////////////////////////////////////////////////
    // ui variables and such
    
    QWidget         *page;
    QCheckBox       *forceplaycheckbox;
    QPushButton     *controllerpreviousbutton;
    QPushButton     *controllerstopbutton;
    QPushButton     *controllerplaybutton;
    QPushButton     *controllerffworardbutton;
    QPushButton     *controllernextbutton;
    QPushButton     *controllerrewindbutton;
    TimelineSlider  *controllerslider;
    SupergRangeControl* controllerStartFrameControl;
    SupergRangeControl* controllerEndFrameControl;
    QPushButton     *keyframebutton;
    QLCDNumber      *timecodedisplay;
    QFrame          *imagingframe;
    QPushButton     *imagingphotobutton;
    QPushButton     *imagingrenderbutton;

    //protected:
    QHBoxLayout     *pageLayout;

    //these have been moved here
    QPushButton*   GetButton;

    //////////////////////////////////////
    //for the new dynamic label groups
    bool hasLabels;
    QLabel          *JahModuleLabel[5];
    QString         JahModuleLabelText[5];

    bool hasSliders;

#if 0
    SliderButton    *JahSliders[NUMBER_OF_AXIS_SLIDERS];
    SliderButton*   m_text_extrude_slider;
#else
    QSlider         *JahSliders[NUMBER_OF_AXIS_SLIDERS];
    QSlider*        m_text_extrude_slider;
#endif

    QLabel*         m_text_extrude_label;
    InputLCD*       m_text_extrude_lcd;

    InputLCD        *JahSlidersLCD[NUMBER_OF_AXIS_SLIDERS];
    QLabel          *JahSlidersLabel[NUMBER_OF_AXIS_SLIDERS];

    QString         JahSlidersLabelText[NUMBER_OF_AXIS_SLIDERS];

    bool hasButtons;
    QCheckBox       *JahModuleOption[10];
    QMultiLineEdit     *textEd;
    QCheckBox*      m_select_colors_individually_qcheckbox;
    QCheckBox*      m_pbuffer_select_checkbox;

    InputLCD          *m_slip_frames_lcd;     
    InputLCD          *m_in_frames_lcd;   
    InputLCD          *m_out_frames_lcd;
    InputLCD          *m_key_slip_frames_lcd;  
    InputLCD          *m_key_in_frames_lcd;    
    InputLCD          *m_key_out_frames_lcd;

    QCheckBox*        m_lock_key_and_clip_checkbox;



    ///////////////////////////////////////////////
    //for the onscreen buffer
    virtual void  updateEffects();
    JahToolButton* getStopButton() { return m_stop_button; }
    
    //////////////////////////////////////////////////////////////////////
    //controller variables

    int    Astartframe, Aendframe, Aanimframe; 

    QListView*      m_layer_listview;

    QHBox           *MiddleFrame;       QSlider *     Slider2;
    SupergRangeControl*    m_animframe_lcd;   
    SupergRangeControl*    startFrameControl;
    SupergRangeControl*    endFrameControl;

    QCheckBox       *forcedplay;

    JahToolButton   *scrubfirst, * scrubprevious,  * scrubstop, * scrubplay, * scrubnext, * scrublast;
    
    JahToolButton   *keyback, *keyadd,  *keyremove, *keynext;
    JahToolButton*  m_stop_button;

    QPushButton     *scrubkey, * scrubrenderSlides;
    JahToolButton   * scrubrender, * scrubrenderAll;

    QPushButton*    m_delete_keyframe_button;
    QPushButton*    m_add_keyframe_button;
    QPushButton*    m_prev_keyframe_button;
    QPushButton*    m_next_keyframe_button;

    //buttons that control the listbox
    QPushButton     *AddButton, *AddFxButton, *DelButton, *SceneButton;
    QPushButton*    m_refresh_button;
    QPushButton*    m_undo_button;
    QPushButton*    m_redo_button;
    QPushButton*    m_delete_button;
    QPushButton*    m_copy_one_button;
    QPushButton*    m_copy_all_button;
    QPushButton*    m_paste_button;
    QPushButton     *NameButton;
    QPushButton     *MoveupButton, *MovedownButton;
    FancyPopup      *namepopup;

    //scene control variables
    QWidgetStack    *controlpanel;
    QFrame          *scene_controls;
    QFrame          *object_controls;

    //interface stacks
    //need to switch to an array
    QWidgetStack    *objectControlStack;
    QHBox           *objectControl[10];

    QCheckBox*      GpuSelect;
    QPushButton*    m_select_fps_pushbutton;
    QLabel*         m_select_fps_label;

    QToolButton*    m_src_blend_mode_select_button;
    QToolButton*    m_dst_blend_mode_select_button;
    QToolButton*    m_reset_blend_mode_button;

    QLabel*         m_src_blend_mode_select_label;
    QLabel*         m_dst_blend_mode_select_label;
    QLabel*         m_reset_blend_mode_label;

    QPushButton*    m_compositing_mode_select_button;
    QLabel*         m_compositing_mode_select_label;
    QLabel*         m_compositing_mode_label;

    bool            m_module_uses_lighting;
    QSlider*        m_lighting_slider[12];
    InputLCD*       m_lighting_lcd[12];
    QLabel*         m_lighting_slider_label[12];
    QPushButton*    m_run_script_button;
    QPushButton*    m_save_as_export_button;

public:
    QPushButton*                getRunScriptButton() { return m_run_script_button; }
    virtual void                chooseNewCpuEffect(LayerListEntry* parent, QWidget* thebutton, int x_offset = 0, int y_offset = 0);
    virtual void                addCpuEffectLayer(int effect_id, JahLayer* parent_jah_layer, const char* effect_name);
    virtual void                chooseNewMeshEffect(LayerListEntry* parent, QWidget* thebutton, int x_offset = 0, int y_offset = 0);
    virtual void                addMeshEffectLayer(int effect_id, JahLayer* parent_jah_layer, const char* effect_name);
    virtual void                chooseNewGpuEffect(LayerListEntry* parent, QWidget* thebutton, int x_offset = 0, int y_offset = 0);
    virtual void                addGpuEffectLayer(int effect_id, JahLayer* parent_jah_layer, const char* effect_name);
    
    EffectLayer*        addEffectLayer(LayerListEntry* parent, QString& title, 
                                       EffectInfo::EFFECT_TYPE effect_type, int plugin_id, std::string& guid);
    EffectLayer*        addEffectLayer(JahLayer* parent, std::string& title, 
                                       EffectInfo::EFFECT_TYPE effect_type, int plugin_id, std::string& guid);
    int                 addPluginsToMenu(int usingclass, int existingitems, QPopupMenu *menu);
    void                setPluginAxisSliderLabels(EffectLayer* effect_layer);
    virtual void        setDefaultAxisSliderLabels();
    virtual void        setSliderNames(EffectLayer* effect_layer);
    virtual void        applySliderNames(EffectLayer* effect_layer);

    virtual void        setLayerName(JahLayer* jah_layer, const char* layer_name);


    void parsePluginOption(int option);  //used for path global for layers


    void                doNegative(JahLayer* base_layer, QImage& theimage);
    void                doSwapRGB(JahLayer* base_layer, QImage& theimage);
    void                doMirror (JahLayer* base_layer, QImage& theimage);

    //math based functions
    void                doGrey(JahLayer* base_layer, QImage& pix);
    void                doContrast(JahLayer* base_layer, QImage& pix );
    void                doBrightness(JahLayer* base_layer, QImage& pix );

    void                doMatrixSharpen(JahLayer* base_layer, QImage& pix);
    void                doMatrixBlur(JahLayer* base_layer, QImage& pix);
    void                doMatrixEmboss(JahLayer* base_layer, QImage& pix);

    void                doEdge(JahLayer* base_layer, QImage& pix);
    void                doColorSelect(JahLayer* base_layer, QImage& pix);
    //void doBlur(QImage& pix);
    void                doGaussianBlur(JahLayer* base_layer, QImage& pix);

    void                doCPUColorize(JahLayer* base_layer, QImage& pix);
    void                doChromaKeyerCPU(JahLayer* key_layer, QImage& pix);
    void                cpuChromaKeyer(JahLayer* key_layer, int buffer_width, int buffer_height, GLuint key_texture_id);

    //opengl accelerated
    void                doColorMatrixRt(JahLayer* base_layer);
    void                doBlurRt(JahLayer* base_layer);
    void                doSharpenRt(JahLayer* base_layer);
    void                doContrastRt(JahLayer* base_layer);

    //opengl mesh accelerated functions
    void                doFlagRt(JahLayer* base_layer);
    void                doRippleRt(JahLayer* base_layer);
    //void doBaseLayer(void);

    //opengl geometry based mesh effects
    void                doSwirlRt(JahLayer* base_layer);
    void                doFisheyeRt(JahLayer* base_layer);

    //////////////////////////////////////////////////////////////
    //gpu based effects
    void                doNV30CharcoalGPU(JahLayer* base_layer);
    void                doNV30LightingGPU(JahLayer* base_layer);
    void                doNV30EarthquakeGPU(JahLayer* base_layer);

    void                doNV30Fire3DGPU(JahLayer* base_layer);

    void                doColorizeGPU(JahLayer* base_layer);
    void                doNV30ColorizeGPU(JahLayer* base_layer);
    void                doArbColorizeGPU(JahLayer* base_layer);

    void                doChromaKeyerGLSLSeparateAlpha(JahLayer* key_layer);
    void                doChromaKeyerGLSL(JahLayer* key_layer);

    void                doColorCorrectorGLSL(JahLayer* image_layer);
    void                doStabilizeGLSL(JahLayer* image_layer);

    void                doStretchCrop(JahLayer* image_layer);

    


    // GPGPU effects
    void                doNV30FisheyeGPGPU(QImage& pix);
    void                doNV30SwirlGPGPU(QImage& pix);

    void                getMeshNormal(JahLayer* base_layer, float4& normal, int mesh_x_coordinate, 
                                      int mesh_y_coordinate, int mesh_x_dimension, 
                                      int mesh_y_dimension);

    void                doPlugin(JahLayer* image_layer, int plugin_number, QImage& pix);
    void                udpatePluginSettings(int pl);
    static bool         updateQimageFromTexture(QImage* q_image, TextureImage& texture_image);
    static bool         updateTextureFromQImage(TextureImage& texture_image, QImage* q_image, JahLayer* jah_layer);
    static bool         updateQimageFromRgbaBuffer(QImage* q_image, unsigned int* image_buffer, int image_width, int image_height);



    bool        noiseMatrixInitialized;
    int*        permutation_table;

    float4*     gradient_table;

    bool        fireTextureInitialized;
    bool        randomTextureInitialized;
    bool        contrastTextureInitialized;

    GLuint      gradient_texture;
    GLuint      permutation_texture;
    GLuint      turbulence_texture;
    GLuint      random_texture;
    GLuint      contrast_texture;
    float       z_offset;


    typedef void (GLWorld::*CpuEffectFunction)(JahLayer* base_layer, QImage&);
    typedef void (GLWorld::*MeshEffectFunction)(JahLayer* base_layer);
    typedef void (GLWorld::*GpuEffectFunction)(JahLayer* base_layer);

    static int getNumberEmbeddedCpuEffects() { return m_number_embedded_cpu_effects; }
    static int getNumberEmbeddedMeshEffects() { return m_number_embedded_mesh_effects; }
    static int getNumberEmbeddedGpuEffects() { return m_number_embedded_gpu_effects; }

    std::vector<EffectInfo*>*   getCpuEffectsSelectionVector() { return &m_cpu_effects_selection_vector; }
    std::vector<EffectInfo*>*   getMeshEffectsSelectionVector() { return &m_mesh_effects_selection_vector; }
    std::vector<EffectInfo*>*   getGpuEffectsSelectionVector() { return &m_gpu_effects_selection_vector; }

    CpuEffectFunction   getCpuEffectFunction(int i) { return m_cpu_effects_dispatch_table[i]; }
    MeshEffectFunction  getMeshEffectFunction(int i) { return m_mesh_effects_dispatch_table[i]; }
    GpuEffectFunction   getGpuEffectFunction(int i) { return m_gpu_effects_dispatch_table[i]; }

    void        callEffect(EffectInfo::EFFECT_TYPE effect_type, JahLayer* effect_layer, 
                           JahLayer* base_layer, QImage& newimage);

    JahLayer*   getCurrentEffectLayer() { return m_current_effect_layer; }
    void        setCurrentEffectLayer(JahLayer* layer) { m_current_effect_layer = layer; }
    float       getCameraDistance() { return m_camera_distance; }

    GlslShader*                 getFragmentShader() { return m_fragment_shader; }
    GlslShader*                 getVertexShader() { return m_vertex_shader; }
    GlslProgram*                getShaderProgram() { return m_shader_program; }
    std::vector<GlslShader*>    getShaderVector() { return m_shader_vector; }
    GLhandleARB                 getShaderProgramHandle() { return m_shader_program_handle; }
    bool                        getGpuActive() { return m_gpuactive; }
    void                        setGpuActive(bool flag) { m_gpuactive = flag; }
    int                         findLayer(JahLayer* layer);
    int                         findEffectParent(JahLayer* layer);
    void                        executeEffectsList(JahLayer* image_layer, bool use_pbuffer, bool draw_only_to_selected_effect = false);


    GLuint*     getCompositeTextureIdPtr() { return &m_composite_texture_id; } 
    GLuint      getCompositeTextureId() { return m_composite_texture_id; } 
    void        setCompositeTextureId(GLuint id) { m_composite_texture_id = id; }
    float4*     getCompositeSizeRatioPtr() { return &m_composite_size_ratio; }
    void        glslChromaKeyer(JahLayer* key_layer, GLuint base_texture_id, GLuint key_texture_id);
    void        glslChromaKeyer2(JahLayer* key_layer, GLuint key_texture_id);
    void        saveEffectRenderingInBaseTexture(JahLayer* base_layer);
    bool        setTextureImageParameters(TextureImage* texture_image, int texture_width, int texture_height, 
                         int buffer_width, int buffer_height, GLuint texture_id);

protected:
    GLuint          m_src_blend_factor;
    GLuint          m_dst_blend_factor;

public:
    GLuint          getSrcBlendFactor() { return m_src_blend_factor; }
    GLuint          getDstBlendFactor() { return m_dst_blend_factor; }
    void            setSrcBlendFactor(JAH_GL_BLEND_MODE factor);
    void            setDstBlendFactor(JAH_GL_BLEND_MODE factor);
    JahControl*     getJahControl() { return m_jah_control; }
    void            setJahControl(JahControl* jah_control) { m_jah_control = jah_control; }
#ifndef JAHPLAYER
    void            runScript(std::string& script_file_name);
#endif
    void            setCompositeType(CompositeType::TYPE type);
    void            updateCompositeButton(CompositeType::TYPE type);

protected:
    static int          m_number_embedded_cpu_effects;
    static int          m_number_embedded_mesh_effects;
    static int          m_number_embedded_gpu_effects;

    static const char*  m_color_correction_fragment_shader;

    std::vector<CpuEffectFunction>      m_cpu_effects_dispatch_table;
    std::vector<MeshEffectFunction>     m_mesh_effects_dispatch_table;
    std::vector<GpuEffectFunction>      m_gpu_effects_dispatch_table;

    std::vector<EffectInfo*>        m_cpu_effects_selection_vector;
    std::vector<EffectInfo*>        m_mesh_effects_selection_vector;
    std::vector<EffectInfo*>        m_gpu_effects_selection_vector;

    static bool         m_noise_matrix_initialized;
    static bool         m_contrast_texture_initialized;
    static bool         m_random_texture_initialized;
    static int*         m_permutation_table;
    static float4*      m_gradient_table;

    static GLuint       m_gradient_texture;
    static GLuint       m_permutation_texture;
    static GLuint       m_turbulence_texture;
    static GLuint       m_random_texture;
    static GLuint       m_contrast_texture;

    JahLayer*           m_current_effect_layer;
    float               m_camera_distance;
    bool                m_gpuactive;
    
    GLuint              m_composite_texture_id;
    float4              m_composite_size_ratio;

    bool                    m_select_colors_individually;
    bool                    m_gpu_select;
    GLuint                  m_base_texture_id;
    GLuint                  m_key_texture_id;
    bool                    m_textures_initialized;
    bool                    m_use_textures_for_readback;
    GLint                   m_red_limits_handle;
    GLint                   m_green_limits_handle;
    GLint                   m_blue_limits_handle;
    GLint                   m_select_colors_individually_handle;
    GLint                   m_create_chroma_alpha_mask_handle;
    GLint                   m_clear_color_handle;
    GLint                   m_soften_value_handle;
    float                   m_red_low;
    float                   m_red_high;
    float                   m_green_low;
    float                   m_green_high;
    float                   m_blue_low;
    float                   m_blue_high;
    float                   m_alpha_low;
    float                   m_alpha_high;
    float                   m_soften_value;
    float4                  m_clear_color;
    JahLayer*               m_chroma_base_layer;
    JahLayer*               m_chroma_key_layer;
    JahLayer*               m_chroma_settings_layer;
    JahControl*             m_jah_control;

    struct 
    {
        int         red_low;
        int         red_high;
        int         green_low;
        int         green_high;
        int         blue_low;
        int         blue_high;
        int         alpha_low;
        int         alpha_high;
        int         soften_value;
    } m_chroma_keyer_range;



    std::vector<GlslShader*>    m_shader_vector;
    GlslShader*                 m_vertex_shader;
    GlslShader*                 m_fragment_shader;
    GlslProgram*                m_shader_program;
    GLhandleARB                 m_shader_program_handle;

	int curr_width_;
	int curr_height_;

public:

    bool m_render_at_image_resolution;

	// Used by the implementation of scheduleUpdate
	virtual void updatePosition( );
	virtual bool usesVideoHead( ) { return true; }
	virtual void schedulePosition( );
    virtual void setObjectTab( JahLayer* jah_layer ); 
    virtual void setPointLayer(int index, JahLayer* layer) { m_point_layer[index] = layer; }
    virtual JahLayer* getPointLayer(int index) { return m_point_layer[index]; }


protected:
	virtual bool usesKeyFrames( ) { return true; }

/////////////////////////////////////////////////////
// need to figure out what is not a slot and make
// sure its not under 'slots'

public slots:
   	virtual void scheduleUpdate( );

    void slotUpdateAnimation();
    void slotSetUseOpenGlKeyer();
    void slotSetUseFastShaderKeyer();

	void slotSetSrcBlendFactor();
    void slotSetDstBlendFactor();
    void slotResetBlendFactor();
    void slotSetCompositingMode();

    void slotLightColorAmbientRed();
    void slotLightColorAmbientGreen();
    void slotLightColorAmbientBlue();

    void slotLightColorDiffuseRed();
    void slotLightColorDiffuseGreen();
    void slotLightColorDiffuseBlue();

    void slotLightColorSpecularRed();
    void slotLightColorSpecularGreen();
    void slotLightColorSpecularBlue();

    void slotLightPosition();

    virtual void slotLightColorAmbient(QColor color);
    virtual void slotLightColorDiffuse(QColor color);
    virtual void slotLightColorSpecular(QColor color);

    void slotChooseFramesPerSecond();
    void slotSetShowLights();


    //used to get assets from the desktop
    void grabDesktop(void)      { emit GrabDesktop();      };

    //opengl routine
    virtual void paintGL();

    virtual void    layerClicked(QListViewItem* item);
    virtual void    effectNodeClicked( int layernumber );
    
    virtual void    mousePressEvent( QMouseEvent *e);
    virtual void    mouseReleaseEvent( QMouseEvent *e );
    virtual void    mouseMoveEvent( QMouseEvent  *e );
    virtual void    setSelectColorsIndividually(void);

    virtual void connectKeyframer( void ) ;
    virtual void addKeyframer( QWidget* parentwidget ) ;

    virtual void setResolution(int renval);
    virtual void updateResMenu(int renval);

    // layer controls
    virtual void setXRotation( int degrees );    
    virtual void setYRotation( int degrees );    
    virtual void setZRotation( int degrees );
    
    virtual float setScaleValue ( int   );
    virtual int   getScaleValue ( float );
    
    virtual void setXScale   ( int degrees );    
    virtual void setYScale   ( int degrees );    
    virtual void setZScale   ( int degrees );
    virtual void setXTrans   ( int degrees );    
    virtual void setYTrans   ( int degrees );    
    virtual void setZTrans   ( int degrees );
    virtual void setAlpha    ( int degrees );    
    virtual void setZoom     ( int degrees );    
    virtual void setExtrude  ( int degrees );

    virtual void setColor(void); 

    virtual void setKeyframeDrawStatus(void);
    virtual void slotSetLayerSelectedVisible();
    virtual void resetAllSliders(void);

    //for spaceball
    virtual void spaceEvent  ( double x, double y, double z, double a, double b, double c );

    //for 3d objects
    virtual void loadObjObject(void);

    // for video clips
    virtual void slotSetSlipFrames    ( int frame );
    virtual void setinFrames      ( int frame );
    virtual void setoutFrames     ( int frame );
    virtual void slotSetKeySlipFrames   ( int frame );
    virtual void setinKeyFrames   ( int frame );
    virtual void setoutKeyFrames  ( int frame );

    //for editing
    virtual void noTool( void );    
    virtual void toolTranslate( void );    
    virtual void toolScale( void );     
    virtual void toolRotate( void );
    virtual void changeZoom( int val );
    virtual void resetZoom( void );
	virtual GLfloat setZoom( GLfloat value ) { return value; }

    virtual void setHFog(int hf);
    virtual void setVFog(int vf);
    virtual void updateFog(void);

    virtual void SetGpuSelect(void);

    virtual void updateToolDisplay(void);

    virtual void setKeyStatus(void);
    virtual void setClipStatus(void);
    
    virtual void  toggleForegroundStatus();
    virtual void  toggleDepthStatus();

    void            setTextLayerText(JahLayer* text_layer, const char* text_string);
    void            setText(const char* text_string);

    virtual void  setText(const QString& q_string);    void  setFontColor( void );    void  loadFont( void );
    virtual void  setTextSafe( void );

    virtual void  setGrid( void );
    virtual void  setSmooth( void );
    virtual void  setBlur( void );
    virtual void  setFog(void);

    virtual void  InvertKeyData(void);

    ///////////////////////////////////////////////////////////////////
    //slider control
    virtual void  updateSliders();      //to be overridden...
    virtual void  slotUpdateListviewCheckboxes(); 
    virtual void  updateSliderValues(); //to be overridden...
    virtual void  updateSliders(motionNode*); //to be overridden...
    virtual void  updateUiOptions();    //to be overridden...
    virtual void  updateUiSettings();   //to be overridden...

    virtual void hideAllHeadings( );
    virtual void showAllHeadings( );
    virtual void showHeadings(int ns );
    virtual void setHeadingValue(int heading, QString value);
    virtual void setDefaultHeadings(void);

    virtual void hideAllSliders( );
    virtual void hideSliders( int ns );
    virtual void showAllSliders( );
    virtual void showSliders(int ns );
    virtual void showSlider(int, bool);

    virtual void setSliderValue(int slider, int value);
    virtual void setSliderLabelValue(int slider, QString value);
    virtual void setDefaultSliderText(void);


    virtual void showOptions(int ns );
    virtual void hideOptions(int ns );

    void setOptionValue(int option, bool value);

    virtual void  sliderselected();
    virtual void  slotTimeSliderReleased();
    virtual void  updateTimeSlider(int value);
    virtual void  sliderValueChanged(int value);

    virtual void  updatesliderStartframe(int);
    virtual void  updatesliderEndframe(int);

    virtual void toggleForcePlay(void);
    virtual void setForcePlay(bool status);

    virtual void firstframeanimation(void);
    virtual void previousframeanimation(void);
    virtual void nextframeanimation(void);
    virtual void lastframeanimation(void);

    //for when you hit the play and stop buttons
    virtual void  startanimation();
    virtual void  stopanimation();
    virtual void  toggleAnimation(bool value);

    virtual void  updateAnimation(int Frame, bool force = false);

    //key frame routines
    virtual void  addKeyframe();
    virtual void  removeKeyframe();
    virtual void  prevKeyframe();
    virtual void  nextKeyframe();
    virtual void  slotKeyframeChanged();
    virtual void  updateKeyframeDisplay();

    virtual bool isLayerVisible(JahLayer* jah_layer);

    //used to render from module out to desktop
    virtual QImage  Photo(assetData);

    virtual QString buildFrameName(int frameval, assetData theclip );

    virtual QImage  RenderAll(assetData,int,int);
    
    virtual void  renderGL(void);
    virtual void  Render(void);
    virtual void  RenderScene(void);
    virtual void  setSceneToRender(bool renStatus);

    virtual QString checkandcleandir(QString destDir, int clipnum);

    virtual void  checkRenderQuality(void);
    virtual void  checkJahRenderer(void);
    virtual void  setRenderQuality(int quality);
    
    //for load and save buttons
    virtual void  SceneLoad(void);
    virtual void  SceneAppend(void);
    virtual void  importFx(void);
    virtual void  saveEffects(void);
    virtual bool  SceneLoadName(QString);
    virtual bool  ModelLoadName(QString);
    virtual void  sceneSave(void);
    virtual void  PackageSave(void);

    virtual void  getScript();
    virtual void  stopScript();

    virtual void  saveAsExport();


    //used for vga emulated video head
    //the slot thats called by a checkbox
    //shouldnt be here but in the modules ui code
    //virtual void    SetVideoRender(void);
    virtual void    SetVideoRender(bool);
    virtual void    getRenderResolution(int &, int&);

    virtual void    SoftPhoto(QImage&);

    //used to grap asset from desktop and add to layer
    virtual void    addClip(JahLayer* jah_layer, assetData clip);
    virtual void    addClip(assetData);
    virtual void    addClipKey(assetData);

    virtual void    nameLayer(void);
    virtual void    nameLayerFromTopMenu(void);

    virtual void    ResetAll(void);
    virtual void    ResetLayer(void);
    virtual void    ClearAll(void);
    virtual void    loadClearAll(void);

    virtual void  setlayerName(QString);
    virtual void  toggleLight(void);
    virtual void  toggleMesh(void);
    virtual void  setUsePbuffer();

    virtual void  toggleExtendHeadTail(void);
    virtual void  toggleLoop(void);
    virtual void  togglePingPong(void);
    virtual void  toggleShader1(void);
    virtual void  toggleShader2(void);

    virtual void  toggleReflect(void);
    virtual void  toggleSmooth(void);
    virtual void  slotShowLights();

    virtual void  changeParticleColors();

    virtual void toggleStatsDisplay(void);

    virtual bool  getSelectionTree(QListView* listv, int &layer, int &effect, bool &status);

    virtual void  addSelectionFx(bool status = TRUE, QString fxname = 0, bool headtail = FALSE, bool set_selected = true);
    virtual QListViewItem* addEffectToUserInterface(QListViewItem* parent, QString& name, 
                                            bool checked, bool selected);

    virtual void  clearSelection(QListView *thelv);
    virtual void  changeSelection(int layer);

    virtual void  setLayerVisible( int, bool  );  //this can be overridden if necessary

    //change layer type
    virtual void ChangeObjectLayer(void);
    virtual void ChangeObjectCube(void);
    virtual void ChangeObjectCylinder(void);
    virtual void ChangeObjectSphere(void);
    virtual void ChangeObjectBezier(void);
    virtual void ChangeObjectMesh(void);


    virtual void  delLayer(void);
    virtual void  deleteEffectsList(JahLayer* layer);
    virtual void  deleteLightsList(JahLayer* layer);
    virtual void  moveLayerUp(void);
    virtual void  moveLayerDown(void);

    //layer keframe control routines
    virtual void addkeyNodes(JahLayer * layer, int theframe);
    virtual void removekeyNodes(JahLayer * layer, int theframe);
    virtual void updatekeyNodes(JahLayer * layer,int currentFrame);
    virtual void updateKeyNodes();
    virtual void clearkeys(JahLayer * layer);
    virtual void resetkeys(JahLayer * layer);

    virtual void autoSaveThisModule();

signals:
    virtual void    updateDesktop( assetData );
    
    //this is for the network module and http feedback
    virtual void    updateBrowser( QString debugmessage );
    void hideUI( bool );
    void    GrabDesktop(void);       //linked to main jahcontrol to talk to desktop
    void showInHead( JahHeadable * );

private:
	void lazy( );

    //new variables for color conversion mode control
    bool        flipColorData;
    GLenum      texture_format;
    int         texture_mode;

    QString jahLoadfilename;

	// Indicator for module started
	bool m_started;

public:

    // These are general method which don't seem to have a home in the current
    // jahshaka design - they allow threads to interact safely with the QT and X
    // components - provided here as static methods to allow use in C libraries
    // via pointer to function passing
    
    // The placement is wrong though - they're not world specific...

    static void qt_lock( ) { qApp->lock( ); }
    static void qt_unlock( ) { qApp->unlock( ); }

};


#endif // GLWORLD_H

