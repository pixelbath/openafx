###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =   lib
CONFIG      +=  staticlib 
HEADERS     =   	glworld.h \
                    glworldgpueffects.h \
                	glworldlists.h 

SOURCES     =	glworld.cpp \
			glworldanimation.cpp \
			glworldcontrols.cpp \
			glworldcpueffects.cpp \
			glworlddataio.cpp \
			glworlddataload.cpp \
			glworlddatasave.cpp \
			glworldeffect.cpp \
			glworldfeedback.cpp \
			glworldflags.cpp \
			glworldglcore.cpp \
			glworldgpueffects.cpp \
			glworldgpucolorize.cpp \
			glworldinterface.cpp \
			glworldlayer.cpp \
			glworldlayerkeys.cpp \
			glworldlayerutils.cpp \
			glworldlists.cpp \
			glworldmesheffects.cpp \
			glworldplugineffects.cpp \
			glworldrender.cpp \
			glworldsliders.cpp

TARGET = jahworld
DEPENDPATH = $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           .  

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile 
}

INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
                        ../../../AuxiliaryLibraries/FTGL \
                        ../../../AuxiliaryLibraries/particle \ 
                        ../../../AuxiliaryLibraries/blur 
                        
INCLUDEPATH +=		../../../OpenLibraries/openobjectlib \
			../../../OpenLibraries/openobjectlib/surface3d \
			../../../OpenLibraries/openmedialib \
			../../../OpenLibraries/opencore \
			../../../OpenLibraries/openimagelib \
			../../../OpenLibraries/openassetlib \
			../../../OpenLibraries/opengpulib \
			../../../OpenLibraries/openmedialib/mediaobject 

INCLUDEPATH +=          ../../JahSource/jahmain
INCLUDEPATH +=          ../../JahDesktop/desktop
INCLUDEPATH +=          ../../JahModules/effect
INCLUDEPATH +=          ../../JahModules/animation

INCLUDEPATH +=          ../../JahCore/jahobjects \
			../../JahCore/jahrender \
			../../JahCore/jahworld

INCLUDEPATH +=          ../../JahWidgets/interfaceobjs \ 
			../../JahWidgets/nodes \ 
			../../JahWidgets/keyframes \
            ../../JahWidgets/wireup \
                        ../../JahWidgets/mediatable

INCLUDEPATH +=		../../JahLibraries \
			../../JahLibraries/jahtracer \
			../../JahLibraries/jahtimer \
			../../JahLibraries/jahkeyframes \
			../../JahLibraries/jahpreferences \
			../../JahLibraries/jahformatter \
			../../JahLibraries/jahtranslate \
			../../JahLibraries/jahplugins \
			../../JahLibraries/jahdataio \
			../../JahLibraries/jahglcore \
			../../JahLibraries/jahxml \
			$$FREEDIR

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT, true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

contains( MLTSUPPORT, true ) {
	INCLUDEPATH += ../../JahModules/editing_mlt
}
				
#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH += 		$$SGIDIR
}

#patch for glx qt namespace conflicts		
QMAKE_CXXFLAGS+="-DQT_CLEAN_NAMESPACE"

			

