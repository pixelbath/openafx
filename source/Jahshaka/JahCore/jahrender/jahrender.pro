###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE = lib
CONFIG		+= staticlib 

HEADERS		= 	jahrender.h 

SOURCES		= 	jahrender.cpp \
                        jah3dobjs.cpp \
                        jahfonts.cpp \
                        jahlights.cpp \
                        jahmeshes.cpp \
                        jahobjects.cpp \
                        jahparticles.cpp \
                        jahwidget.cpp \
				jahrendermanager.cpp 

TARGET 		= 	jahrender
DEPENDPATH = $$JAHDEPENDPATH

###############################################################
#the project related includes            
INCLUDEPATH =           .  

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile 
}

INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
                        ../../../AuxiliaryLibraries/FTGL \
                        ../../../AuxiliaryLibraries/particle 
                                               
INCLUDEPATH +=		../../../OpenLibraries/openobjectlib \
				../../../OpenLibraries/openmedialib \
				../../../OpenLibraries/opengpulib \
				../../../OpenLibraries/opencore \
				../../../OpenLibraries/openimagelib \
				../../../OpenLibraries/openassetlib \
				../../../OpenLibraries/openmedialib/mediaobject 

INCLUDEPATH +=          ../../JahCore/jahobjects \
				../../JahCore/jahrender \
				../../JahCore/jahworld

INCLUDEPATH +=          ../../JahWidgets/interfaceobjs \ 
			../../JahWidgets/nodes \ 
			../../JahWidgets/keyframes \
            ../../JahWidgets/wireup \
                        ../../JahWidgets/mediatable


INCLUDEPATH +=		../../JahLibraries \
				../../JahLibraries/jahplugins \
				../../JahLibraries/jahtracer \
				../../JahLibraries/jahkeyframes \
				../../JahLibraries/jahpreferences \
				../../JahLibraries/jahformatter \
				../../JahLibraries/jahdataio \
				../../JahLibraries/jahtimer \
				../../JahLibraries/jahglcore \
				../../JahLibraries/jahtranslate \
				$$FREEDIR

INCLUDEPATH +=          ../../JahDesktop/desktop
INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew 


# openobjectlib support
contains( OPENOBJECTLIBSUPPORT, true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
win32{
    INCLUDEPATH += C:\freetype\include
}

contains( JAHOS,IRIX ) {
INCLUDEPATH += 		$$SGIDIR
}
			


