###############################################################
#
# VisualMedia OpenLibraries QMake build files
#
###############################################################

include( ../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Configuring OpenLibraries" )
message( "-------------------------" )

SUBDIRS += JahCore
SUBDIRS += JahDesktop
SUBDIRS += JahLibraries
SUBDIRS += JahModules
SUBDIRS += JahSource
SUBDIRS += JahWidgets

TEMPLATE = subdirs

