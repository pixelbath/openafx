/*******************************************************************************
**
** The header file for the Jahshaka player module
** The Jahshaka Project
** Copyright (C) 2000-2006 VM Inc.
** Released under the GNU General Public License
**
*******************************************************************************/

#include "MainEncoder.h"
#include "jahtranslate.h"

#include <qtplayer.h>
#include <qhbox.h>
#include <qvbox.h>
#include <qtabwidget.h>
#include <qlistview.h>
#include <qpushbutton.h>
#include <qlayout.h>
#include <qtimer.h>
#include <qcombobox.h>
#include <qstylefactory.h>
#include <qwidgetstack.h>
#include <qlabel.h>
#include <qpopupmenu.h>
#include <qlineedit.h>
#include <qprogressdialog.h>
#include <qspinbox.h>
#include <qapplication.h>
#include <qfiledialog.h>

#include <jahformatter.h>
#include <valueFormatters.h>
#include <widget.h>
#include <supergrangecontrol.h>
#include <timelineSlider.h>
#include <jahpreferences.h>
#include <jahtracer.h>

#include <assetexchange.h>
#include <render.h>

double MainEncoder::fpsVal[NumFPS];


//==============================================================================
MainEncoder::MainEncoder( const QGLWidget *core, QWidget *parent, const char* name, QHBox* control, QHBox* controller,
	QHBox* leftcontroller, QHBox* rightcontroller) 
	: QWidget (parent, name)
	, m_core( core )
	, m_parent( parent )
	, m_control( control )
	, m_controller( controller )
	, m_leftcontroller( leftcontroller )
	, m_rightcontroller( rightcontroller )
	, m_started( false )
	, m_jahnle_render( 0 )
{
    ModuleName = name;
    minFrame = 0;
    maxFrame = 0;
}

MainEncoder::~MainEncoder()
{
	delete m_jahnle_render;
}

void MainEncoder::activate( )
{
	if ( m_started ) return;

	m_started = true;

    m_jah_translate = JahTranslate::getInstance();

	//initalize the global vars from the prefs
    jtrace = JahTrace::getInstance();	//set up tracer
    JahPrefs& jprefs = JahPrefs::getInstance();

    JahBasePath  = jprefs.getBasePath().data();

    buildController(m_controller);

    //restore the preferences note that its not a dialog
    loadPreferences();

    /////////////////////////////////////////////////////////
    //create the ui for the player

    mainworldLayout = new QHBoxLayout( m_parent);

    encplayer = new QtPlayer( m_core, "test:", 0, m_parent);
    mainworldLayout->addWidget( encplayer );

    //////////////////////////////////////////////////////////
    //this creates the interface of the module for us
    buildInterface( m_control );

    //////////////////////////////////////////////////////////////////////////
    // connect the slots

    connect(encplayer, SIGNAL( showImage( int, QImage * ) ), this, SLOT( showImage( int, QImage * ) ) );    //connect player drag n drop to player load signal
    connect(encplayer, SIGNAL(draggedAImage()), this, SLOT(draggedAImage()) );
    connect(encplayer, SIGNAL(resizePlayer(mediaSize)), this, SLOT(ResizeDisplay(mediaSize)) );

    //////////////////////////////////////////////////////////
    //create our timer for the player
    timer = new QTimer(this);
	//playonce = false;
    //connect(timer, SIGNAL(timeout()), this, SLOT(Action()) );

	framerateCounter = 0;
	accumulated = 0;

    //create config object to be passed to the encoder
    encodingStatus = false;
}

void MainEncoder::loadPreferences()
{
    //different frame rates
    fpsVal[F90] = 90.0;
    fpsVal[F60] = 60.0;
    fpsVal[F48] = 48.0;
    fpsVal[F30] = 30.0;
    fpsVal[F24] = 24.0;
    fpsVal[F20] = 20.0;
    fpsVal[F15] = 15.0;
    fpsVal[F12] = 12.0;
    fpsVal[F10] = 10.0;

    //default to 30fps
    currentSpeed = fpsVal[F30];

    //default values
    curFrame	=  0;
    pingpong	=  1;
    frameStep   =  1;
}


///////////////////////////////////////////////////////////////
// this is the main calling routine

void MainEncoder::LoadMySequence(assetData desktopclip)
{
    //the local instance of the asset
    theassetData = desktopclip;
	QString filename = theassetData.location+theassetData.filename+theassetData.extension;

    switch (desktopclip.theCategory)
    {
	    case VideoCategory::IMAGE:
			filename += QString( "/sequence:" );
			break;

		case VideoCategory::MEDIA:
		case VideoCategory::CLIP:
            if (theassetData.theType != VideoType::MOVIE)
				filename = theassetData.location + theassetData.filename + QString( "*" ) + theassetData.extension + QString( "/sequence:" );
			break;

		case VideoCategory::AUDIO:
		default:
			break;
    }
    
    filename.replace( "//", "/" );

    encplayer->show( );
	encplayer->play( filename, 0 );

    controllerslider->setRange( 0,  encplayer->length( ) );

    //this sets up the interface values
    LoadSequenceCompleted(1, true);

    SetFrameNumber( 0 );
}

void	MainEncoder::SetFrameNumber(int i)
{
    int frame = i;

    if (maxFrame == 0)
    {
        // No clip loaded yet
        return;
    }

    if(frame > (int)maxFrame) 
    {
        frame = minFrame;
        encplayer->seek(frame);
        encplayer->stop();
    }
    
    if(frame < (int)minFrame) 
    {
        frame = minFrame;
        encplayer->seek(frame);
    }

    frameNumString.setNum( frame );
    animframe->setValue( frame );
    controllerslider->setValue( frame );
}

void	MainEncoder::Clear()
{
    Stop();

    //encplayer->Reset();

    //zero the sliders and settings
    curFrame	=  0;
    pingpong	=  0;
    frameStep   =  1;
}

//used for external access to plyaer
void	MainEncoder::ClearOut()
{
    Clear();
}

void	MainEncoder::LoadSequenceCompleted(int, bool)
{
    startFrame = theassetData.startframe;
    minFrame = 0;
    maxFrame = encplayer->length( );

    controllerslider->setValue( 0 );

}

void MainEncoder::sliderValueChanged(int frame_number)
{
	if ( encplayer->speed() == 0 )
    {
		encplayer->seek(frame_number);
    }
}

//--------------------------------------------------------------------
void	MainEncoder::SetMinFrame(int value)
{
	if ( value <= int( maxFrame ) )
	{
            minFrame = value;
            controllerslider->setRange(minFrame, maxFrame);
	}
}

void	MainEncoder::SetMaxFrame(int value)
{
	if ( value >= int( minFrame ) )
	{
            maxFrame = value;
            controllerslider->setRange(minFrame, maxFrame);
	}
}

//--------------------------------------------------------------------
void	MainEncoder::LoopForward()
{
	encplayer->set_speed( 1 );
}

void	MainEncoder::setLoopForward()
{
    bool isact = false;

    if (timer->isActive())
        { timer->stop(); isact=true; }


        playMode = false;
        pingpong = false;

		framerateCounter = 0;
		accumulated = 0;

        //timer speeds here
        if (isact) 
        { 
            timer->start((int)(1000.0/currentSpeed)); 
        }
}

//--------------------------------------------------------------------
void	MainEncoder::PingPong()
{
    bool isact = false;

    if (timer->isActive())
        { timer->stop(); isact=true; }

    playMode = true;
    pingpong = true;

	framerateCounter = 0;
	accumulated = 0;

    if (isact) 
    { 
        timer->start((int)(1000.0/currentSpeed)); 
    }

}
//stops the timer if its running
void	MainEncoder::Stop()
{
	if ( encplayer ) encplayer->stop();
}

void	MainEncoder::FastForward()
{
	if ( encplayer ) encplayer->set_speed( 25 );
}

void	MainEncoder::FrameForward()
{
	if ( encplayer ) encplayer->seek( encplayer->position( ) + 1 );
}

void	MainEncoder::FrameBackward()
{
	if ( encplayer ) encplayer->seek( encplayer->position( ) - 1 );
}

void	MainEncoder::Rewind()
{
	if ( encplayer ) encplayer->set_speed( -25 );
}

//--------------------------------------------------------------------
void MainEncoder::Action()
{
    //if frame==lastframe then stop timer for regular play
    switch(playMode)
    {
        case LOOP_FORWARD:
            ActionLoopForward();
            break;
        case PING_PONG:
            ActionPingPong();
            break;
        default:
            //warning ("MainEncoder::playMode is set incorrectly.\nplayMode = %d", playMode);
            timer->stop();
    }
}

//- - - - - - - - - - - - - - - - - - - - - - -
void MainEncoder::ActionLoopForward()
{
        curFrame += frameStep;

        if (curFrame > maxFrame)
        {
            if (playonce)
            {
                timer->stop();
                curFrame = maxFrame;
            }
            else
            {
                curFrame = minFrame + 1;
            }
        }

        controllerslider->setValue(curFrame);
        SetFrameNumber(curFrame);
}

void MainEncoder::ActionPingPong()
{
    curFrame += pingpong;
    if (curFrame > maxFrame)
    {
        curFrame = maxFrame;
        pingpong = -frameStep;
    }
    else if (curFrame < minFrame)
    {
        curFrame = minFrame;
        pingpong = frameStep;
    }

    controllerslider->setValue(curFrame);
    SetFrameNumber(curFrame);
}

//--------------------------------------------------------------------
void MainEncoder::switchFps(int fpsval)
{

    switch(fpsval) {
        case 90  : { SetFPS(F90); break; }
        case 60  : { SetFPS(F60); break; }
        case 48  : { SetFPS(F48); break; }
        case 30  : { SetFPS(F30); break; }
        case 24  : { SetFPS(F24); break; }
        case 20  : { SetFPS(F20); break; }
        case 15  : { SetFPS(F15); break; }
        case 12  : { SetFPS(F12); break; }
        case 10  : { SetFPS(F10); break; }
        default  : { SetFPS(F30); break; }
    }


}
//--------------------------------------------------------------------
void MainEncoder::SetFPS(FPS index)
{
    //for(int i = 0; i < 7; i++)

    currentSpeed = fpsVal[index];

    if (timer->isActive())
    {
        timer->changeInterval((int)(1000.0/currentSpeed));
    }
}

//--------------------------------------------------------------------

void MainEncoder::SetStep(int index)
{

    switch(index)
    {
        case 1:	frameStep = 1;
            break;
        case 2:	frameStep = 2;
            break;
        case 3:	frameStep = 3;
            break;
        case 4:	frameStep = 4;
            break;
        case 5:	frameStep = 5;
            break;
        case 6:	frameStep = 6;
            break;
    }
}

//--------------------------------------------------------------------

void MainEncoder::SetZoom(int index)
{
    //map the zoom value
    if (index==0) 
        index=1;

    //double thezoom = (index/100.0);    

    //update the necessary player object now
    //encplayer->SetDisplayZoom(thezoom);

    //now update the ui based on the slider position
    SetFrameNumber(controllerslider->value());

}

void MainEncoder::ResizeDisplay(mediaSize thesize)
{
    player->setMinimumSize( QSize( thesize.width, thesize.height ) );
    player->setMaximumSize( QSize( thesize.width, thesize.height ) );
}

void MainEncoder::slotGenerateRenderFormatList()
{
    // We have to do this because the format selection dialogue will only come up once
    // after the list is populated
    if (m_jahnle_render != NULL)
    {
        delete m_jahnle_render;
    }

    if (m_mlt_properties != NULL)
    {
        delete m_mlt_properties;
    }

    if (m_render_formats_stringlist != NULL)
    {
        delete m_render_formats_stringlist;
    }

    m_mlt_properties = new Mlt::Properties;
    m_jahnle_render = new jahnle::Render;
    m_render_formats_stringlist = new QStringList;
    m_render_profiles_stringlist = new QStringList;

    QString path = JahBasePath + "plugins/encoders/";
    getJahNleRender()->createFormatsList( *getMltProperties(), *getRenderFormatsStringList(), path );
    getMltProperties()->set( "format", getRenderFormatsStringList()->first().data() );


    QStringList::Iterator it;

    for ( it = getRenderFormatsStringList()->begin(); it != getRenderFormatsStringList()->end(); ++it ) 
    {
        std::string item = (*it).data();
        m_render_formats_combobox->insertItem(*it);
    }

    slotGenerateRenderProfileList();
}

void MainEncoder::slotGenerateRenderProfileList()
{
    getRenderProfilesStringList()->clear();
    getJahNleRender()->createProfilesList( *getMltProperties(), *getRenderProfilesStringList() );
    getMltProperties()->set( "profile", getRenderProfilesStringList()->first().data() );

    QStringList::Iterator it;

    m_render_profiles_combobox->clear();

    for ( it = getRenderProfilesStringList()->begin(); it != getRenderProfilesStringList()->end(); ++it ) 
    {
        std::string item = (*it).data();
        m_render_profiles_combobox->insertItem(*it);
    }
}

void MainEncoder::slotSelectRenderFormat(int offset)
{
    std::string format_string = ( *getRenderFormatsStringList()->at(offset) ).data();
    getMltProperties()->set( "format", format_string.data() );
    std::string render_format = getMltProperties()->get("format");
    slotGenerateRenderProfileList();
}
    
void MainEncoder::slotSelectRenderProfile(int offset)
{
    std::string format_string = ( *getRenderProfilesStringList()->at(offset) ).data();
    getMltProperties()->set( "profile", format_string.data() );
    std::string render_profile = getMltProperties()->get("profile");
}

void MainEncoder::slotStartRender()
{
    if ( getJahNleRender() == NULL)
    {
        return;
    }

	Stop( );

	QString filename = theassetData.location+theassetData.filename+theassetData.extension;
	bool sequence = false;

	if ( theassetData.theType == VideoType::SEQUENCE )
	{
		if ( theassetData.startframe >= 1 )
		{
			filename = theassetData.location + ".all" + theassetData.extension;
			sequence = true;
		}
	}

	Mlt::Producer producer( ( char * )filename.latin1( ) );

	if ( sequence )
	{
		producer.set( "ttl", 1 );
		producer.set( "length", theassetData.endframe - theassetData.startframe + 1 );
		producer.set( "out", theassetData.endframe - theassetData.startframe );
	}

	Mlt::Filter filter( "avcolour_space" );
	filter.set( "forced", mlt_image_yuv422 );
	producer.attach( filter );

	Mlt::Frame *frame = producer.get_frame( );

	jahnle::Render render;

	Mlt::Properties *list = ( Mlt::Properties * )getMltProperties()->get_data( "profile_list" );
	list->set( "render_off", 1 );

    if ( render.executeProfile( *getMltProperties(), frame->get_int( "width" ), frame->get_double( "aspect_ratio" ) ) )
	{
		// Create the progress dialog
		QProgressDialog progress( QString( "Rendering " ) + render.getFileName( ), "Abort", producer.get_length( ) );
		progress.show( );

		Mlt::Consumer *consumer = render.getConsumer( );
		int sw = consumer->get_int( "width" );
		int sh = consumer->get_int( "height" );

		// Iterate through each frame and push it to the consumer
		for ( int i = 0; i < producer.get_length( ); i ++ )
		{
			// Set the progress and process any pending events
    		progress.setProgress( i );
    		qApp->processEvents( );
    		if ( progress.wasCanceled() )
        		break;
	
			// Fetch, push and delete
			Mlt::Frame *f = producer.get_frame( );

			int w = f->get_int( "width" );
			int h = f->get_int( "height" );

			// Force deinterlacing
			f->set( "consumer_deinterlace", 1 );

			if ( consumer->get_int( "render_off" ) == 0 && ( w != sw || h != sh ) )
			{
				mlt_image_format format = mlt_image_rgb24a;
				uint8_t *image = f->get_image( format, w, h );

				QImage temp( image, w, h, 32, NULL, 0, QImage::IgnoreEndian );
				temp = temp.smoothScale( sw, sh );

				uint8_t *blah = ( uint8_t * )malloc( sw * sh * 4 );
				memcpy( blah, temp.bits( ), sw * sh * 4 );

				f->set( "image", blah, sw * sh * 4, free );
				f->set( "width", sw );
				f->set( "height", sh );
				f->set( "format", mlt_image_rgb24a );
				f->set( "aspect_ratio", consumer->get_double( "consumer_aspect_ratio" ) );
				f->set( "alpha", NULL, 0 );
			}

			render.push( f );
			delete f;
		}
	}

	delete frame;
	getMltProperties()->set( "file_name", ( char * )NULL );
}

void MainEncoder::draggedAImage()
{
    emit encoderDraggedAImage();
}

void MainEncoder::showImage( int position, QImage * )
{
	animframe->blockSignals( true );
	controllerslider->blockSignals( true );
	SetFrameNumber( position );
	controllerslider->blockSignals( false );
	animframe->blockSignals( false );
}

void MainEncoder::sliderPressed( )
{
	encplayer->set_speed( 0 );
}

void MainEncoder::sliderReleased( )
{
	encplayer->refresh( );
	encplayer->restore_speed( );
}

void MainEncoder::buildController( QHBox* controller) 
{
    QVBox* top = new QVBox( controller );

    // Container widget
    QWidget *transportContainer = new QWidget( top, "page" );
    QHBoxLayout *transportLayout = new QHBoxLayout( transportContainer );
    
    transportLayout->addStretch();
    
    // Transport buttons
    scrubfirst = new JahToolButton( transportContainer, "controllerrewindbutton" );
    scrubfirst->setFixedSize( 25, 29 );
    JahFormatter::addJahPlayerButton( scrubfirst, JahBasePath+"Pixmaps/player/rewind_off.png", JahBasePath+"Pixmaps/player/rewind_on.png" );
    connect( scrubfirst,  SIGNAL(clicked()), SLOT(Rewind())  );
    connect( scrubfirst,  SIGNAL(released()), this, SLOT(sliderReleased()) );
    transportLayout->addWidget( scrubfirst );
    
    scrubprevious = new JahToolButton( transportContainer, "controllerpreviousbutton" );
    scrubprevious->setFixedSize( 25, 29 );
    JahFormatter::addJahPlayerButton( scrubprevious, JahBasePath+"Pixmaps/player/frewind_off.png", JahBasePath+"Pixmaps/player/frewind_on.png" );
    connect( scrubprevious,  SIGNAL(clicked()), SLOT(FrameBackward())  );
    connect( scrubprevious,  SIGNAL(released()), this, SLOT(sliderReleased()));
    transportLayout->addWidget( scrubprevious );
    
    scrubstop = new JahToolButton( transportContainer, "controllerstopbutton" );
    scrubstop->setFixedSize( 25, 29 );
    JahFormatter::addJahPlayerButton( scrubstop, JahBasePath+"Pixmaps/player/stop_off.png", JahBasePath+"Pixmaps/player/stop_on.png" );
    connect( scrubstop,  SIGNAL(clicked()), SLOT(Stop())  );
    transportLayout->addWidget( scrubstop );
    
    scrubplay = new JahToolButton( transportContainer, "play" );
    scrubplay->setFixedSize( 25, 29 );
    JahFormatter::addJahPlayerButton( scrubplay, JahBasePath+"Pixmaps/player/play_off.png", JahBasePath+"Pixmaps/player/play_on.png" );
    connect( scrubplay,  SIGNAL(clicked()), SLOT(LoopForward())  );
    transportLayout->addWidget( scrubplay );
    
    scrubnext = new JahToolButton( transportContainer, "controllernextbutton" );
    scrubnext->setFixedSize( 25, 29 );
    JahFormatter::addJahPlayerButton( scrubnext, JahBasePath+"Pixmaps/player/ffoward_off.png", JahBasePath+"Pixmaps/player/ffoward_on.png" );
    connect( scrubnext,  SIGNAL(clicked()), SLOT(FrameForward())  );
    connect( scrubnext,  SIGNAL(released()), this, SLOT(sliderReleased())  );
    transportLayout->addWidget( scrubnext );
    
    scrublast = new JahToolButton( transportContainer, "controllerffworardbutton" );
    scrublast->setFixedSize( 25, 29 );
    JahFormatter::addJahPlayerButton( scrublast, JahBasePath+"Pixmaps/player/foward_off.png",  JahBasePath+"Pixmaps/player/foward_on.png" );
    connect( scrublast,  SIGNAL(clicked()), SLOT(FastForward())  );
    connect( scrublast,  SIGNAL(released()), this, SLOT(sliderReleased())  );
    transportLayout->addWidget( scrublast );

    transportLayout->addSpacing( 10 );

    // Time code display
    animframe = new SupergRangeControl( transportContainer, "timecodedisplay" );
    animframe->setFormatter( TimecodeValueFormatter() );
    JahFormatter::configure( animframe, 0, 0 );
    connect( animframe, SIGNAL(valueChanged(int)), this, SLOT( sliderValueChanged( int ) ) );
    transportLayout->addWidget( animframe );
    transportLayout->addStretch();

    // Container widget
    QWidget* sliderContainer = new QWidget( top );
    QHBoxLayout* sliderLayout = new QHBoxLayout( sliderContainer );

    QPalette p = palette();
    JahFormatter::swap( p, QColorGroup::Shadow, QColorGroup::Background );
    
    SupergRangeControl* controllerStartFrameControl = new SupergRangeControl( sliderContainer );
    controllerStartFrameControl->setFormatter( TimecodeValueFormatter() );
    controllerStartFrameControl->setPalette( p );
    controllerStartFrameControl->setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );
    JahFormatter::configure( controllerStartFrameControl, Astartframe, Astartframe );
    sliderLayout->addWidget( controllerStartFrameControl );

    // Slider
    controllerslider = new TimelineSlider( sliderContainer, "controllerslider" );
    controllerslider->setMinValue( Astartframe );
    controllerslider->setMaxValue( Aendframe );
    controllerslider->setValue   ( Aanimframe );
    sliderLayout->addWidget( controllerslider );

    SupergRangeControl* controllerEndFrameControl = new SupergRangeControl( sliderContainer );
    controllerEndFrameControl->setFormatter( TimecodeValueFormatter() );
    controllerEndFrameControl->setPalette( p );
    controllerEndFrameControl->setSizePolicy( QSizePolicy( QSizePolicy::Preferred, QSizePolicy::Preferred ) );
    JahFormatter::configure( controllerEndFrameControl, Aendframe, Astartframe + 1 );
    sliderLayout->addWidget( controllerEndFrameControl );

    connect( controllerslider, SIGNAL( minChanged( int ) ), controllerStartFrameControl, SLOT( setValue( int ) ) );
    connect( controllerslider, SIGNAL( maxChanged( int ) ), controllerEndFrameControl, SLOT( setValue( int ) ) );
    connect( controllerStartFrameControl, SIGNAL( valueChanged( int ) ), this, SLOT( SetMinFrame( int ) ) );
    connect( controllerEndFrameControl, SIGNAL( valueChanged( int ) ), this, SLOT( SetMaxFrame( int ) ) );

    connect(controllerslider, SIGNAL(valueChanged(int)), this, SLOT(sliderValueChanged(int)) );
    connect(controllerslider, SIGNAL(sliderPressed()), this, SLOT(sliderPressed()) );
    connect(controllerslider, SIGNAL(sliderReleased()), this, SLOT(sliderReleased()) );
}

void MainEncoder::buildInterface( QHBox* qtinterface )
{
    //////////////////////////////////////////////////////////////
    // set up the scene options, or prefernces

    EffectsFrame = new QFrame(qtinterface,"interface");
    EffectsFrame->setGeometry( QRect( 0, 0, 855, 195) );

    /////////////////////////////////////////////////////////////////////
    //set up the optionbar

    m_render_normalisation_label = new QLabel(EffectsFrame, "RenderNormalisationLabel");
    m_render_normalisation_label->setGeometry( QRect( 10, 15, 190, 25 ) );
    m_render_normalisation_label->setAlignment( int( QLabel::AlignRight ) );
    m_render_normalisation_label->setText( getJahTranslate()->tr("Normalisation") );

	m_render_normalisation_combobox = new QComboBox( false, EffectsFrame, "RenderNormalisationCombobox" );
	m_render_normalisation_combobox->setGeometry( QRect( 210, 15, 200, 25 ) );
	m_render_normalisation_combobox->insertItem( "PAL" );
	m_render_normalisation_combobox->insertItem( "NTSC" );
	
    m_render_formats_label = new QLabel(EffectsFrame, "RenderFormatsLabel");
    m_render_formats_label->setGeometry( QRect( 10, 45, 190, 25 ) );
    m_render_formats_label->setAlignment( int( QLabel::AlignRight ) );
    m_render_formats_label->setText( getJahTranslate()->tr("Encoding Format") );

    m_render_formats_combobox = new QComboBox( false, EffectsFrame, "RenderFormatsCombobox" );
    m_render_formats_combobox->setGeometry( QRect( 210, 45, 200, 25 ) );
    connect( m_render_formats_combobox,  SIGNAL( highlighted(int) ), SLOT( slotSelectRenderFormat(int) ) );

    m_render_profiles_label = new QLabel(EffectsFrame, "RenderFormatsLabel");
    m_render_profiles_label->setGeometry( QRect( 10, 75, 190, 25 ) );
    m_render_profiles_label->setAlignment( int( QLabel::AlignRight ) );
    m_render_profiles_label->setText( getJahTranslate()->tr("Profile") );

    m_render_profiles_combobox = new QComboBox( false, EffectsFrame, "RenderProfilesCombobox" );
    m_render_profiles_combobox->setGeometry( QRect( 210, 75, 200, 25 ) );
    connect( m_render_profiles_combobox,  SIGNAL( highlighted(int) ), SLOT( slotSelectRenderProfile(int) ) );


    m_jahnle_render = NULL;
    m_mlt_properties = NULL;
    m_render_formats_stringlist = NULL;
    slotGenerateRenderFormatList();

    m_render_button = new QPushButton(EffectsFrame, "RenderButton");
    JahFormatter::addButton( m_render_button,  210, 110, 100, 25,  getJahTranslate()->tr("Start Encoding") );
    connect  ( m_render_button,  SIGNAL(clicked()), this, SLOT( slotStartRender() )  );

}

void MainEncoder::createMenuItem( QPopupMenu * themenu )
{
	Q_CHECK_PTR(themenu);
}

