
TEMPLATE    =           app

win32:system( embed.bat )
unix:system( ./embed.sh )

CONFIG	+= 		qt warn_on release rtti console exceptions

HEADERS    =           	dirview.h \
                        jahfileiconview.h \
				iconprovider.h \
				jahprogressbar.h \
				assetstringfunctions.h \
				embedded-images.h

SOURCES     =           main.cpp \
			      dirview.cpp \
                        jahfileiconview.cpp \
				iconprovider.cpp \
				jahprogressbar.cpp

FORMS	= 			assetmanmainwidget.ui \
				assetmanitem.ui \
				loginform1.ui \ 
				advancedsearchform.ui

TARGET      =           assetmanui

INCLUDEPATH +=		../openassetlib/src
DEPENDPATH 	+= 		../openassetlib/src
LIBS 		+= 		-lv2_openassetlib -L../openassetlib/distributables/
unix: LIBS		+=		../openassetlib/common/lib/sqlite3.lib



