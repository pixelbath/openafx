###############################################################
#
# VisualMedia OpenLibraries QMake build files
#
###############################################################

include( ../../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Building JahDesktop" )
message( "-------------------" )

SUBDIRS += desktop
SUBDIRS += encoder
SUBDIRS += library
SUBDIRS += network
SUBDIRS += player
#SUBDIRS += videoio

contains( JAHPLAYER,true ) {	
SUBDIRS -= encoder
SUBDIRS -= network
}

TEMPLATE = subdirs

