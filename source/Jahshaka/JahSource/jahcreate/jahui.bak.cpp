/*******************************************************************************
**
** The source file for the Jahshaka Jahroutines module
** The Jahshaka Project
** Copyright (C) 2000-2004 The Jahshaka Project
** Released under the GNU General Public License
**
*******************************************************************************/

#include "jahcreate.h"


void JahControl::createJahUI(	QFrame *mainworld,		  QFrame *mainleftmenu,		QFrame *mainrightmenu,
								QFrame *maincontroller,	  QFrame *modulesettings,
								QFrame *D2mainworld,	  QFrame *D2mainleftmenu,	QFrame *D2mainrightmenu,
								QFrame  *D2maincontroller,QFrame *D2modulesettings )
{
    //////////////////////////////////////////////////////////
    //create the main controler for the world space object

    wideframeLayout = new QGridLayout( mainworld, 1, 1, 11, 6, "frame3Layout");

    //the splitter
    splitter2 = new QSplitter( mainworld, "splitter2" );
    splitter2->setOrientation( QSplitter::Horizontal );

    wideframeLeft = new QHBox( splitter2, "frame4" );
    wideframeLeft->setFrameShape( QFrame::StyledPanel );
    wideframeLeft->setFrameShadow( QFrame::Raised );

    wideframeCenter = new QHBox( splitter2, "frame5" );
    wideframeCenter->setFrameShape( QFrame::StyledPanel );
    wideframeCenter->setFrameShadow( QFrame::Raised );

    wideframeRight = new QHBox( splitter2, "frame5" );
    wideframeRight->setFrameShape( QFrame::StyledPanel );
    wideframeRight->setFrameShadow( QFrame::Raised );

    wideframeLayout->addWidget( splitter2, 0, 0 );


	///////////////////////////////////////////////////////////////
	// the new layout here, currently back to front ie mainworldLayout1 and 2
	// should be switched
	// if not in widescreen mode then we can just hide all the splitters


    //this will be for the desktop alone for widescrreen mode
	//it is the left screen acces module
    mainworldLayout2 = new QHBoxLayout( wideframeLeft, 0, 0, "mainworldLayoutLeft");
    WorldLayout2 = new QWidgetStack(wideframeLeft);
    mainworldLayout2->addWidget( WorldLayout2 );

	//this is for the main worldspace
    mainworldLayout = new QHBoxLayout( wideframeCenter, 0, 0, "mainworldLayoutCenter");
    WorldLayout = new QWidgetStack(wideframeCenter);
    mainworldLayout->addWidget( WorldLayout );

	//it is the right screen acces module
    mainworldLayout1 = new QHBoxLayout( wideframeRight, 0, 0, "mainworldLayoutRight");
    WorldLayout1 = new QWidgetStack(wideframeRight);
    mainworldLayout1->addWidget( WorldLayout1 );
	
	
	//////////////////////////////////////////////////////////////////////////
	//also we need to remember size of left/right ui's for when the application restarts
	QValueList<int> sizeList;

	if (!widescreen)
	{
		sizeList.append(0);
		sizeList.append(600);
		sizeList.append(0);
		splitter2->setSizes(sizeList);
	}
	else
	{
		sizeList.append(100);
		sizeList.append(600);
		sizeList.append(0);
		splitter2->setSizes(sizeList);
	}

	/////////////////////////////////////////////////////////////
	// this builds the rest of the ui

    //////////////////////////////////////////////////////////////
    //create the main left menu controler
    mainworldLeftLayout = new QHBoxLayout( mainleftmenu, 0, 0, "mainworldLeftLayout");
    MainLeftControler = new QWidgetStack(mainleftmenu);
    mainworldLeftLayout->addWidget( MainLeftControler );

    //////////////////////////////////////////////////////////////
    //create the main right menu controler
    mainworldRightLayout = new QHBoxLayout( mainrightmenu, 0, 0, "mainworldRightLayout");
    MainRightControler = new QWidgetStack(mainrightmenu);
    mainworldRightLayout->addWidget( MainRightControler );

    //////////////////////////////////////////////////////////////
    //create the middle frame menu/option controler
    maincontrollerLayout = new QHBoxLayout( maincontroller, 0, 0, "maincontrollerLayout");
    MainControler = new QWidgetStack(maincontroller);
    maincontrollerLayout->addWidget( MainControler );

    //////////////////////////////////////////////////////////////
    //Create Interface frame and switching controllers for modules
    optionals = new QWidgetStack(modulesettings);
    optionals->setGeometry( QRect( 5, 1, 885, 200) );

    ////////////////////////////////////////////////////////////////
    //for dualhead
    if (dualheadprefs)
    {
        jtrace->info( "Setting up dual head" );

        //////////////////////////////////////////////////////////////
        //create the main controler for the world space object
        D2mainworldLayout = new QHBoxLayout( D2mainworld, 0, 0, "D2mainworldLayout");
        D2WorldLayout = new QWidgetStack(D2mainworld);
        D2mainworldLayout->addWidget( D2WorldLayout );

        //////////////////////////////////////////////////////////////
        //create the main left menu controler
        D2mainworldLeftLayout = new QHBoxLayout( D2mainleftmenu, 0, 0, "D2mainworldLeftLayout");
        D2MainLeftControler = new QWidgetStack(D2mainleftmenu);
        D2mainworldLeftLayout->addWidget( D2MainLeftControler );

        //////////////////////////////////////////////////////////////
        //create the main right menu controler
        D2mainworldRightLayout = new QHBoxLayout( D2mainrightmenu, 0, 0, "D2mainworldRightLayout");
        D2MainRightControler = new QWidgetStack(D2mainrightmenu);
        D2mainworldRightLayout->addWidget( D2MainRightControler );

        //////////////////////////////////////////////////////////////
        //create the middle frame menu/option controler
        D2maincontrollerLayout = new QHBoxLayout( D2maincontroller, 0, 0, "D2maincontrollerLayout");
        D2MainControler = new QWidgetStack(D2maincontroller);
        D2maincontrollerLayout->addWidget( D2MainControler );

        //////////////////////////////////////////////////////////////
        //Create Interface frame and switching controllers for modules
        D2optionals = new QWidgetStack(D2modulesettings);
        D2optionals->setGeometry( QRect( 5, 1, 860, 200) );
    }


}

void JahControl::createJahDesktopUI(	QFrame *moduleselection, QFrame *D2moduleselection)
{
    if (!dualheadprefs)
    {
        Desktop = new QPushButton( moduleselection, "Desktop" );
        Desktop->setGeometry( QRect( 10, 10, 111, 30 ) );
        Desktop->setText( jt->tr( "Desktop"  ) );
        connect( Desktop, SIGNAL(clicked()), SLOT(changeModeDesktop()) );

        if (!widescreen)
        {
            desktopdisplay = new QHBox (WorldLayout, "desktop");
            WorldLayout->addWidget(desktopdisplay);

            CreateDesktopModule(desktopdisplay);
        }
        
		if (widescreen)
        {
            //using widescreen mode
            desktopdisplay  = new QHBox (WorldLayout, "widedesktop");
            desktopdisplay2 = new QHBox (WorldLayout2, "widedesktop");

            WorldLayout2->addWidget(desktopdisplay2);
            WorldLayout->addWidget(desktopdisplay);

            CreateDesktopModule(desktopdisplay,desktopdisplay2);
        }

    }

    if (dualheadprefs)
    {
        DualDesktop = new QPushButton( D2moduleselection, "Desktop" );
        DualDesktop->setGeometry( QRect( 10, 10, 111, 30 ) );
        DualDesktop->setText( jt->tr( "Desktop"  ) );
        connect( DualDesktop, SIGNAL(clicked()), SLOT(changeModeDesktop()) );

        desktopdisplay = new QHBox (D2WorldLayout, "d2desktop");
        D2WorldLayout->addWidget(desktopdisplay);
        CreateDesktopModule(desktopdisplay);
    }


    //used to add encoding module to dualhead
    if (dualheadprefs)
    {
        DualEncoding = new QPushButton( D2moduleselection, "DualEncoding" );
        DualEncoding->setGeometry( QRect( 10, 90, 111, 30 ) );
        DualEncoding->setText( jt->tr( "Encode"  ) );
        connect( DualEncoding,  SIGNAL(clicked()), SLOT(changeModeEncoding())  );
    }

    if (dualheadprefs)
    {
        DualNetwork = new QPushButton( D2moduleselection, "Network" );
        DualNetwork->setGeometry( QRect( 10, 50, 111, 30 ) );
        DualNetwork->setText( jt->tr( "Network"  ) );
        connect( DualNetwork,  SIGNAL(clicked()), SLOT(changeModeNetwork())  );
    }


}

void JahControl::createJahDesktopButtons(QFrame *moduleselection)
{

    AnimationModule = new QPushButton( moduleselection, "Animation" );
    AnimationModule->setGeometry( QRect( 10, 50, 111, 30 ) );
    AnimationModule->setText( jt->tr("Animation") );
    connect( AnimationModule, SIGNAL(clicked()), SLOT(changeModeAnimation()) );  //was Effects

    EffectsModule = new QPushButton( moduleselection, "Effects" );
    EffectsModule->setGeometry( QRect( 10, 83, 111, 30 ) );
    EffectsModule->setText( jt->tr( "Effects"  ) );
    connect( EffectsModule,  SIGNAL(clicked()), SLOT(changeModeEffects())  );

    EditingModule = new QPushButton( moduleselection, "Editing" );
    EditingModule->setGeometry( QRect( 10, 116, 111, 30 ) );
    EditingModule->setText( jt->tr( "Editing"  ) );
    connect( EditingModule,  SIGNAL(clicked()), SLOT(changeModeEditing())  );

    PaintModule = new QPushButton( moduleselection, "paint" );
    PaintModule->setGeometry( QRect( 10, 149, 111, 30 ) );
    PaintModule->setText( jt->tr( "Paint"  ) );
    connect( PaintModule,  SIGNAL(clicked()), SLOT(changeModePaint())  );

    TextModule = new QPushButton( moduleselection, "text" );
    TextModule->setGeometry( QRect( 10, 182, 111, 30 ) );
    TextModule->setText( jt->tr( "Text CG"  ) );
    connect( TextModule,  SIGNAL(clicked()), SLOT(changeModeText())  );
}
