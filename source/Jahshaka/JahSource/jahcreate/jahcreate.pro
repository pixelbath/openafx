###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 

contains( JAHPLAYER,true ) {	
HEADERS     =           jahplayercreate/jahplayercreate.h 

SOURCES     =           jahplayercreate/jahplayercreate.cpp \
			jahplayercreate/jahplayerdesktopinterface.cpp \
			jahplayercreate/jahplayerdesktoproutines.cpp \
			jahplayercreate/jahplayerdesktoptraffic.cpp \
			jahplayercreate/jahplayerevents.cpp \
			jahplayercreate/jahplayerinitialize.cpp \
			jahplayercreate/jahplayermenubar.cpp \
			jahplayercreate/jahplayermodes.cpp \
			jahplayercreate/jahplayermodules.cpp \
			jahplayercreate/jahplayerprefs.cpp \
			jahplayercreate/jahplayerroutines.cpp \
			jahplayercreate/jahplayerstyle.cpp \
			jahplayercreate/jahplayerui.cpp

} else {


HEADERS     =           jahcreate.h 

SOURCES     =           jahcreate.cpp \
			jahdesktopinterface.cpp \
			jahdesktoproutines.cpp \
			jahdesktoptraffic.cpp \
			jahevents.cpp \
			jahinitialize.cpp \
			jahmenubar.cpp \
			jahmodes.cpp \
			jahmodules.cpp \
			jahprefs.cpp \
			jahroutines.cpp \
			jahstyle.cpp \
			jahui.cpp

}

TARGET      =           jahcreate

DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           .  

contains( JAHPLAYER,true ) {	
INCLUDEPATH     += 	        jahplayercreate \
                                ../jahmain/jahplayermain
}
#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile 
}

INCLUDEPATH +=          ../../../AuxiliaryLibraries/apollon \
                        ../../../AuxiliaryLibraries/blur \
                        ../../../AuxiliaryLibraries/FTGL \
                        ../../../AuxiliaryLibraries/gift \
                        ../../../AuxiliaryLibraries/glew \ 
                        ../../../AuxiliaryLibraries/particle \ 
                        ../../../AuxiliaryLibraries/spaceball \ 
                        ../../../AuxiliaryLibraries/sqlite  
                        
INCLUDEPATH +=			../../../OpenLibraries/opencore \
						../../../OpenLibraries/openassetlib \
						../../../OpenLibraries/openassetlib/v2_openassetlib/src \
						../../../OpenLibraries/openobjectlib \
						../../../OpenLibraries/openobjectlib/surface3d \
						../../../OpenLibraries/openmedialib \
						../../../OpenLibraries/openmedialib/mediaobject \ 
						../../../OpenLibraries/opengpulib \
						../../../OpenLibraries/openimagelib \
						../../../OpenLibraries/opennetworklib 
						
win32 {
INCLUDEPATH += 			../../../OpenLibraries/openmedialib/codecs/aviutils 
}

INCLUDEPATH +=          ../../JahCore/jahobjects \
						../../JahCore/jahrender \
						../../JahCore/jahworld 

INCLUDEPATH +=			../../JahSource/jahcreate \
						../../JahSource/jahmain \
						../../JahSource/jahsplash

INCLUDEPATH +=			../../JahModules/animation \
						../../JahModules/colorize \
						../../JahModules/effect \
						../../JahModules/keyer \
						../../JahModules/painter \
						../../JahModules/text \
						../../JahModules/tracker 

# Special case - uses another editing module when MLT available
contains( MLTSUPPORT,true ) {
	INCLUDEPATH += ../../JahModules/editing_mlt
}
else {
	INCLUDEPATH += ../../JahModules/editing
}

INCLUDEPATH +=			../../JahDesktop/desktop \
						../../JahDesktop/encoder \
						../../JahDesktop/library \
						../../JahDesktop/network \
						../../JahDesktop/player \
						../../JahDesktop/videoio 

INCLUDEPATH +=			../../JahWidgets/colortri \
						../../JahWidgets/interfaceobjs \
						../../JahWidgets/calc \
						../../JahWidgets/jahfileloader \
						../../JahWidgets/keyframes \
						../../JahWidgets/nodes \
						../../JahWidgets/wireup \
						../../JahWidgets/timeline \
						../../JahWidgets/waveform \
						../../JahWidgets/mediatable 

INCLUDEPATH +=			../../JahLibraries \
						../../JahLibraries/jahdatabase \
						../../JahLibraries/jahdataio \
						../../JahLibraries/jahformatter \
						../../JahLibraries/jahglcore \ 
						../../JahLibraries/jahkeyframes \
						../../JahLibraries/jahplayer \
						../../JahLibraries/jahplayer/audioplayer \
						../../JahLibraries/jahplayer/diskplayer \
						../../JahLibraries/jahplayer/ramplayer \
						../../JahLibraries/jahplugins \
						../../JahLibraries/jahpreferences \
						../../JahLibraries/jahthemes \
						../../JahLibraries/jahtimer \
						../../JahLibraries/jahtracer \
						../../JahLibraries/jahtranslate \
						$$FREEDIR

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT, true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}

#patch for glx qt namespace conflicts		
QMAKE_CXXFLAGS+="-DQT_CLEAN_NAMESPACE"



