###############################################################
#
# VisualMedia OpenLibraries QMake build files
#
###############################################################

include( ../../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Configuring JahSource" )
message( "---------------------" )

SUBDIRS += jahmain
SUBDIRS += jahsplash
SUBDIRS += jahcreate

contains( JAHPLAYER,true ) {	
SUBDIRS -= jahsplash
}

TEMPLATE = subdirs

