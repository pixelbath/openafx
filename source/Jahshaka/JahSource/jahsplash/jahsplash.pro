###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 

HEADERS     =           jahstartup.h \
                        jahstartupgl.h 

SOURCES     =           jahstartup.cpp \
                        jahstartupgl.cpp 

TARGET      =           jahsplash

DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes
#INCLUDEPATH +=           ../../../AuxiliaryLibraries/glew  

#files for audiosupport
#contains( JAHAUDIO,true ) {
#INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile 
#}

#INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
#                        ../../../AuxiliaryLibraries/FTGL \
#                        ../../../AuxiliaryLibraries/particle 
                        
#INCLUDEPATH +=			../../../OpenLibraries/opencore \
#						../../../OpenLibraries/openassetlib \
#						../../../OpenLibraries/openobjectlib \
#						../../../OpenLibraries/openmedialib \
#						../../../OpenLibraries/openmedialib/mediaobject 

#INCLUDEPATH +=          ../../JahCore/jahobjects \
#						../../JahCore/jahrender \
#						../../JahCore/jahworld

INCLUDEPATH +=			../../JahLibraries/jahpreferences \
						../../JahWidgets/interfaceobjs
#						../../JahLibraries/jahtracer \
#						../../JahLibraries/jahpreferences \
#						$$FREEDIR

INCLUDEPATH += $$OPENLIBRARIES_INCLUDE ../../../OpenLibraries/opengpulib

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



