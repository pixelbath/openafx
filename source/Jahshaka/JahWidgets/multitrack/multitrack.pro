###############################################################
#
# Multitrack widget
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

TEMPLATE                =   lib
CONFIG      		+=  staticlib 
HEADERS                 =   multitrack.h
SOURCES                 =   multitrack.cpp
TARGET                  =   multitrack
DEPENDPATH              =   $$JAHDEPENDPATH
INCLUDEPATH             += ../../JahLibraries/jahformatter \
                           ../../JahLibraries/jahpreferences

