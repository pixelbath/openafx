@echo on

set FILES=default-effect-thumb.png 

set FILES=%FILES% overview-bg.png grid-for-canvas.png green-glow.png 
echo Embedding: %FILES%

qembed --images %FILES% > wireup-embedded-images.h
