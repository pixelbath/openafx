###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE =              lib
CONFIG		+=          staticlib 
                        
HEADERS		=           dirview.h \
                        jahfilewindow.h \
                        jahfileframe.h \
                        jahfileiconview.h \
                        iconprovider.h \
			embedded-images.h
SOURCES		=           dirview.cpp \
                        jahfilewindow.cpp \
                        jahfileframe.cpp \
                        jahfileiconview.cpp \
			iconprovider.cpp
TARGET = jahfileloader
DEPENDPATH =            $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           .  

#files for audiosupport
#contains( JAHAUDIO,true ) {
#INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile 
#}

#INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
#                        ../../../AuxiliaryLibraries/FTGL \
#                        ../../../AuxiliaryLibraries/particle 
                        
INCLUDEPATH +=			../../../OpenLibraries/opencore \
						../../../OpenLibraries/openassetlib \
						../../../OpenLibraries/openassetlib/v2_openassetlib/src \
#						../../../OpenLibraries/openobjectlib \
#						../../../OpenLibraries/openmedialib \
#						../../../OpenLibraries/openmedialib/mediaobject

INCLUDEPATH +=		../../JahDesktop/library 

#INCLUDEPATH +=          ../../JahWidgets/jahcalc
#						../../JahCore/jahrender \
#						../../JahCore/jahworld

INCLUDEPATH +=			../../JahLibraries/jahpreferences
#						../../JahLibraries/jahtracer \
#						../../JahLibraries/jahpreferences \
#						$$FREEDIR

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



