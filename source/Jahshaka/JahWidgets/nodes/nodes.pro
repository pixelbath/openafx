###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE = lib
CONFIG      +=          staticlib 
HEADERS     =           jahnodes.h \
                        jahnodesitem.h \
                        jahnodeeditor.h
SOURCES     =           jahnodes.cpp \
                        jahnodesitem.cpp \
                        jahnodeeditor.cpp
TARGET      =           jahnodes
DEPENDPATH  =           $$JAHDEPENDPATH


###############################################################
#the project related includes
INCLUDEPATH =           .  

INCLUDEPATH +=			../../JahLibraries/jahpreferences \
						../../JahLibraries/jahformatter \
						../../JahWidgets/interfaceobjs

INCLUDEPATH +=			../../JahLibraries/jahtracer

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}
