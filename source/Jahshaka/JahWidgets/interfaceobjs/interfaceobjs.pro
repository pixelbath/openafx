###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
HEADERS     =           dialogs.h \
                        InputLCD.h \
                        widget.h \
                        jahprogressbar.h \
                        loadingdialog.h \
			supergrangecontrol.h \
                        collapsable.h \
                        timelineSlider.h \
                        stretchableButton.h

SOURCES     =           dialogs.cpp \
                        InputLCD.cpp \
                        jahprogressbar.cpp \
                        loadingdialog.cpp \
                        supergrangecontrol.cpp \
                        collapsable.cpp \
                        timelineSlider.cpp \
                        stretchableButton.cpp

TARGET      =           interfaceobjs
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           .  


INCLUDEPATH +=          ../../JahWidgets/jahcalc
INCLUDEPATH +=	        ../../JahLibraries/jahpreferences
INCLUDEPATH +=          ../../JahLibraries/jahformatter

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}




