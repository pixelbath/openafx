###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
                        

HEADERS     =           colordropper.h

SOURCES     =           colordropper.cpp

TARGET      =           colordropper
DEPENDPATH  =            $$JAHDEPENDPATH

###############################################################
###############################################################
#the project related includes
INCLUDEPATH =           .  

#files for audiosupport
#contains( JAHAUDIO,true ) {
#INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile
#}

#INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
#                        ../../../AuxiliaryLibraries/FTGL \
#                        ../../../AuxiliaryLibraries/particle 
                        
#INCLUDEPATH +=			../../../OpenLibraries/opencore \
#						../../../OpenLibraries/openassetlib \
#						../../../OpenLibraries/openobjectlib \
#						../../../OpenLibraries/openmedialib \
#						../../../OpenLibraries/openmedialib/mediaobject 

#INCLUDEPATH +=          ../../JahDesktop/desktop


INCLUDEPATH +=			../../JahLibraries/jahtracer \
						../../JahLibraries/jahpreferences \
						$$FREEDIR

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}




