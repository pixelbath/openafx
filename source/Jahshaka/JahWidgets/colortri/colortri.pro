###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
                        
# NOTE! The widgets here should really be under a FORMS section 
# so the code is generated runtime, but for now we put the generated
# files here.
HEADERS     =           keyercolorwidget.h \
			colortriangle.h \
			qtcolortriangle.h \
			qtcolordisplay.h \
            colorcorrectorwidget.h \
            gpumathlib.h
SOURCES     =           keyercolorwidget.cpp \
			colortriangle.cpp \
			qtcolortriangle.cpp \
			qtcolordisplay.cpp \
            colorcorrectorwidget.cpp
TARGET      =           keyercolorwidget
DEPENDPATH  =            $$JAHDEPENDPATH

###############################################################
###############################################################
#the project related includes
INCLUDEPATH =           .  

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile
}

#INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
#                        ../../../AuxiliaryLibraries/FTGL \
#                        ../../../AuxiliaryLibraries/particle 
                        
INCLUDEPATH +=			../../../OpenLibraries/gpumathlib

INCLUDEPATH +=			../../../OpenLibraries/opencore \
						../../../OpenLibraries/openassetlib \
						../../../OpenLibraries/openobjectlib \
						../../../OpenLibraries/openmedialib \
						../../../OpenLibraries/openmedialib/mediaobject 

INCLUDEPATH +=          ../../JahDesktop/desktop
INCLUDEPATH +=          ../../JahWidgets/colordropper

INCLUDEPATH +=			../../JahLibraries/jahtracer \
						../../JahLibraries/jahpreferences \
						$$FREEDIR

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}




