###############################################################
#
# VisualMedia OpenLibraries QMake build files
#
###############################################################

include( ../../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Configuring JahWidgets" )
message( "----------------------" )

SUBDIRS += colortri
SUBDIRS += interfaceobjs
SUBDIRS += jahcalc
SUBDIRS += jahfileloader
SUBDIRS += keyframes
SUBDIRS += nodes
SUBDIRS += multitrack
SUBDIRS += mediatable
SUBDIRS += colordropper
SUBDIRS += waveform

contains( JAHPLAYER,true ) {	
SUBDIRS -= colortri
SUBDIRS -= keyframes
SUBDIRS -= nodes
SUBDIRS -= wireup
SUBDIRS -= timeline
SUBDIRS -= multitrack
}

TEMPLATE = subdirs

