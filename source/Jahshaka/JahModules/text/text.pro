###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE = lib
CONFIG      +=      staticlib 
HEADERS     =       character.h 
SOURCES     =       character.cpp \
                    characterinterface.cpp \
                    characterselections.cpp  
TARGET      =       text
DEPENDPATH  =       $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           .  

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile 
}

INCLUDEPATH +=          ../../../AuxiliaryLibraries/apollon \
                        ../../../AuxiliaryLibraries/blur \
                        ../../../AuxiliaryLibraries/FTGL \
                        ../../../AuxiliaryLibraries/gift \
                        ../../../AuxiliaryLibraries/glew \ 
                        ../../../AuxiliaryLibraries/particle \ 
                        ../../../AuxiliaryLibraries/spaceball \ 
                        ../../../AuxiliaryLibraries/sqlite  
                        
INCLUDEPATH +=		../../../OpenLibraries/opencore \
			../../../OpenLibraries/openassetlib \
			../../../OpenLibraries/openobjectlib \
			../../../OpenLibraries/openobjectlib/surface3d \
			../../../OpenLibraries/openmedialib \
			../../../OpenLibraries/openmedialib/mediaobject \ 
			../../../OpenLibraries/opengpulib \
			../../../OpenLibraries/openimagelib \
			../../../OpenLibraries/opennetworklib 
						
INCLUDEPATH +=          ../../JahCore/jahobjects \
			../../JahCore/jahrender \
			../../JahCore/jahworld 

INCLUDEPATH +=		../../JahModules/text 

INCLUDEPATH +=		../../JahWidgets/colortri \
			../../JahWidgets/interfaceobjs \
			../../JahWidgets/calc \
			../../JahWidgets/jahfileloader \
			../../JahWidgets/keyframes \
			../../JahWidgets/nodes \
			../../JahWidgets/wireup \
			../../JahWidgets/timeline \
			../../JahWidgets/mediatable 

INCLUDEPATH +=		../../JahDesktop/desktop

INCLUDEPATH +=		../../JahModules/animation

INCLUDEPATH +=          ../../JahSource/jahcreate

INCLUDEPATH +=		../../JahLibraries \
			../../JahLibraries/jahdatabase \
			../../JahLibraries/jahdataio \
			../../JahLibraries/jahformatter \
			../../JahLibraries/jahglcore \ 
			../../JahLibraries/jahkeyframes \
			../../JahLibraries/jahplayer \
			../../JahLibraries/jahplayer/audioplayer \
			../../JahLibraries/jahplayer/diskplayer \
			../../JahLibraries/jahplayer/ramplayer \
			../../JahLibraries/jahplugins \
			../../JahLibraries/jahpreferences \
			../../JahLibraries/jahthemes \
			../../JahLibraries/jahtimer \
			../../JahLibraries/jahtracer \
			../../JahLibraries/jahtranslate \
			$$FREEDIR

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT, true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
#win32{
#    INCLUDEPATH += C:\freetype\include
#}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}

#patch for glx qt namespace conflicts		
QMAKE_CXXFLAGS+="-DQT_CLEAN_NAMESPACE"



