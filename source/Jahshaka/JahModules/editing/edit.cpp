/*******************************************************************************
**
** The source file for the Jahshaka editing module
** The Jahshaka Project
** Copyright (C) 2000-2005 The Jahshaka Project
** Released under the GNU General Public License
**
*******************************************************************************/

#include "edit.h"


void GLEdit::initializeObjects() 
{
    jtrace->info( "Initializing GLEdit Class");
    
    //make the current opengl context current
    makeCurrent();

	//initialize the timeline object
    theeditdesktop = new EditDesktop( this, moduleOptionsUI[4], "editdesk" );
    theeditdesktop->setFixedSize( QSize( 980, 600 ) );    //was 955,486

	configureLayer ( "World", false, false, false, true );
    buildLayer("Background", LayerCategory::LAYER, ObjectCategory::BACKDROP );

	//setup/customize listviewitem for layer 1
	initializeListview(0,false);

    //hook up menu and sliders and default tool
    setObjectTab(m_number_of_layers); 
	updateSliders();

    //turn forceplay on here
    setForcePlay(true);

}

//////////////////////////////////////////////////
// the new mediatable is initalized here
void GLEdit::initializeMediaTable( QHBox* parentbox)
{
    mediatable = new JahDesktopSideTable( 0, parentbox );
    //connect ( mediatable, SIGNAL(currentChanged ( int, int )), this, SLOT( selectSideTableRow(int,int)) );

    //connect ( editor, SIGNAL(itemRenamed( int, QString )), mediatable, SLOT( renameAsset(int, QString))); 
    //sidetableSelect = -1;
}


void GLEdit::updateUiSettings()
{
    ////////////////////////////////////////////////////////////////////////////////////
    // all other ui settings
    //for transparency

    //update video frames here
    FramesLcd->setValue   ( getActiveJahLayer()->asset.getNumFrames() );
    inFramesLcd->setValue ( getActiveJahLayer()->m_inframe );
    outFramesLcd->setValue( getActiveJahLayer()->m_outframe );

    KeyFramesLcd->setValue   ( getActiveJahLayer()->keyasset.getNumFrames() );
    inKeyFramesLcd->setValue ( getActiveJahLayer()->m_keyinframe );
    outKeyFramesLcd->setValue( getActiveJahLayer()->m_keyoutframe );

    extendHeadTail->setChecked ( getActiveJahLayer()->showEdges );
    toggleTheLoop->setChecked ( getActiveJahLayer()->loop );
    togglePing->setChecked ( getActiveJahLayer()->ping );

    //extended values here need to add everything new
    GridSelect->setChecked(gridval);
    TextSelect->setChecked(textsafe->layerStatus);

    ClipSelect->setChecked(getActiveJahLayer()->drawtheLayer);
    KeySelect->setChecked(getActiveJahLayer()->drawtheKey);

}

















