###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE = lib
CONFIG		+= staticlib
HEADERS		= 		base.h edit.h preview.h gpupreview.h project.h audio.h undo.h track.h \
					item.h table.h page.h pageproject.h pagecut.h pagetrack.h pagesettings.h render.h
SOURCES		= 		edit.cpp preview.cpp gpupreview.cpp audio.cpp project.cpp fxbar.cpp \
					render.cpp pageproject.cpp pagecut.cpp pagetrack.cpp pagesettings.cpp 

contains( JAHOS,LINUX ) {
	HEADERS += previewsdl.h
	SOURCES += previewsdl.cpp
}

TARGET = edit
DEPENDPATH = $$JAHDEPENDPATH

###############################################################
#the project related includes
INCLUDEPATH =           .  

INCLUDEPATH +=		../../../OpenLibraries/opencore \
			../../../OpenLibraries/openassetlib \
			../../../OpenLibraries/opengpulib
						
INCLUDEPATH +=          ../../JahCore/jahobjects \
			../../JahCore/jahworld 

INCLUDEPATH += ../../JahWidgets/interfaceobjs \
			../../JahWidgets/keyframes \
			../../JahWidgets/multitrack \
			../../JahWidgets/mediatable 

INCLUDEPATH +=		../../JahDesktop/desktop

INCLUDEPATH += ../../JahLibraries/jahformatter \
			../../JahLibraries/jahglcore \ 
			../../JahLibraries/jahpreferences \
			../../JahLibraries/jahtranslate \
			$$FREEDIR

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT,true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

contains( OPENALFRAMEWORK,true ) {
	INCLUDEPATH += $$OPENAL_INCLUDE
}

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}

#patch for glx qt namespace conflicts		
QMAKE_CXXFLAGS+="-DQT_CLEAN_NAMESPACE"




