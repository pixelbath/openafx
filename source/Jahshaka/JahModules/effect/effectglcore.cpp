/*******************************************************************************
**
** The source file for the Jahshaka animation module
** The Jahshaka Project
** Copyright (C) 2000-2006 VM Inc.
** Released under the GNU General Public License
**
*******************************************************************************/

#include <stdio.h>

#include "effect.h"
#include "pbuffer.h"
#include <qtimer.h>
#include <jahrender.h>


void GLEffect::paintGL()
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    
    if (!select)
    { 
        if ( isLayerVisible( getJahLayer(0) ) )
        {
            renderSpace->drawLayer( getJahLayer(0), true );
        }
        else
        {
            renderSpace->drawLayer(titlesafe, true);
        }
    }
    
    //draw the grid if its visible
    if (gridval) 
    {  
        renderSpace->drawLayer(thegrid, true); 
    }
    
    drawAllLayers(0, getRenderOnlyToSelectedEffect() );

    if (textsafe->layerStatus) 
    {  
        renderSpace->drawLayer(textsafe, true);  
    }
    
    m_sliders_have_changed = false;

    glPopMatrix();
}
