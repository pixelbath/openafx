/*******************************************************************************
**
** The source file for the Jahshaka animation interface module
** The Jahshaka Project
** Copyright (C) 2000-2006 VM Inc.
** Released under the GNU General Public License
**
*******************************************************************************/

#include "effect.h"
#include <InputLCD.h>
#include <supergrangecontrol.h>
#include <jahformatter.h>
#include <dialogs.h>
#include <openmedialib.h>
#include <jahtranslate.h>
#include <valueFormatters.h>

#include <qlistview.h>
#include <qframe.h>
#include <qhbox.h>
#include <qslider.h>
#include <qlayout.h>
#include <widget.h>

GLfloat GLEffect::setZoom( GLfloat )
{
	return GLfloat( zoom->value( ) + 1 ) / 1000.0;
}

void GLEffect::buildInterfaceMenus( QHBox*, QHBox* rightcontroller) 
{

	// Container widget
	QWidget *container = new QWidget( rightcontroller, "container" );
	QVBoxLayout *container_layout = new QVBoxLayout( container, 0, 0, "container_layout");

	container_layout->addStretch( 0 );

    //rightmenu here
	QString image = JahBasePath+"Pixmaps/desktop/zoomin.png";
    toolzoomout = new JahToolButton( container, "zoomout" );
    JahFormatter::addJahPlayerButton( toolzoomout, image, image );
	toolzoomout->setAutoRepeat( true );
	container_layout->addWidget( toolzoomout );

	container_layout->addSpacing( 3 );

    zoom = new QSlider( container, "scrubslider" );
    zoom->setOrientation( QSlider::Vertical );
    zoom->setMinValue( 1 );    
    zoom->setMaxValue( 5000 );   
    zoom->setValue   ( 1000 );
    zoom->setPageStep( 10 );
	container_layout->addWidget( zoom, 0, Qt::AlignHCenter );

	container_layout->addSpacing( 3 );

    toolzoomin = new JahToolButton( container, "zoomin" );
	image = JahBasePath+"Pixmaps/desktop/zoomout.png";
    JahFormatter::addJahPlayerButton( toolzoomin, image, image );
	toolzoomin->setAutoRepeat( true );
	container_layout->addWidget( toolzoomin );

	container_layout->addSpacing( 10 );

    //moving ui elements after zoom
    tooltranslate = new JahToolButton( container, "translate" );
    JahFormatter::addJahPlayerButton( tooltranslate, JahBasePath+"Pixmaps/desktop/transtool.png", JahBasePath+"Pixmaps/desktop/transtool.png" );
	container_layout->addWidget( tooltranslate );
    
    toolrotate =   new JahToolButton( container, "rotate" );
    JahFormatter::addJahPlayerButton( toolrotate, JahBasePath+"Pixmaps/desktop/rotatetool.png", JahBasePath+"Pixmaps/desktop/rotatetool.png" );
	container_layout->addWidget( toolrotate );
    
    toolscale =   new JahToolButton( container, "scale" );
    JahFormatter::addJahPlayerButton( toolscale, JahBasePath+"Pixmaps/desktop/scaletool.png", JahBasePath+"Pixmaps/desktop/scaletool.png" );
	container_layout->addWidget( toolscale );
    
	container_layout->addSpacing( 10 );

    scrubrender = new JahToolButton( container, "controllerrewindbutton" );
    JahFormatter::addJahPlayerButton( scrubrender, JahBasePath+"Pixmaps/desktop/phototool.png", JahBasePath+"Pixmaps/desktop/phototool.png" );
    connect( scrubrender,  SIGNAL(clicked()), SLOT(Render())  );
	container_layout->addWidget( scrubrender );
    
    scrubrenderAll = new JahToolButton( container, "controllerpreviousbutton" );
    JahFormatter::addJahPlayerButton( scrubrenderAll, JahBasePath+"Pixmaps/desktop/rendertool.png", JahBasePath+"Pixmaps/desktop/rendertool.png" );
    connect( scrubrenderAll,  SIGNAL(clicked()), SLOT( RenderScene() )  );
	container_layout->addWidget( scrubrenderAll );


    //connect tools to object and add object to display widget
    connect( tooltranslate,  SIGNAL(clicked()), this, SLOT(toolTranslate() )  );
    connect( toolscale,      SIGNAL(clicked()), this, SLOT(toolScale() )  );
    connect( toolrotate,     SIGNAL(clicked()), this, SLOT(toolRotate() )  );

    connect( toolzoomin,    SIGNAL(clicked()), this, SLOT(resetZoom() )  );
    connect( toolzoomout,    SIGNAL(clicked()), this, SLOT(resetZoom() )  );
    connect( zoom,  SIGNAL(valueChanged(int)), this, SLOT(changeZoom(int) )  );

	container_layout->addStretch( );
}

void GLEffect::buildInterface( QHBox* f ) 
{
    JahPrefs& jprefs = JahPrefs::getInstance();

    QWidget* placeholder = new QWidget( f );
    QHBoxLayout* mainLayout = new QHBoxLayout( placeholder );
    // JahFormatter::setMarginAndSpacing( mainLayout );

    ///////////////////////////////////////////////////////////
    //The layers interface

    QBoxLayout* layersLayout = new QVBoxLayout();
    mainLayout->addLayout( layersLayout );
    JahFormatter::setMarginAndSpacingSmall( layersLayout );

    ///////////////////////////////////////////////////////////
    //The layer options
    //these are defined in the world object
    AddButton = new QPushButton( placeholder, "AddButton" );
    AddButton->setText( jt->tr("Add CPU") );
    layersLayout->addWidget( AddButton );
    connect  ( AddButton,  SIGNAL(clicked()), this, SLOT( addCpuEffect())  );

    AddRTButton = new QPushButton( placeholder, "AddRTButton" );
    AddRTButton->setText( jt->tr("Add RT") );
    layersLayout->addWidget( AddRTButton );
    connect  ( AddRTButton,  SIGNAL(clicked()), this, SLOT( addMeshEffect() )  );

    AddGpuButton = new QPushButton( placeholder, "AddGpuButton" );
    AddGpuButton->setText( jt->tr("Add GPU") );
    layersLayout->addWidget( AddGpuButton );
    connect  ( AddGpuButton,  SIGNAL(clicked()), this, SLOT( addGpuEffect() )  );

    JahFormatter::addSpacingSmall( layersLayout );

    NameButton = new QPushButton( placeholder, "NameButton" );
    NameButton->setText( jt->tr("Name") );
    layersLayout->addWidget( NameButton );
    connect  ( NameButton,  SIGNAL(clicked()), this, SLOT( nameLayer())  );

    namepopup = new FancyPopup( this, jt->tr("Enter Layer Name"), 250, 150 );  //send size and position as well
    connect ( namepopup,      SIGNAL( returnText(QString)), SLOT( setlayerName(QString)) );

    DelButton = new QPushButton( placeholder, "Delete Button" );
    DelButton->setText( jt->tr("Del") );
    layersLayout->addWidget( DelButton );
    connect  ( DelButton,  SIGNAL(clicked()), this, SLOT( delLayer())  );

    JahFormatter::addSpacingSmall( layersLayout );

    MoveupButton = new QPushButton( placeholder, "Move Layer Up Button" );
    MoveupButton->setText( jt->tr("Up") );
    layersLayout->addWidget( MoveupButton );
    connect  ( MoveupButton,  SIGNAL(clicked()), this, SLOT( moveLayerUp())  );

    MovedownButton = new QPushButton( placeholder, "Move Layer Down Button" );
    MovedownButton->setText( jt->tr("Down") );
    layersLayout->addWidget( MovedownButton );
    connect  ( MovedownButton,  SIGNAL(clicked()), this, SLOT( moveLayerDown())  );

    layersLayout->addStretch();

    //////////////////////////////////////////////////////////////
    // set up the scene options, or prefernces

    EffectsFrame = new QFrame( placeholder );
    QBoxLayout* EffectsFrameLayout = new QHBoxLayout( EffectsFrame );
    JahFormatter::setSpacing( EffectsFrameLayout );
    mainLayout->addWidget( EffectsFrame );

    JahFormatter::addSpacingSmall( EffectsFrameLayout );

    /////////////////////////////////////////////////////////
    //this is the list-view
    m_layer_listview = new QListView( EffectsFrame );
    m_layer_listview->setRootIsDecorated(true);

    connect( m_layer_listview, SIGNAL( clicked( QListViewItem* ) ), this, SLOT( layerClicked( QListViewItem* ) ) );

    m_layer_listview->addColumn( (jt->tr("EFFECTS")), -1 );
    m_layer_listview->setSorting( -1,1 );   // disables the autosort
    //effects currently only works in single selection mode
    m_layer_listview->setSelectionMode (QListView::Single );

    JahFormatter::setListViewAsSingleColumn( m_layer_listview );

    EffectsFrameLayout->addWidget( m_layer_listview );

    /////////////////////////////////////////////////////////
    //Set up tabbed interface

    tabframe = new QTabWidget( EffectsFrame, "axiscontrols" );	
    EffectsFrameLayout->addWidget( tabframe );

    tabframe->setTabShape( QTabWidget::Triangular );
    tabframe->setAutoMask( FALSE );

    //this has become the settings panel need to update variable names
    moduleOptionsUI[0] = new QHBox( EffectsFrame, "encoderUI0" ); //scenecontrols
    tabframe->insertTab( moduleOptionsUI[0], tr( "  "+jt->tr("SCENE")+"  " ) );

    moduleOptionsUI[1] = new QHBox( EffectsFrame, "axiscontrols" ); //axiscontrols
    tabframe->insertTab( moduleOptionsUI[1], tr( "  "+jt->tr("CONTROLS")+"  " ) );

    moduleOptionsUI[2] = new QHBox( EffectsFrame, "texturecontrols" ); //texturecontrols
    tabframe->insertTab( moduleOptionsUI[2], tr( "    "+jt->tr("CLIP")+"    " ) );

    //this has become the settings panel need to update variable names
    moduleOptionsUI[3] = new QHBox( EffectsFrame, "keysettings" ); //keycontrols
    tabframe->insertTab( moduleOptionsUI[3], tr( "  "+jt->tr("KEYFRAMES")+"  " ) );

    /////////////////////////////////////////////////////////////////////
    // the layout boxes for different types of layers
    // the object control tab bar
    // this is the object control panel

    //QPalette p( QColor( 0, 0, 0 ) );
    //p.setColor( QPalette::Active, QColorGroup::Foreground, Qt::green );

    ///////////////////////////////////////////////////////////////////
    // the scene control tab bar

    //mover this into the layers...
    ControlFrameUI[0][0] = new QFrame( moduleOptionsUI[0], "ControlFrame3" ); //scenecontrols
    QBoxLayout* outer = new QVBoxLayout( ControlFrameUI[0][0] );
    QBoxLayout* top = new QHBoxLayout();
    JahFormatter::setMarginAndSpacing( top );
    outer->addLayout( top );

    QBoxLayout* buttons = new QVBoxLayout();
    buttons->setResizeMode( QLayout::Fixed );
    top->addLayout( buttons );

    LoadButton = new QPushButton( ControlFrameUI[0][0], "LoadButton" );
    LoadButton->setText( jt->tr("Load") );
    buttons->addWidget( LoadButton );
    connect  ( LoadButton,  SIGNAL(clicked()), this, SLOT( SceneLoad())  );

    SaveButton = new QPushButton( ControlFrameUI[0][0], "SaveButton" );
    SaveButton->setText( jt->tr("Save") );
    buttons->addWidget( SaveButton );
    connect  ( SaveButton,  SIGNAL(clicked()), this, SLOT( sceneSave())  );

    ResetButton = new QPushButton( ControlFrameUI[0][0], "ResetButton" );
    ResetButton->setText( jt->tr("Reset All") );
    buttons->addWidget( ResetButton );
    connect  ( ResetButton,  SIGNAL(clicked()), this, SLOT( ResetAll())  );

    ClearButton = new QPushButton( ControlFrameUI[0][0], "ClearButton" );
    ClearButton->setText( jt->tr("Clear Scene") );
    buttons->addWidget( ClearButton );
    connect( ClearButton,  SIGNAL(clicked()), this, SLOT( ClearAll())  );

    QBoxLayout* col2 = new QVBoxLayout();
    top->addLayout( col2 );

    QBoxLayout* checkAndFrameLayout = new QHBoxLayout();
    col2->addLayout( checkAndFrameLayout );

    QBoxLayout* checkLayout = new QVBoxLayout();
    checkAndFrameLayout->addLayout( checkLayout );

    VideoRenderSelect = new QCheckBox( ControlFrameUI[0][0], "SmoothSelect" );
    VideoRenderSelect->setText( jt->tr("Render using source res") );
    checkLayout->addWidget( VideoRenderSelect );
    connect( VideoRenderSelect,  SIGNAL(clicked()), this, SLOT(changeVideoRender())  );

    m_render_only_to_selected_effect_qcheckbox = new QCheckBox( ControlFrameUI[0][0], "SmoothSelect" );
    m_render_only_to_selected_effect_qcheckbox->setText( jt->tr("Draw only to selected effect") );
    checkLayout->addWidget( m_render_only_to_selected_effect_qcheckbox );
    connect( m_render_only_to_selected_effect_qcheckbox,  SIGNAL(clicked()), this, SLOT(changeRenderOnlyToSelectedEffect())  );

    QGridLayout* lcdLayout = new QGridLayout( 2, 2 );
    checkAndFrameLayout->addLayout( lcdLayout );
    
    QLabel* sframetext = new QLabel( ControlFrameUI[0][0], "sframetext" );
    lcdLayout->addWidget( sframetext, 0, 0, Qt::AlignVCenter );
    sframetext->setText( tr( jt->tr("STARTFRAME") ) );
    sframetext->setAlignment( int( QLabel::AlignRight ) );
    
    startFrameControl = new SupergRangeControl( ControlFrameUI[0][0], "startframe" );
    startFrameControl->setFormatter( TimecodeValueFormatter() );
    lcdLayout->addWidget( startFrameControl, 0, 1 );
    JahFormatter::configure( startFrameControl, Astartframe, Astartframe );
    connect( startFrameControl, SIGNAL(valueChanged(int)),   SLOT(updatesliderStartframe(int)) );
    
    QLabel* eframetext = new QLabel( ControlFrameUI[0][0], "eframetext" );
    lcdLayout->addWidget( eframetext, 1, 0, Qt::AlignVCenter );
    eframetext->setText( tr( jt->tr("ENDFRAME")  ) );
    eframetext->setAlignment( int( QLabel::AlignRight ) );
    
    endFrameControl = new SupergRangeControl( ControlFrameUI[0][0], "endframe" );
    endFrameControl->setFormatter( TimecodeValueFormatter() );
    lcdLayout->addWidget( endFrameControl, 1, 1 );
    JahFormatter::configure( endFrameControl, Aendframe, Astartframe );
    connect( endFrameControl, SIGNAL(valueChanged(int)),   SLOT(updatesliderEndframe(int)) );

    QBoxLayout* checkAndResLayout = new QHBoxLayout();
    col2->addLayout( checkAndResLayout );

    mesh1 = new QCheckBox( ControlFrameUI[0][0], "light1" );
    mesh1->setText( jt->tr("toggle mesh") );
    checkAndResLayout->addWidget( mesh1 );
    connect( mesh1,  SIGNAL(clicked()), this, SLOT(toggleMesh())  );

    m_show_lights_checkbox = new QCheckBox( ControlFrameUI[0][0], "m_show_lights_checkbox" );
    m_show_lights_checkbox->setText( jt->tr("Auto Key") );
    checkAndResLayout->addWidget( m_show_lights_checkbox );

    QGridLayout* g = new QGridLayout( 2, 2 );
    checkAndResLayout->addLayout( g );

    m_select_fps_label = new QLabel( ControlFrameUI[0][0], "FPSPushButtonLabel" );
    g->addWidget( m_select_fps_label, 0, 0 );
    m_select_fps_label->setAlignment( int( QLabel::AlignCenter ) );
    m_select_fps_label->setText( jt->tr( "Playback Speed" ) );

    m_select_fps_pushbutton = new QPushButton( ControlFrameUI[0][0], "FPSPushButton" );
    g->addWidget( m_select_fps_pushbutton, 1, 0 );
    QString text_string = getFramesPerSecondName(FRAMES_PER_SECOND_30).data();
    m_select_fps_pushbutton->setText(text_string);
    connect(m_select_fps_pushbutton, SIGNAL( pressed() ), SLOT( slotChooseFramesPerSecond() ) );

    jahreslabel = new QLabel( ControlFrameUI[0][0], "languagelabel" );
    g->addWidget( jahreslabel, 0, 1 );
    jahreslabel->setAlignment( int( QLabel::AlignCenter ) );
    jahreslabel->setText( jt->tr( "Resolution" ) );

    JahresCombo = new QComboBox( FALSE, ControlFrameUI[0][0], "JahresCombo" );
    g->addWidget( JahresCombo, 1, 1 );
    hasResMenu = true;
    projectData thedata;
    thedata.buildComboBox(JahresCombo);
    int defaultRes = jprefs.getJahResolution();
    JahresCombo->setCurrentItem(defaultRes); //should be set to appdefault
    connect( JahresCombo,  SIGNAL(activated(int)), this, SLOT(setResolution(int))  );

    top->addStretch();
    outer->addStretch();
	

    ///////////////////////////////////////////////////////////////
    // object layer options sub control set

    ControlFrameUI[1][0] = new QFrame( moduleOptionsUI[1], "ControlFrame1" ); //ControlFrame1
    ControlFrameUI[1][0]->setGeometry( QRect( 0, 0, 660, 190 ) );    // was 510

    createAxisPanel ( ControlFrameUI[1][0] );

    //add the placeholders for plugins
    JahModuleOption[0] = new QCheckBox( ControlFrameUI[1][0], "Placeholder" );
    JahFormatter::addCheckButton( JahModuleOption[0], 520, 30, 145, 25, "Placeholder1" );
    connect( JahModuleOption[0],  SIGNAL(clicked()), this, SLOT(setOption1())  ); //for clip

    //increment the ui uption counter...
    numUiOptions +=1;

    //add the placeholders for plugins
    JahModuleOption[1] = new QCheckBox( ControlFrameUI[1][0], "Placeholder" );
    JahFormatter::addCheckButton( JahModuleOption[1], 520, 60, 145, 25, "Placeholder2" );
    connect( JahModuleOption[1],  SIGNAL(clicked()), this, SLOT(setOption2())  ); //for clip

    //increment the ui uption counter...
    numUiOptions +=1;

    //add the placeholders for plugins
    JahModuleOption[2] = new QCheckBox( ControlFrameUI[1][0], "Placeholder" );
    JahFormatter::addCheckButton( JahModuleOption[2], 520, 90, 145, 25, "Placeholder3" );
    connect( JahModuleOption[2],  SIGNAL(clicked()), this, SLOT(setOption3())  ); //for clip

    //increment the ui uption counter...
    numUiOptions +=1;

    //add the placeholders for plugins
    JahModuleOption[3] = new QCheckBox( ControlFrameUI[1][0], "Placeholder" );
    JahFormatter::addCheckButton( JahModuleOption[3], 520, 120, 145, 25, "Placeholder4" );
    connect( JahModuleOption[3],  SIGNAL(clicked()), this, SLOT(setOption4())  ); //for clip

    //increment the ui uption counter...
    numUiOptions +=1;

    hasButtons = true;


    ///////////////////////////////////////////////////////////////
    // texture layer options
    // need to add video-texture options here
    //image options

    ControlFrameUI[2][0] = new QFrame( moduleOptionsUI[2], "ControlFrame2" ); //was ControlFrame2
    ControlFrameUI[2][0]->setGeometry( QRect( 0, 0, 660, 190 ) );

    ClipSelect = new QCheckBox( ControlFrameUI[2][0], "ClipSelect" );
    JahFormatter::addCheckButton( ClipSelect, 0, 30, 45, 25, jt->tr("Clip") );
    connect( ClipSelect,  SIGNAL(clicked()), this, SLOT( setClipStatus() )  ); //for clip
    
    grabdesktopClip = new QPushButton( ControlFrameUI[2][0], "grabdesktop" );
    JahFormatter::addButton( grabdesktopClip, 50, 30, 110, 25, jt->tr("Grab Desktop") );
    connect( grabdesktopClip,  SIGNAL(clicked()), this, SLOT( grabDesktop() )  );
    
    keyimage_2 = new QPushButton( ControlFrameUI[2][0], "clipmapping" );
    JahFormatter::addButton( keyimage_2, 165, 30, 35, 25, "IM" );
    connect( keyimage_2,  SIGNAL(clicked()), this, SLOT(keyCliplayer())  );

    m_use_opengl_key_layer_checkbox = new QCheckBox( ControlFrameUI[2][0], "KeySelect" );
    JahFormatter::addCheckButton( m_use_opengl_key_layer_checkbox, 220, 30, 120, 25, jt->tr("OpenGL Key") );
    connect( m_use_opengl_key_layer_checkbox,  SIGNAL(clicked()), this, SLOT( slotSetUseOpenGlKeyer() )  );
    
    m_use_fast_shader_key_layer_checkbox = new QCheckBox( ControlFrameUI[2][0], "m_use_fast_shader_key_layer_checkbox" );
    JahFormatter::addCheckButton( m_use_fast_shader_key_layer_checkbox, 220, 52, 120, 25, jt->tr("Fast Shader Key") );
    connect( m_use_fast_shader_key_layer_checkbox,  SIGNAL(clicked()), this, SLOT( slotSetUseFastShaderKeyer() )  );

    KeySelect = new QCheckBox( ControlFrameUI[2][0], "KeySelect" );
    JahFormatter::addCheckButton( KeySelect, 0, 75, 45, 25, jt->tr("Key") );
    connect( KeySelect,  SIGNAL(clicked()), this, SLOT( setKeyStatus() )  );
    
    grabdesktopKey = new QPushButton( ControlFrameUI[2][0], "grabdesktop" );
    JahFormatter::addButton( grabdesktopKey, 50, 75, 110, 25, jt->tr("Grab Desktop") );
    connect( grabdesktopKey,  SIGNAL(clicked()), this, SLOT( grabDesktopKey() )  ); //for key...
    
    keyimage_3 = new QPushButton( ControlFrameUI[2][0], "keyOne_3" );
    JahFormatter::addButton( keyimage_3, 165, 75, 35, 25, "KM" );
    connect( keyimage_3,  SIGNAL(clicked()), this, SLOT(keyClipkey())  );
    
    keyStatusBox = new QPushButton( ControlFrameUI[2][0], "Select1" );
    JahFormatter::addButton( keyStatusBox, 220, 75, 35, 25, "Invert" );
    connect( keyStatusBox,  SIGNAL(clicked()), this, SLOT( InvertKeyData() )  );
    
    // video options
    // need to hook this up now...
    
    VideoLabel = new QLabel( ControlFrameUI[2][0], "TranslateText_1" );
    JahFormatter::addLabel( VideoLabel, 380, 10, 120, 21, "VIDEO OPTIONS", 9);
    
    
    m_slip_frames_lcd = new InputLCD( ControlFrameUI[2][0] );
    JahFormatter::addLabelLcd( ControlFrameUI[2][0], m_slip_frames_lcd, 380, 30, 60, 21,  0,  "Slip", 8);
    connect(m_slip_frames_lcd, SIGNAL(valueChanged(int)), this, SLOT( slotSetSlipFrames(int) ) );
    // These are necessary because of some Qt problem
    m_slip_frames_lcd->setValue(1);
    m_slip_frames_lcd->setValue(0);
    
    m_in_frames_lcd = new InputLCD( ControlFrameUI[2][0] );
    JahFormatter::addLcdLabel( ControlFrameUI[2][0], m_in_frames_lcd,     460, 30, 30, 21,  0,  "In", 8);
    m_in_frames_lcd->setMinInt(1);
    connect(m_in_frames_lcd, SIGNAL(valueChanged(int)), this, SLOT( setinFrames(int) ) );
    
    m_out_frames_lcd = new InputLCD( ControlFrameUI[2][0] );
    JahFormatter::addLcdLabel( ControlFrameUI[2][0], m_out_frames_lcd,    530, 30, 30, 21,  0,  "Out", 8);
    m_out_frames_lcd->setMinInt(1);
    connect(m_out_frames_lcd, SIGNAL(valueChanged(int)), this, SLOT( setoutFrames(int) ) );
    
    m_key_slip_frames_lcd = new InputLCD( ControlFrameUI[2][0] );
    JahFormatter::addLabelLcd( ControlFrameUI[2][0], m_key_slip_frames_lcd,    380, 75, 60, 21,  0,  "Slip", 8);
    connect(m_key_slip_frames_lcd, SIGNAL(valueChanged(int)), this, SLOT( slotSetKeySlipFrames(int) ) );
    // These are necessary because of some Qt problem
    m_key_slip_frames_lcd->setValue(1);
    m_key_slip_frames_lcd->setValue(0);
    
    m_key_in_frames_lcd = new InputLCD( ControlFrameUI[2][0] );
    JahFormatter::addLcdLabel( ControlFrameUI[2][0], m_key_in_frames_lcd,  460, 75, 30, 21,  0,  "In", 8);
    m_key_in_frames_lcd->setMinInt(1);
    connect(m_key_in_frames_lcd, SIGNAL(valueChanged(int)), this, SLOT( setinKeyFrames(int) ) );
    
    m_key_out_frames_lcd = new InputLCD( ControlFrameUI[2][0] );
    JahFormatter::addLcdLabel( ControlFrameUI[2][0], m_key_out_frames_lcd, 530, 75, 30, 21,  0,  "Out", 8);
    m_key_out_frames_lcd->setMinInt(1);
    connect(m_key_out_frames_lcd, SIGNAL(valueChanged(int)), this, SLOT( setoutKeyFrames(int) ) );
    
    extendHeadTail = new QCheckBox( ControlFrameUI[2][0], "extendHeadTail" );
    JahFormatter::addCheckButton( extendHeadTail, 380, 95, 150, 25, "Extend Head-Tail" );
    connect( extendHeadTail,  SIGNAL(clicked()), this, SLOT(toggleExtendHeadTail())  );

    m_lock_key_and_clip_checkbox = new QCheckBox( ControlFrameUI[2][0], "m_lock_key_and_clip_checkbox" );
    JahFormatter::addCheckButton( m_lock_key_and_clip_checkbox, 380, 130, 100, 25, "Lock Key" );
    connect( m_lock_key_and_clip_checkbox,  SIGNAL(clicked()), this, SLOT( slotUpdateAnimation() )  );
    
    toggleTheLoop = new QCheckBox( ControlFrameUI[2][0], "toggleTheLoop" );
    JahFormatter::addCheckButton( toggleTheLoop, 510, 95, 100, 25, "Loop" );
    connect( toggleTheLoop,  SIGNAL(clicked()), this, SLOT( toggleLoop() )  );
    
    togglePing = new QCheckBox( ControlFrameUI[2][0], "togglePing" );
    JahFormatter::addCheckButton( togglePing, 510, 130, 100, 25, "Ping Pong" );
    connect( togglePing,  SIGNAL(clicked()), this, SLOT( togglePingPong() )  );
    
    //what is this used for?
    //need to make the icon a lock... 3x current height as well for links
    //lockicon	= new QToolButton(ControlFrameUI[2][0],"lockicon");
    //lockicon->setGeometry( QRect( 600, 55, 18, 26 ) );
    //lockicon->setFixedSize(18, 26);  	//lighticon->setUsesBigPixmap(true);
    //lockicon->setAutoRaise(true);
    
    FindMediaLabel = new QLabel( ControlFrameUI[2][0], "FindMediaLabel" );
    JahFormatter::addLabel( FindMediaLabel, 0, 110, 110, 21, "Media Location", 9);
    
    FindMediaText	= new QLineEdit( ControlFrameUI[2][0], "FindMediaText" );
    FindMediaText->setGeometry( QRect( 2, 135, 250, 21 ) );
    //FindMediaText->setText( "jahshaka" );
    
    FindMediaButton = new QPushButton( ControlFrameUI[2][0], "find" );
    JahFormatter::addButton( FindMediaButton, 255, 135, 55, 21, jt->tr("locate") );
    connect( FindMediaButton,  SIGNAL(clicked()), this, SLOT( locateMissingMedia() )  );
    

	////////////////////////////////////////////////////////
	// give this man a keyframer!
	addKeyframer(moduleOptionsUI[3]);

}

void GLEffect::createMenuItem( QPopupMenu * themenu )
{
	Q_CHECK_PTR(themenu);
    themenu->insertItem("&"+jt->tr("CPU Effect"),this,SLOT( addCpuEffectFromTopMenu() ) );
    themenu->insertItem("&"+jt->tr("Mesh Effect"),this,SLOT( addMeshEffectFromTopMenu() ) );
    themenu->insertItem("&"+jt->tr("GPU Effect"),this,SLOT( addGpuEffectFromTopMenu() ) );
    themenu->insertSeparator();
    themenu->insertItem("&"+jt->tr("Delete Effect"),this,SLOT( delLayer() ) );
    themenu->insertSeparator();
    themenu->insertItem("&"+jt->tr("Move Effect Up"),this, SLOT( moveLayerUp() ) );
    themenu->insertItem("&"+jt->tr("Move Effect Down"),this, SLOT( moveLayerDown() ) ); 
}























