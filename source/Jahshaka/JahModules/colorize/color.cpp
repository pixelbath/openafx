/*******************************************************************************
**
** The source file for the Jahshaka color module
** The Jahshaka Project
** Copyright (C) 2000-2006 VM Inc.
** Released under the GNU General Public License
**
*******************************************************************************/

#include "color.h"
#include <glworldlists.h>
#include <glcore.h>
#include <InputLCD.h>
#include <supergrangecontrol.h>
#include <qlabel.h>
#include <jahpreferences.h>

void GLColorize::start( )
{
	buildInterface(m_controls);
	buildInterfaceMenus(m_leftcontroller,m_rightcontroller);
	initializeObjects();

	// Update sliders to reflected selected fps
	setResolution( JahPrefs::getInstance().getJahResolution() );
}

void GLColorize::initializeObjects()
{    
    jtrace->info( "Initializing GLColor Class");

    //make the current opengl context current so the textures can be set up properly
    makeCurrent();
	
    ///////////////////////////////////////////////////////////////////
	// going to replace this code with a routine to do it for us

	JahLayer* jah_layer = configureLayer ( "World", false, true, false, true );
    buildLayer(jah_layer, "World", LayerCategory::LAYER, ObjectCategory::BACKDROP );

	jah_layer = configureLayer ( "Image Layer", true, true, true, true );
	buildLayer(jah_layer, "Image Layer", LayerCategory::LAYER, ObjectCategory::MESHLAYER );  

    setImageLayer( getLayerList()->last()->getJahLayer() );

	//setup/customize listviewitem for layer 1
	initializeListview(1,TRUE);

	//call objectmenu to update ui and then update sliders
    objectMenu(getNumberOfLayers() - 1);
	updateSliders();

    /////////////////////////////////////////////////////
    //initialize the buffer used for effect outputimage buffering
    //configureBuffer(1); 

    //turn forceplay on here
    setForcePlay(true);

    //////////////////////////////////////////////////////
    //added for custom variables that need to be set up 
    initializeVariables();

    //////////////////////////////////////////////////////
    //sets up the core functionality
    configureModuleEngine();

	
    //////////////////////////////////////////////////////
    //set the ui tool
    noTool();

	////////////////////////////////////////////
    //use source size renders
    VideoRenderSelect->setChecked(true);
	m_render_at_image_resolution = true;

    JahSlidersLabel[0]->setText("Red");
    JahSlidersLabel[1]->setText("Green");
    JahSlidersLabel[2]->setText("Blue");

    JahSlidersLabel[3]->setText("Tint");
    JahSlidersLabel[4]->setText("Brightness");
    JahSlidersLabel[5]->setText("Strength");

    JahSlidersLabel[6]->setText("Hue");
    JahSlidersLabel[7]->setText("Saturation");
    JahSlidersLabel[8]->setText("Value");

    JahSlidersLabel[9]->setText("Gamma");

    setSliderValue(9, 100);
}

void GLColorize::initializeVariables(void)
{
    //this is a flag to make sure this routine isnt run
    //more than one time... not sure if this is really
    //needed anyore

    static bool initVariables = true;

    //this is used for the gpu effects
    // The camera distance calculation should be passed in from the GL resize
    // routine.  This is a HACK which should be fixed FIXME

    if (initVariables)
    {
        #define PI                  3.14159265358979323846
        #define VIEW_ANGLE			45.0
        #define VIEW_ANGLE_RADIANS 	((VIEW_ANGLE / 180.0) * PI)

        m_camera_distance = ((float)Y_RESOLUTION / 2.0) / tan(VIEW_ANGLE_RADIANS / 2.0);

        contrastTextureInitialized = FALSE;
    }

    initVariables = false;
}

//this routine is used to configure the modules engine and core functionality
void GLColorize::configureModuleEngine(void)    
{ 
	//////////////////////////////////////////////////////
    //configure the color correction mode

    std::string guid = "";

	if ( nv_gpusupport ) // || gpusupport ) //nvidia support for the color corrector
    {
        std::string title = "Colorize GPU";
        addEffectLayer( getImageLayer(), title, EffectInfo::COLORIZE_GPU_TYPE, -1, guid); 
        m_gpu_select_qcheckbox->toggle();
    }
    else 
	{
        std::string title = "Colorize CPU";
        addEffectLayer(getImageLayer(), title, EffectInfo::COLORIZE_CPU_TYPE, -1, guid);
    }

    getImageLayer()->setEffectsSlidersHaveChanged(true);
    m_sliders_have_changed = true;
}


//////////////////////////////////////////////////
// adds a new layer to the listview and to the system
void GLColorize::objectMenu(int menu)
{
    changeSelection(menu);
}

//////////////////////////////////////////
// need to fix missing command below makeTexture
//////////////////////////////////////////
///////////////////////////////////////////
///////////////////////////////////////////

void GLColorize::addClip(assetData clip)
{
    if (clip.endframe > maxFrame)
    {
        maxFrame = clip.endframe;
        updatesliderEndframe(maxFrame);
        endFrameControl->setValue(maxFrame);
    }

    getImageLayer()->assignAsset(clip, VideoCategory::IMAGE);
    getImageLayer()->LoadLayerAsset(VideoCategory::IMAGE);
    getImageLayer()->makeTexture();

    //set renderspace imagesize here...
    core->imagewidth  = getImageLayer()->getImageWidth();
    core->imageheight = getImageLayer()->getImageHeight();

    //forceupdatefxbuffer = true;

    //initialize the buffer
   // if (useBuffering)
   //     initBuffer(&getImageLayer()->textureData.objectImage);

    updateGL();
    updateSliders();

    m_sliders_have_changed = true;
    
    //update start and end points to the start and end points of the source clip
    updatesliderStartframe (  clip.startframe);

    if (   getImageLayer()->asset.theType == VideoType::MOVIE 
        || getImageLayer()->asset.theType == VideoType::SEQUENCE
       ) 
    {
        updatesliderEndframe(  clip.endframe);
    }

    //update the ui settings
    //startFrameControl->setMinMaxInt( clip.startframe, clip.endframe-1 );
    startFrameControl->setValue(clip.startframe);

    //endFrameControl->setMinMaxInt( clip.startframe+1, clip.endframe );
    endFrameControl->setValue(clip.endframe);

}

void
GLColorize::gpuSelect()
{
    bool selected = getGpuSelectCheckbox()->isOn();
    EffectLayer* effect_layer = getImageLayer()->getEffectsList()->first();

    if (selected)
    {
        effect_layer->setType(EffectInfo::COLORIZE_GPU_TYPE);
        getImageLayer()->layername = "Colorize GPU";
    }
    else
    {
        effect_layer->setType(EffectInfo::COLORIZE_CPU_TYPE);
        getImageLayer()->layername = "Colorize CPU";
    }

    updateGL();
}

QCheckBox*
GLColorize::getGpuSelectCheckbox()
{
    return m_gpu_select_qcheckbox;
}

    

