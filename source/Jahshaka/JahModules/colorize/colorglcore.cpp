/*******************************************************************************
**
** The source file for the Jahshaka color module
** The Jahshaka Project
** Copyright (C) 2000-2006 VM Inc.
** Released under the GNU General Public License
**
*******************************************************************************/

#include "color.h"
#include <coreobject.h>
#include <jahrender.h>
#include "pbuffer.h"
#include <qtimer.h>

void GLColorize::paintGL()
{
     if ( m_animation )
    {  
        updateAnimation( getCurrentFrame() );
        incrementCurrentFrame(1);
        
        if ( getCurrentFrame() > maxFrame )
        {
            setCurrentFrame(1);
        }
    }
    
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    
    if (!select)
    { 
        if ( isLayerVisible( getJahLayer(0) ) )
        {
            renderSpace->drawLayer( getJahLayer(0), true );
        }
        else
        {            
            renderSpace->drawLayer(titlesafe, true);
        }
    }
        
    //draw the grid if its visible
    if (gridval) 
    {  
        renderSpace->drawLayer(thegrid, true); 
    }
    
    JahLayer* base_layer = getJahLayer(1);

    if (base_layer) 
    {
        if ( base_layer->layerStatus )
        {
            buildEffects(base_layer);
            renderSpace->drawLayer(base_layer, true);
        }
    }

    glPopMatrix();
    
    if (textsafe->layerStatus) 
    {  
        renderSpace->drawLayer(textsafe, true);  
    }
    
    glFlush();
    
    if ( m_animation ) 
    {
        timer->start( getRedrawDelay(), TRUE );
    }
    
    m_sliders_have_changed = false;
}





