###############################################################
#
# VisualMedia OpenLibraries QMake build files
#
###############################################################

include( ../../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Building JahModules" )
message( "-------------------" )

SUBDIRS += animation
SUBDIRS += colorize

# Special case for editing with MLT available
contains( MLTSUPPORT,true ) {
	SUBDIRS += editing_mlt
} else {
	SUBDIRS += editing
}

SUBDIRS += effect
SUBDIRS += keyer
SUBDIRS += painter
SUBDIRS += text
SUBDIRS += tracker

contains( JAHPLAYER,true ) {	
SUBDIRS -= colorize
SUBDIRS -= editing
SUBDIRS -= editing_mlt
SUBDIRS -= effect
SUBDIRS -= keyer
SUBDIRS -= painter
SUBDIRS -= text
SUBDIRS -= tracker
}

TEMPLATE = subdirs

