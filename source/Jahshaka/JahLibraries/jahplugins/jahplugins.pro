###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
HEADERS     =           jahplugins.h \
                        jahplugintemplate.h \
			jahpluginlib.h

SOURCES     =           jahplugins.cpp \
                        jahpluginlib.cpp

TARGET      =           jahplugins
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

#files for audiosupport
#contains( JAHAUDIO,true ) {
#INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile 
#}

INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew 
#						../../../AuxiliaryLibraries/particle \
#						../../../AuxiliaryLibraries/FTGL
						
#INCLUDEPATH +=			../../../OpenLibraries/opencore \
#						../../../OpenLibraries/openassetlib \
#						../../../OpenLibraries/openobjectlib \
#						../../../OpenLibraries/openmedialib \
#						../../../OpenLibraries/openmedialib/mediaobject 

#INCLUDEPATH +=          ../../JahCore/jahobjects 

#INCLUDEPATH +=			../../JahWidgets/interfaceobjs 

INCLUDEPATH +=			../../JahLibraries/jahtracer \
						../../JahLibraries/jahpreferences 
#						../../JahLibraries/jahkeyframes \
#						$$FREEDIR

!win32 {
INCLUDEPATH += $$OPENLIBRARIES_INCLUDE 
}
					
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



