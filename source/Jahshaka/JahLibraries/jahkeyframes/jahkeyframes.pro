###############################################################
#
# Jahshaka QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
                        
HEADERS     =           jahkeyframes.h \
                        jahkeyframeobject.h
SOURCES     =           jahkeyframes.cpp \
                        jahkeyframeobject.cpp

TARGET      =           jahkeyframes
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

INCLUDEPATH +=          ../jahtracer \
						$$FREEDIR
						
INCLUDEPATH += $$OPENLIBRARIES_INCLUDE ../../../OpenLibraries/opengpulib

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}


