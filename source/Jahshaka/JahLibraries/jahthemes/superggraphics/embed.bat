@echo on

set FILES= cooldepthbg.png coolsliderbg.png
set FILES=%FILES%  tab-current-left.png tab-current-right.png

echo Embedding: %FILES%

qembed --images %FILES% > superg-embedded-images.h
