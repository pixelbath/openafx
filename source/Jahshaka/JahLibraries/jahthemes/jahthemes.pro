###############################################################
#
# Jahshaka QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
                        
HEADERS     =           themes.h \
			wood.h \
			marble.h \
			brushmetal.h \
			charcoal.h \
			supergtheme.h
			
SOURCES     =           themes.cpp \
			wood.cpp \
			marble.cpp \
			brushmetal.cpp \
			charcoal.cpp \
			supergtheme.cpp

TARGET      =           jahthemes
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

INCLUDEPATH +=			../../../../Pixmaps/interface
						
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



