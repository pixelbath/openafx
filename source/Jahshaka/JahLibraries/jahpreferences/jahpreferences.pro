###############################################################
#
# Jahshaka QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
                        
HEADERS     =           jahpreferences.h \
                        jahstats.h

SOURCES     =           jahpreferences.cpp \
                        jahstats.cpp 

TARGET      =           jahpreferences
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

#files for audiosupport
#contains( JAHAUDIO,true ) {
#INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile 
#}

#INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew 
#						../../../AuxiliaryLibraries/particle \
#						../../../AuxiliaryLibraries/FTGL
						
#INCLUDEPATH +=			../../../OpenLibraries/opencore \
#						../../../OpenLibraries/openassetlib \
#						../../../OpenLibraries/openobjectlib \
#						../../../OpenLibraries/openmedialib \
#						../../../OpenLibraries/openmedialib/mediaobject 

#INCLUDEPATH +=          ../../JahCore/jahobjects 

#INCLUDEPATH +=			../../JahWidgets/interfaceobjs 

INCLUDEPATH +=			../../JahLibraries/jahtracer \
						../../JahLibraries/jahpreferences 
#						../../JahLibraries/jahkeyframes \
#						$$FREEDIR

						
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



