###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE = lib
CONFIG      +=  staticlib 
HEADERS     =   glcore.h pbuffer.h
SOURCES     =   glcore.cpp pbuffer.cpp
TARGET      =   glcore
DEPENDPATH  =   $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =   . 

INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew 

INCLUDEPATH +=          ../../../OpenLibraries/opengpulib 

INCLUDEPATH +=		../../JahLibraries \
				../../JahLibraries/jahtracer \
				../../JahLibraries/jahpreferences \
				../../JahLibraries/jahglcore \
				$$FREEDIR
						

contains( JAHOS,IRIX ) {
INCLUDEPATH += 		$$SGIDIR
}

!win32 {
INCLUDEPATH += /usr/olib/0.1/include
}

#patch for glx qt namespace conflicts		
QMAKE_CXXFLAGS+="-DQT_CLEAN_NAMESPACE"

