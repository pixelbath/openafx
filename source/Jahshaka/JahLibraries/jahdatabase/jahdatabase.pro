###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE        =       lib
CONFIG          +=      staticlib 
HEADERS         =       jahdatabase.h

SOURCES         =       jahdatabase.cpp

TARGET          =       jahdatabase
DEPENDPATH      =       $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile 
}

INCLUDEPATH +=          ../../../OpenLibraries/opencore \
						../../../OpenLibraries/openimagelib \
						../../../OpenLibraries/openmedialib \
						../../../OpenLibraries/openmedialib/mediaobject \
						../../../OpenLibraries/openassetlib \
						../../../OpenLibraries/openassetlib/v2_openassetlib/sqlite3/src \
						../jahtracer \
						../jahpreferences \
						$$FREEDIR
						

contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



