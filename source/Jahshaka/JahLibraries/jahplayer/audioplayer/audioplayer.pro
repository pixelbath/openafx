###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE        =   lib
CONFIG          +=  staticlib 
HEADERS         =   Audio_Dev.h \
                    Audio_IO.h \
                    Audio_OSS.h \
                    types.h \
                    Lock.h
SOURCES         =   Audio_Dev.cpp \
                    Audio_IO.cpp \
                    Audio_OSS.cpp
TARGET          =   audioplayer
DEPENDPATH      =   $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../../AuxiliaryLibraries/sndfile/sndfile 
}

#INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
#						../../../AuxiliaryLibraries/particle \
#						../../../AuxiliaryLibraries/FTGL
						
INCLUDEPATH +=			../../../../OpenLibraries/opencore \
						../../../../OpenLibraries/openassetlib \
						../../../../OpenLibraries/openobjectlib \
						../../../../OpenLibraries/openmedialib \
						../../../../OpenLibraries/openmedialib/mediaobject 

#INCLUDEPATH +=          ../../JahCore/jahobjects 

#INCLUDEPATH +=			../../JahWidgets/interfaceobjs 

INCLUDEPATH +=			../../../JahLibraries/jahtracer 
#						../../../JahLibraries/jahkeyframes \
#						../../../JahLibraries/jahpreferences \
#						$$FREEDIR

						
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}
