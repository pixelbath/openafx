###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE        =  lib
CONFIG          += staticlib 
HEADERS         =  diskplayer.h \
				   videohead.h
				   
SOURCES         =  diskplayer.cpp \
				   videohead.cpp
TARGET          =  diskplayer
DEPENDPATH      =  $$JAHDEPENDPATH

###############################################################
###############################################################
#the project related includes particle
INCLUDEPATH =           . 

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../../AuxiliaryLibraries/sndfile/sndfile 
}

INCLUDEPATH +=          ../../../../AuxiliaryLibraries/glew
#						../../../AuxiliaryLibraries/particle \
#						../../../AuxiliaryLibraries/FTGL
						
INCLUDEPATH +=			../../../../OpenLibraries/opencore \
						../../../../OpenLibraries/openassetlib \
						../../../../OpenLibraries/openobjectlib \
						../../../../OpenLibraries/openimagelib \
						../../../../OpenLibraries/opengpulib \
						../../../../OpenLibraries/openmedialib \
						../../../../OpenLibraries/openmedialib/mediaobject 

#INCLUDEPATH +=          ../../JahCore/jahobjects 

#INCLUDEPATH +=			../../JahWidgets/interfaceobjs 

INCLUDEPATH +=			../../../JahLibraries/jahtracer \
						../../../JahLibraries/jahglcore \
						../../../JahLibraries/jahpreferences 
#						$$FREEDIR

!win32 {
INCLUDEPATH += /usr/olib/0.1/include
}			
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}
