###############################################################
#
# Jahshaka 1.9 QMake build files
#
###############################################################

include( ../../../../Settings.pro )

###############################################################
# Configure subdirectories
message( "Configuring Player Support" )

message( "Adding Media Player Support" )

SUBDIRS +=  diskplayer

#files for audio support
contains( JAHAUDIO,true ) {
message( "Adding Audio Player Support" )
SUBDIRS +=  audioplayer
}

TEMPLATE = subdirs
