###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =   		lib
CONFIG		+=          staticlib 

HEADERS     =		    bxmlnode.h \
				        jahbxmlnode.h \
				        jahloadxml.h 

SOURCES     =   		bxmlnode.cpp \
				        jahbxmlnode.cpp \
                        jahloadxml.cpp 

TARGET      =   		jahxml

DEPENDPATH  =   		$$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT,true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile
}

INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
						../../../AuxiliaryLibraries/particle \
						../../../AuxiliaryLibraries/FTGL
						
INCLUDEPATH +=			../../../OpenLibraries/opencore \
						../../../OpenLibraries/openassetlib \
						../../../OpenLibraries/opengpulib \
						../../../OpenLibraries/openimagelib \
						../../../OpenLibraries/openobjectlib \
						../../../OpenLibraries/openmedialib \
						../../../OpenLibraries/openmedialib/mediaobject 

INCLUDEPATH +=          ../../JahCore/jahobjects 

#INCLUDEPATH +=			../../JahWidgets/interfaceobjs 

INCLUDEPATH +=			../../JahLibraries/jahtracer \
						../../JahLibraries/jahpreferences \
						../../JahLibraries/jahplugins \
						../../JahLibraries/jahkeyframes \
						$$FREEDIR

						
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}




