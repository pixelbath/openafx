###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE        =       lib
CONFIG          +=      staticlib 
HEADERS         =       jahdataio.h

SOURCES         =       jahdataio.cpp  \
                        jahdataioutils.cpp 

TARGET          =       jahdataio
DEPENDPATH      =       $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile/sndfile 
}

INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew \
				../../../AuxiliaryLibraries/particle \
				../../../AuxiliaryLibraries/FTGL
						
INCLUDEPATH +=		../../../OpenLibraries/opencore \
				../../../OpenLibraries/opengpulib \
				../../../OpenLibraries/openassetlib \
				../../../OpenLibraries/openimagelib \
				../../../OpenLibraries/openobjectlib \
				../../../OpenLibraries/openmedialib \
				../../../OpenLibraries/openmedialib/mediaobject 

INCLUDEPATH +=          ../../JahCore/jahobjects  \
				../../JahCore/jahworld


INCLUDEPATH +=		../../JahLibraries/jahtracer \
				../../JahLibraries/jahkeyframes \
				../../JahLibraries/jahplugins \
				../../JahLibraries/jahpreferences \
				$$FREEDIR

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT, true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}
						
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



