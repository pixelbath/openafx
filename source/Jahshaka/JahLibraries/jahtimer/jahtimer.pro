###############################################################
#
# Jahshaka 1.9a4 QMake module files
#
###############################################################

#include presets file in home directory
include( ../../../../Settings.pro )

###############################################################
#the rest of the makefile settings

TEMPLATE    =           lib
CONFIG      +=          staticlib 
                        
HEADERS     =           jahtimer.h 
SOURCES     =           jahtimer.cpp 

TARGET      =           jahtimer
DEPENDPATH  =           $$JAHDEPENDPATH

###############################################################
#the project related includes particle
INCLUDEPATH =           . 

#files for audiosupport
#contains( JAHAUDIO,true ) {
#INCLUDEPATH +=          ../../../AuxiliaryLibraries/sndfile 
#}

#INCLUDEPATH +=          ../../../AuxiliaryLibraries/glew 
#						../../../AuxiliaryLibraries/particle \
#						../../../AuxiliaryLibraries/FTGL
						
#INCLUDEPATH +=			../../../OpenLibraries/opencore \
#						../../../OpenLibraries/openassetlib \
#						../../../OpenLibraries/openobjectlib \
#						../../../OpenLibraries/openmedialib \
#						../../../OpenLibraries/openmedialib/mediaobject 

#INCLUDEPATH +=          ../../JahCore/jahobjects 

#INCLUDEPATH +=			../../JahWidgets/interfaceobjs 

INCLUDEPATH +=			../jahtracer 
#						../../JahLibraries/jahpreferences 
#						../../JahLibraries/jahkeyframes \
#						$$FREEDIR

						
contains( JAHOS,IRIX ) {
INCLUDEPATH +=          $$SGIDIR
}



