###############################################################
#
# VisualMedia OpenLibraries QMake build files
#
###############################################################

include( ../../../Settings.pro )

###############################################################
# Configure subdirectories

message( "Configuring JahLibraries" )
message( "-------------------------" )

SUBDIRS += jahdatabase
SUBDIRS += jahdataio
#SUBDIRS += jahfontmanager
SUBDIRS += jahformatter
SUBDIRS += jahglcore
SUBDIRS += jahkeyframes
SUBDIRS += jahplugins
SUBDIRS += jahpreferences
SUBDIRS += jahthemes
SUBDIRS += jahtimer
SUBDIRS += jahtranslate
SUBDIRS += jahtracer
SUBDIRS += jahxml

TEMPLATE = subdirs

