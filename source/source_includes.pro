###############################################################
#
# Jahshaka 1.9a9 QMake module files
#
###############################################################

#include presets file in home directory
include( ../Settings.pro )

###############################################################
#set up include path for linking
INCLUDEPATH =           .  

#files for audiosupport
contains( JAHAUDIO,true ) {
INCLUDEPATH +=          AuxiliaryLibraries/sndfile/sndfile \
			AuxiliaryLibraries/sndfile/sndfile/G72x \ 
			AuxiliaryLibraries/sndfile/sndfile/GSM610 
}
                                        
INCLUDEPATH +=		OpenLibraries \
			OpenLibraries/opencore \
			OpenLibraries/openassetlib \
			OpenLibraries/openobjectlib \
			OpenLibraries/openobjectlib/surface3d \
			OpenLibraries/openmedialib \
			OpenLibraries/openmedialib/codecs/mpegenc \
			OpenLibraries/openmedialib/mediaobject \ 
			OpenLibraries/opengpulib \
			OpenLibraries/openimagelib \
			OpenLibraries/opennetworklib 

INCLUDEPATH +=          AuxiliaryLibraries \
                        AuxiliaryLibraries/apollon \
                        AuxiliaryLibraries/blur \
                        AuxiliaryLibraries/FTGL \
                        AuxiliaryLibraries/gift \
                        AuxiliaryLibraries/particle \ 
                        AuxiliaryLibraries/spaceball \ 
                        AuxiliaryLibraries/zlib  \
                        AuxiliaryLibraries/OpenEXR

win32 {
INCLUDEPATH += 		OpenLibraries/openmedialib/codecs/aviutils 
}
						
INCLUDEPATH +=          Jahshaka/JahCore/jahobjects \
			Jahshaka/JahCore/jahrender \
			Jahshaka/JahCore/jahworld 

INCLUDEPATH +=		Jahshaka/JahSource/jahcreate \
			Jahshaka/JahSource/jahmain \
			Jahshaka/JahSource/jahsplash

INCLUDEPATH +=		Jahshaka/JahModules/animation \
			Jahshaka/JahModules/colorize \
			Jahshaka/JahModules/effect \
			Jahshaka/JahModules/keyer \
			Jahshaka/JahModules/painter \
			Jahshaka/JahModules/text \
			Jahshaka/JahModules/tracker 

# Special case - uses another editing module when MLT available
contains( MLTSUPPORT,true ) {
	INCLUDEPATH += Jahshaka/JahModules/editing_mlt
}
else {
	INCLUDEPATH += Jahshaka/JahModules/editing
}

contains( OPENOBJECTLIBSUPPORT,true ) {
	INCLUDEPATH += $$OPENLIBRARIES_INCLUDE
}

contains( OPENALFRAMEWORK, true) {
	INCLUDEPATH += $$OPENAL_INCLUDE
}

contains( OPENOBJECTLIBSUPPORT,false ) {
	INCLUDEPATH += AuxiliaryLibraries/glew
}

INCLUDEPATH +=		Jahshaka/JahDesktop/desktop \
			Jahshaka/JahDesktop/encoder \
			Jahshaka/JahDesktop/library \
			Jahshaka/JahDesktop/network \
			Jahshaka/JahDesktop/player \
			Jahshaka/JahDesktop/videoio 

INCLUDEPATH +=		Jahshaka/JahWidgets/colortri \
			Jahshaka/JahWidgets/interfaceobjs \
			Jahshaka/JahWidgets/jahcalc \
			Jahshaka/JahWidgets/jahfileloader \
			Jahshaka/JahWidgets/keyframes \
			Jahshaka/JahWidgets/nodes \
			Jahshaka/JahWidgets/timeline \
			Jahshaka/JahWidgets/mediatable \
			Jahshaka/JahWidgets/wireup \
			Jahshaka/JahWidgets/colordropper

INCLUDEPATH +=		Jahshaka/JahLibraries \
			Jahshaka/JahLibraries/jahdatabase \
			Jahshaka/JahLibraries/jahdataio \
			Jahshaka/JahLibraries/jahformatter \
			Jahshaka/JahLibraries/jahglcore \ 
			Jahshaka/JahLibraries/jahkeyframes \
			Jahshaka/JahLibraries/jahplayer \
			Jahshaka/JahLibraries/jahplayer/audioplayer \
			Jahshaka/JahLibraries/jahplayer/diskplayer \
			Jahshaka/JahLibraries/jahplayer/ramplayer \
			Jahshaka/JahLibraries/jahplugins \
			Jahshaka/JahLibraries/jahpreferences \
			Jahshaka/JahLibraries/jahthemes \
			Jahshaka/JahLibraries/jahtimer \
			Jahshaka/JahLibraries/jahtracer \
			Jahshaka/JahLibraries/jahtranslate \
			Jahshaka/JahLibraries/jahxml \
			$$FREEDIR

#winblows is not recognizing the FREEDIR var so 
#we have to hard code it...
win32 {
    INCLUDEPATH += C:\freetype\include
}

contains( JAHOS,IRIX ) { INCLUDEPATH +=          $$SGIDIR }
