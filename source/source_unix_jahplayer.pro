###############################################################
#
# Jahshaka 1.9a9 QMake module files
#
###############################################################

#include presets file in home directory
include( ../Settings.pro )

###############################################################
# the rest of the makefile settings

TEMPLATE	=	app

DESTDIR = ..

TARGET	=	jahplayer

unix:target.path = $$JAHPREFIX/bin
unix:INSTALLS += target

DEPENDPATH = $$JAHDEPENDPATH
contains( JAHOS,DARWIN ) {
RC_FILE = ../Pixmaps/jahshaka.icns
}

###############################################################
#set up include path for linking

include(source_includes.pro)

###############################################################
# Now lets link the Application

LIBS +=         Jahshaka/JahSource/jahmain/libjahmain.a \
                Jahshaka/JahSource/jahcreate/libjahcreate.a \

LIBS +=         Jahshaka/JahCore/jahworld/libjahworld.a \
                Jahshaka/JahCore/jahrender/libjahrender.a \
                Jahshaka/JahCore/jahobjects/libjahobjects.a

LIBS +=		Jahshaka/JahModules/animation/libanimate.a \

LIBS +=		Jahshaka/JahDesktop/desktop/libdesktop.a \
		Jahshaka/JahDesktop/player/libplayer.a \
		Jahshaka/JahDesktop/library/liblibrary.a 

LIBS +=		Jahshaka/JahWidgets/jahfileloader/libjahfileloader.a \
		Jahshaka/JahWidgets/waveform/libjahwaveform.a \
		Jahshaka/JahWidgets/mediatable/libjahmediatable.a 

				
LIBS +=		Jahshaka/JahLibraries/jahdatabase/libjahdatabase.a \
		Jahshaka/JahLibraries/jahdataio/libjahdataio.a \
		Jahshaka/JahLibraries/jahglcore/libglcore.a \ 
		Jahshaka/JahLibraries/jahkeyframes/libjahkeyframes.a \
		Jahshaka/JahLibraries/jahplugins/libjahplugins.a \
		Jahshaka/JahLibraries/jahpreferences/libjahpreferences.a \
		Jahshaka/JahLibraries/jahthemes/libjahthemes.a \
		Jahshaka/JahLibraries/jahformatter/libjahformatter.a \
		Jahshaka/JahWidgets/interfaceobjs/libinterfaceobjs.a \
		Jahshaka/JahWidgets/jahcalc/libjahcalc.a \
		Jahshaka/JahLibraries/jahtimer/libjahtimer.a \
		Jahshaka/JahLibraries/jahtracer/libjahtracer.a \
		Jahshaka/JahLibraries/jahtranslate/libjahtranslate.a \ 
		Jahshaka/JahLibraries/jahxml/libjahxml.a 

###############################################################
# first lets link the Openlibraries

LIBS += -L./OpenLibraries/lib -lopencorelib -lopenassetlib -lopenobjectlib -lmediaobject -lformats -lopengpulib -lopenimagelib 

LIBS += -L./OpenLibraries/openassetlib/v2_openassetlib/distributables -lv2_openassetlib
LIBS +=	-L./OpenLibraries/openassetlib/v2_openassetlib/sqlite3/distributables -lsqlite3

contains( OSNAME,[dD]arwin) {
        contains( OPENALFRAMEWORK,true ) {
                LIBS += $$OPENAL_LIBS
        } else {
                LIBS += -lopenal
        }
} else {
	LIBS += -lopenal
	#LIBS += -lSDL
}

# openobjectlib support
contains( OPENOBJECTLIBSUPPORT,true ) {
	LIBS += $$OPENLIBRARIES_LIBS
} else {
	LIBS += AuxiliaryLibraries/glew/libglew.a
}

#files for mpeg encoder on linux
contains( JAHMPEGENCODER,true ) {
LIBS +=         -lmpegenc
}

#hard code the app so it can find the libraries locally  
contains( JAHSTATIC,false ) {
LIBS +=			-Wl,--rpath=./source/OpenLibraries/lib -Wl,--rpath=$$JAHPREFIX/share/jahshaka/OpenLibraries/lib
}

###############################################################
# first lets link the Auxiliaryibraries

#includes for the audio hack


LIBS +=         AuxiliaryLibraries/blur/libblur.a \
		AuxiliaryLibraries/FTGL/libftgl.a \
                AuxiliaryLibraries/glew/libglew.a \
                AuxiliaryLibraries/particle/libparticle.a
                

#includes for the audio hack
contains( JAHAUDIO,true ) {
LIBS +=         AuxiliaryLibraries/sndfile/sndfile/libsndfile.a \
                AuxiliaryLibraries/sndfile/sndfile/G72x/libjahaudiog72.a \
                AuxiliaryLibraries/sndfile/sndfile/GSM610/libjahaudiogGSM.a
}


#files for spaceball on linux
contains( SPACEBALL,true ) {
LIBS +=		AuxiliaryLibraries/spaceball/libspaceball.a 
}   

##############################################################

#unix options                    
LIBS +=     -lm  -lfreetype 

contains( JAHOS,LINUX ) {
LIBS += -ljpeg -lXi
}

contains( LINUXAVISUPPORT,true ) {
LIBS +=         -laviplay
}

contains( JAHOS,DARWIN ) {
LIBS += -framework OpenGL -lobjc
}
contains( JAHOS,IRIX ) {
LIBS += -lm -L/usr/freeware/lib32 -lfreetype
}

###
# installation of data files
###
JAHINSTALL_LOCALROOT = ..
include( $${JAHINSTALL_LOCALROOT}/jahinstall_data.pro )

###



