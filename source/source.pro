###############################################################
#
# Jahshaka 1.9a9 QMake module files
#
###############################################################

#include presets file in home directory
include( ../Settings.pro )

###############################################################
# the rest of the makefile settings

contains( JAHPLAYER,true ) {	

#the linker for windows
win32 {
include(source_win_jahplayer.pro)
}

#the linker for unix based OS's
!win32 {
include(source_unix_jahplayer.pro)
}



} else {

#the linker for windows
win32 {
include(source_win.pro)
}

#the linker for unix based OS's
!win32 {
include(source_unix.pro)
}


}




