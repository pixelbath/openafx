%define tarball_name jahshaka
%define name jahshaka
%define version 2.0RC4
%define release 6
%define _prefix /usr

Summary: jahshaka
Name: jahshaka
Version: %{version}
Release: %{release}
Source: %{tarball_name}.tar.gz
Vendor: The jahshaka Project
URL: http://www.jahshaka.org
License: GPL
Group: Multimedia/Applications
Prefix: %{_prefix}
BuildRoot: %{_tmppath}/%name-%{version}-root
Requires: olib-mlt >= 0.2.2-1 olib-mlt++ >= 0.2.2-1 openlibraries >= 0.3.0-1
BuildRequires: olib-mlt-devel >= 0.2.2-1 olib-mlt++-devel >= 0.2.2-1 openlibraries-devel >= 0.3.0-1

%description
Jahshaka

%prep
%setup -n jahshaka -q

%build
./configure --prefix=%{_prefix} --disable-debug
LDFLAGS=-Wl,--rpath=/usr/olib/0.3.0/lib make
./configure --prefix=$RPM_BUILD_ROOT/%{_prefix}
strip jahshaka
mv jahshaka jahshaka.bin
find . -name "*.so" -exec strip {} \;

echo "#!/usr/bin/env sh" > jahshaka
echo "export PATH=/usr/olib/0.3.0/bin:\$PATH" >> jahshaka
echo "exec jahshaka.bin \"\$*\"" >> jahshaka
chmod +x jahshaka

%install
rm -rf $RPM_BUILD_ROOT
make DESTDIR=$RPM_BUILD_ROOT install
cp jahshaka.bin $RPM_BUILD_ROOT/%{_prefix}/bin/jahshaka.bin

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_prefix}/bin/
%{_prefix}/lib/
%{_prefix}/share/

